<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Http\Request;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels; 

class EnviarContacto extends Mailable
{
    use Queueable, SerializesModels;
    public $datos;
    public $correo;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->datos = $request;
        $this->subject = "Mensaje de contacto";
        $this->correo = $request->correo;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('correo_contacto')->replyTo($this->correo);
    }
}
