<?php

namespace App\Http\Controllers;

use App\Models\categorias;
use App\Models\Subcategorias;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class SubcategoriasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($categoria_id)
    {
        $categoria = categorias::find($categoria_id);
        $subcategorias  = Subcategorias::where('categoria_id',$categoria_id)->get();
        return view("admin.subcategorias.index",[
            'categoria_id'  =>$categoria_id,
            'subcategorias' =>$subcategorias,
            'categoria'     =>$categoria
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($categoria_id)
    {
        return view('admin.subcategorias.form',[
            'form_edit'     =>false,
            'categoria_id'  =>$categoria_id
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $categoria_id)
    {
        $this->validate($request,[
            'nombre'    =>'required'  
        ],[
            'nombre.required'   =>'El nombre de la subcategoría es requerido'
        ]);
        Subcategorias::create([
            'categoria_id'  =>$categoria_id,
            'nombre'        =>$request->nombre
        ]);
        return response()->json([
            'swal'=>true,
            'type'=>'success',//success,error,warning
            'title'=>'Creado',
            'text'=>'Categoria agregada con exito',
            'load'=> route('admin.subcategoria.index',$categoria_id)
        ],200);
        
/*         redirect()->route("admin.subcategoria.index",['categoria_id'=>$categoria_id])->with('mensaje','Categoria agregada con exito'); */
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Subcategorias  $subcategorias
     * @return \Illuminate\Http\Response
     */
    public function show(Subcategorias $subcategorias)
    {
        //DataTables::of();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Subcategorias  $subcategorias
     * @return \Illuminate\Http\Response
     */
    public function edit($categoria_id, Subcategorias $subcategorium)
    { 
        return view("admin.subcategorias.form",[
            'form_edit'         =>true,
            'categoria_id'      =>$categoria_id,
            'subcategoria'      =>$subcategorium
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Subcategorias  $subcategorias
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $categoria_id, Subcategorias $subcategorium)
    {
       $this->validate($request,[
           'nombre' =>'required'
       ],[
           'required'   =>'El :attribute es requerido'
       ]);
       $subcategorium->nombre = $request->nombre;
       $subcategorium->save();
       return response()->json([
           'swal'=>true,
           'type'=>'success',//success,error,warning
           'title'=>'Actualizado',
           'text'=>'Se actualizo la subcategoria',
           'load'=> route('admin.subcategoria.index',$categoria_id)
       ],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Subcategorias  $subcategorias
     * @return \Illuminate\Http\Response
     */
    public function destroy($categoria_id, Subcategorias $subcategorium)
    {
        $subcategorium->delete();
        return response()->json([
            'swal'=>true,
            'type'=>'success',//success,error,warning
            'title'=>'Éxito',
            'text'=>'Se eliminó la subcategoría',
            'reload'=>true
        ],200);
    }
}
