<?php

namespace App\Http\Controllers;

use App\Models\portadanosotros;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class PortadanosotrosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $portadanosotros['portadanosotros']=portadanosotros::paginate(5);
        return view('admin.index.portadanosotros.index',$portadanosotros);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.index.portadanosotros.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $campos=[

            'portada'=>'required|max:10000|mimes:jpeg,png,jpg',

        ];

        $mensaje=[

            'portada.required'=>'La :attribute es requerida',

        ];
        
        $this->validate($request,$campos,$mensaje);

        $portadanosotros = request()->except('_token');

        if($request->hasFile('portada')){
            $portadanosotros['portada']=$request->file('portada')->store('imagennosotros','public');
        }

        portadanosotros::insert($portadanosotros);

        return redirect('/admin_lasombrilla/portadanosotros')->with('mensaje','Imagen agregada con exito');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\portadanosotros  $portadanosotros
     * @return \Illuminate\Http\Response
     */
    public function show(portadanosotros $portadanosotros)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\portadanosotros  $portadanosotros
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $portadanosotros=portadanosotros::findOrFail($id);
        return view('admin.index.portadanosotros.edit', compact('portadanosotros') );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\portadanosotros  $portadanosotros
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $mensaje = [
            "portada.required" => "El :attribute es requerido",
        ];
        
        if($request->hasFile('portada')){

            $campos['portada']='required|max:10000|mimes:jpeg,png,jpg';

        }

        $this->validate($request,$campos,$mensaje);

        $datosportadanosotros = request()->except(['_token','_method']);

        if($request->hasFile('portada')){
            $portadanosotros = portadanosotros::findOrFail($id);
            Storage::delete('public/'.$portadanosotros->portada);
            $datosportadanosotros['portada']=$request->file('portada')->store('portadanosotros','public');
        }

        portadanosotros::where('id','=',$id)->update($datosportadanosotros);
        $mensaje = "Slider modificado con éxito";
        return redirect('/admin_lasombrilla/portadanosotros')->with(['mensaje'=>$mensaje,'portadanosotros'=>$portadanosotros]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\portadanosotros  $portadanosotros
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        portadanosotros::destroy($id);
        return redirect('/admin_lasombrilla/portadanosotros')->with('mensaje','Imagen eliminada con exito');
    }
}
