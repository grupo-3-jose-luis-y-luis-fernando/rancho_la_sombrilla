<?php

namespace App\Http\Controllers;

use App\Models\imagenventajasbrahman;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ImagenventajasbrahmanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $imagenventajasbrahmans['imagenventajasbrahmans']=imagenventajasbrahman::paginate(5);
        return view('admin.nuestragenetica.imagenvbrahman.index',$imagenventajasbrahmans);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.nuestragenetica.imagenvbrahman.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $campos=[

            'ventajas_economicas'=>'required|max:10000|mimes:jpeg,png,jpg',
        ];
       
        $mensaje=[

            'ventajas_economicas.required'=>'El :attribute es requerida'

        ];
        
        $this->validate($request,$campos,$mensaje);

        $imagenventajasbrahman = request()->except('_token');

        if($request->hasFile('ventajas_economicas')){
            $imagenventajasbrahman['ventajas_economicas']=$request->file('ventajas_economicas')->store('imagenventajasbrahman','public');
        }

        imagenventajasbrahman::insert($imagenventajasbrahman);

        return redirect('/admin_lasombrilla/imagenventajasbrahman')->with('mensaje','Imagen agregada con exito');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\imagenventajasbrahman  $imagenventajasbrahman
     * @return \Illuminate\Http\Response
     */
    public function show(imagenventajasbrahman $imagenventajasbrahman)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\imagenventajasbrahman  $imagenventajasbrahman
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $imagenventajasbrahmans=imagenventajasbrahman::findOrFail($id);
        return view('admin.nuestragenetica.imagenvbrahman.edit', compact('imagenventajasbrahmans') );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\imagenventajasbrahman  $imagenventajasbrahman
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $mensaje = [

            "ventajas_economicas.required" => "El :attribute es requerido",
        ];
        
        if($request->hasFile('ventajas_economicas')){

            $campos['ventajas_economicas']='required|max:10000|mimes:jpeg,png,jpg';

        }

        $this->validate($request,$campos,$mensaje);

        $imagenventajasbrahman = request()->except(['_token','_method']);

        if($request->hasFile('ventajas_economicas')){
            $imagenesbrahman = imagenventajasbrahman::findOrFail($id);
            Storage::delete('public/'.$imagenesbrahman->ventajas_economicas);
            $imagenventajasbrahman['ventajas_economicas']=$request->file('ventajas_economicas')->store('imagenventajasbrahman','public');
        }

        imagenventajasbrahman::where('id','=',$id)->update($imagenventajasbrahman);
        $mensaje = "Slider modificado con éxito";
        return redirect('/admin_lasombrilla/imagenventajasbrahman')->with(['mensaje'=>$mensaje,'imagenesbrahman'=>$imagenesbrahman]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\imagenventajasbrahman  $imagenventajasbrahman
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        imagenventajasbrahman::destroy($id);
        return redirect('/admin_lasombrilla/imagenventajasbrahman')->with('mensaje','Imagen eliminada con exito');
    }
}
