<?php

namespace App\Http\Controllers;

use App\Models\fotocontacto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class FotocontactoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $portadacontacto['portadacontacto']=fotocontacto::paginate(5);
        return view('admin.contacto.fotocontacto.index',$portadacontacto);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.contacto.fotocontacto.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $campos=[

            'portada'=>'required|max:10000|mimes:jpeg,png,jpg',

        ];

        $mensaje=[

            'portada.required'=>'La :attribute es requerida',

        ];
        
        $this->validate($request,$campos,$mensaje);

        $portadacontacto = request()->except('_token');

        if($request->hasFile('portada')){
            $portadacontacto['portada']=$request->file('portada')->store('imagencontacto','public');
        }

        fotocontacto::insert($portadacontacto);

        return redirect('/admin_lasombrilla/fotocontacto')->with('mensaje','Imagen agregada con exito');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\fotocontacto  $fotocontacto
     * @return \Illuminate\Http\Response
     */
    public function show(fotocontacto $fotocontacto)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\fotocontacto  $fotocontacto
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $portadacontacto=fotocontacto::findOrFail($id);
        return view('admin.contacto.fotocontacto.edit', compact('portadacontacto') );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\fotocontacto  $fotocontacto
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $mensaje = [
            "portada.required" => "El :attribute es requerido",
        ];
        
        if($request->hasFile('portada')){

            $campos['portada']='required|max:10000|mimes:jpeg,png,jpg';

        }

        $this->validate($request,$campos,$mensaje);

        $datosportadacontacto = request()->except(['_token','_method']);

        if($request->hasFile('portada')){
            $portadacontacto = fotocontacto::findOrFail($id);
            Storage::delete('public/'.$portadacontacto->portada);
            $datosportadacontacto['portada']=$request->file('portada')->store('portadacontacto','public');
        }

        fotocontacto::where('id','=',$id)->update($datosportadacontacto);
        $mensaje = "Slider modificado con éxito";
        return redirect('/admin_lasombrilla/fotocontacto')->with(['mensaje'=>$mensaje,'portadacontacto'=>$portadacontacto]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\fotocontacto  $fotocontacto
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        fotocontacto::destroy($id);
        return redirect('/admin_lasombrilla/fotocontacto')->with('mensaje','Imagen eliminada con exito');
    }
}
