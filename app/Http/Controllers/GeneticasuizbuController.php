<?php

namespace App\Http\Controllers;

use App\Models\geneticasuizbu;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class GeneticasuizbuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $geneticasuizbus['geneticasuizbus']=geneticasuizbu::paginate(5);
        return view('admin.nuestragenetica.suizbu.index',$geneticasuizbus);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.nuestragenetica.suizbu.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $campos=[ 
            'subtitulo'=>'required|string|max:100',
            'parrafo'=>'required|string',
            'imagen'=>'required|max:8192|mimes:jpeg,png,jpg',
            'slider'=>'required|max:8192|mimes:jpeg,png,jpg'
        ];
        
        $mensaje=[
            'subtitulo.required'=>'El titulo es requerido',
            'parrafo.required'=>'El :attribute requerida', 
            'imagen.required'=>'El :attribute es requerida',
            'slider.required'=>'El :attribute es requerida',
            'imagen.max'     =>'El tamaño máximo permitido es de 8M',
            'slider.max'     =>'El tamaño máximo permitido es de 8M',
            'imagen.mimes'   =>'Solo se permiten imágenes jpg o png'
        ];

        
        $this->validate($request,$campos,$mensaje);
        try {
            $datosdegeneticasuizbu = request()->except('_token'); 

            if($request->hasFile('imagen')){
                $datosdegeneticasuizbu['imagen']=$request->file('imagen')->store('geneticasuizbu','public');
            } 

            if($request->hasFile('slider')){
                $datosdegeneticasuizbu['slider']=$request->file('slider')->store('geneticasuizbu','public');
            } 

            geneticasuizbu::insert($datosdegeneticasuizbu);
            return redirect('/admin_lasombrilla/geneticasuizbu')->with('mensaje','Genetica Suizbu agregado con exito');
        } catch (\Throwable $th) {
            echo $th->getMessage();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\geneticasuizbu  $geneticasuizbu
     * @return \Illuminate\Http\Response
     */
    public function show(geneticasuizbu $geneticasuizbu)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\geneticasuizbu  $geneticasuizbu
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $geneticasuizbu=geneticasuizbu::findOrFail($id);
        return view('admin.nuestragenetica.suizbu.edit', compact('geneticasuizbu') );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\geneticasuizbu  $geneticasuizbu
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $campos['subtitulo']='required|string|max:100';
        $campos['parrafo']='required|string'; 
        
        $mensaje=[ 
            'subtitulo.required'=>'El nombre es requerido',
            'parrafo.required'=>'El parrafo es requerido',
            'imagen.required' => 'El :attribute es requerido',
            'slider.required' => 'El :attribute es requerido', 
        ];

        if($request->hasFile('imagen')){

            $campos['imagen']='required|max:10000|mimes:jpeg,png,jpg';

        }

        if($request->hasFile('slider')){

            $campos['slider']='required|max:10000|mimes:jpeg,png,jpg';

        }

        $this->validate($request,$campos,$mensaje);

        $datosdegeneticasuizbu = request()->except(['_token','_method']);

        $geneticasuizbu = geneticasuizbu::findOrFail($id);

        if($request->hasFile('imagen')){ 
            Storage::delete('public/'.$geneticasuizbu->imagen);
            $datosdegeneticasuizbu['imagen']=$request->file('imagen')->store('geneticasuizbu','public');
        }

        if($request->hasFile('slider')){ 
            Storage::delete('public/'.$geneticasuizbu->slider);
            $datosdegeneticasuizbu['slider']=$request->file('slider')->store('geneticasuizbu','public');
        }
        geneticasuizbu::where('id','=',$id)->update($datosdegeneticasuizbu);
        $mensaje = "Genetica Brahman modificado con éxito";
        return redirect('/admin_lasombrilla/geneticasuizbu')->with(['mensaje'=>$mensaje,'geneticasuizbu'=>$geneticasuizbu]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\geneticasuizbu  $geneticasuizbu
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        geneticasuizbu::destroy($id);
        return redirect('/admin_lasombrilla/geneticasuizbu')->with('mensaje','Genetica Suizbu eliminado con exito');
    }
}
