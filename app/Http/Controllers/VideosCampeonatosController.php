<?php

namespace App\Http\Controllers;

use App\Models\Campeonatos;
use App\Models\GaleriaCampeonatos;
use App\Models\video_campeonatos;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\Facades\DataTables;

class VideosCampeonatosController extends Controller
{
    public function index($campeonato_id)
    {
        $campeonato = Campeonatos::findOrFail($campeonato_id);
        return view('admin.campeonatos.videos.index',[
            'campeonato_id'=>$campeonato_id,
            'campeonato'    =>$campeonato
        ]);
    }
    
    public function crear($campeonato_id)
    {
        return view('admin.campeonatos.videos.form',[
            'form_edit'         =>false,
            'campeonato_id'     =>$campeonato_id
        ]);
    }
    
    public function guardar(Request $request,$campeonato_id)
    {
        $this->validate($request,[
            'video' =>'required',
        ],[
            'required'        =>'La :attribute es requerido',
            'video.required' =>'La video es requerida',
        ]);

        $video_campeonato = video_campeonatos::create([
            'video'     =>$request->video,
            'categoria'  =>$campeonato_id
        ]);
        $video_campeonato->save();

        return response()->json([
            'swal'  => true,
            'type'  => 'success', //success,error,warning
            'title' => 'Creado',
            'text'  => 'Imagen agregada con éxito',
            'load'  => route('admin.video_campeonato',$campeonato_id),
        ],200);
    }
             
    public function editar($campeonato_id, $video_campeonato_id)
    { 
        $video_campeonato = video_campeonatos::findOrfail($video_campeonato_id);
        return view('admin.campeonatos.videos.form',[
            'form_edit'             =>true,
            'video_campeonato'    =>$video_campeonato, 
            'campeonato_id'        =>$campeonato_id
        ]);
    }

    public function actualizar(Request $request, $campeonato_id, $video_campeonato_id)
    {
        $this->validate($request,[
            'video' =>'required',
        ],[
            'required'        =>'La :attribute es requerido',
            'video.required' =>'La video es requerida',
        ]);

        $video_campeonato = video_campeonatos::findOrfail($video_campeonato_id);

        $video_campeonato->video = $request->video;
        $video_campeonato->save();

        return response()->json([
            'swal'  => true,
            'type'  => 'success', //success,error,warning
            'title' => 'Editado',
            'text'  => 'Imagen editada con exito',
            'load'  => route('admin.video_campeonato',$campeonato_id),
        ],200);
    }
    
    public function eliminar($campeonato_id, $video_campeonato_id)
    {
        $video_campeonato = video_campeonatos::find($video_campeonato_id);
        $video_campeonato->delete();
        return response()->json([
            'swal'   => true,
            'type'   => 'success',//success,error,warning
            'title'  => 'Éxito',
            'text'   => 'Se eliminó la Imagen',
            'dttable'=> "#dt_video-campeonato"
        ],200);
    }
         
    public function datatable(Request $request, $campeonato_id)
    {
        $video_campeonato = video_campeonatos::select('videoscampeonatos.*','campeonatos.nombre as nombre_categoria')
        ->join("campeonatos",'campeonatos.id','videoscampeonatos.categoria')->where('campeonatos.id',$campeonato_id)->get();
        return DataTables::of($video_campeonato)
        ->addColumn('numero', function($video_campeonato){
            return $video_campeonato->id;
        })
        ->addColumn('categoria', function($video_campeonato){
            return $video_campeonato->nombre_categoria;
        })
        ->addColumn('btn', function($video_campeonato){
            return "<a href=".route('admin.editar_video_campeonato', ['campeonato_id'=>$video_campeonato->categoria,'video_campeonato_id'=>$video_campeonato->id])." class='btn btn-warning'>Editar</a>
            <form class='form' action='".route('admin.eliminar_video_campeonato', ['campeonato_id'=>$video_campeonato->categoria,'video_campeonato_id'=>$video_campeonato->id])."' data-alert='true'>
                ".csrf_field()."
                <input type='submit' class='btn btn-danger' value='Eliminar'>
            </form>";
        })
        ->rawColumns(['numero','btn'])
        ->toJson();
    }
}
