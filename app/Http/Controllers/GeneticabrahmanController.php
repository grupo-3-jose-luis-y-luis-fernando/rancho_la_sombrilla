<?php

namespace App\Http\Controllers;

use App\Models\geneticabrahman;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class GeneticabrahmanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $geneticabrahmans['geneticabrahmans']=geneticabrahman::paginate(5);
        return view('admin.nuestragenetica.brahman.index',$geneticabrahmans);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.nuestragenetica.brahman.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $campos=[ 
            'subtitulo'=>'required|string|max:100',
            'parrafo'=>'required|string',
            'imagen'=>'required|max:8192|mimes:jpeg,png,jpg',
            'slider'=>'required|max:8192|mimes:jpeg,png,jpg'
        ];
        
        $mensaje=[
            'subtitulo.required'=>'El titulo es requerido',
            'parrafo.required'=>'El :attribute requerida', 
            'imagen.required'=>'El :attribute es requerida',
            'slider.required'=>'El :attribute es requerida',
            'imagen.max'     =>'El tamaño máximo permitido es de 8M',
            'slider.max'     =>'El tamaño máximo permitido es de 8M',
            'imagen.mimes'   =>'Solo se permiten imágenes jpg o png'
        ];

        
        $this->validate($request,$campos,$mensaje);
        try {
            $datosdegeneticabrahman = request()->except('_token'); 

            if($request->hasFile('imagen')){
                $datosdegeneticabrahman['imagen']=$request->file('imagen')->store('geneticabrahman','public');
            } 

            if($request->hasFile('slider')){
                $datosdegeneticabrahman['slider']=$request->file('slider')->store('geneticabrahman','public');
            } 

            geneticabrahman::insert($datosdegeneticabrahman);
            return redirect('/admin_lasombrilla/geneticabrahman')->with('mensaje','Genetica Brahman agregado con exito');
        } catch (\Throwable $th) {
            echo $th->getMessage();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\geneticabrahman  $geneticabrahman
     * @return \Illuminate\Http\Response
     */
    public function show(geneticabrahman $geneticabrahman)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\geneticabrahman  $geneticabrahman
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $geneticabrahman=geneticabrahman::findOrFail($id);
        return view('admin.nuestragenetica.brahman.edit', compact('geneticabrahman') );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\geneticabrahman  $geneticabrahman
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $campos['subtitulo']='required|string|max:100';
        $campos['parrafo']='required|string'; 
        
        $mensaje=[ 
            'subtitulo.required'=>'El nombre es requerido',
            'parrafo.required'=>'El parrafo es requerido',
            'imagen.required' => 'El :attribute es requerido',
            'slider.required' => 'El :attribute es requerido', 
        ];

        if($request->hasFile('imagen')){

            $campos['imagen']='required|max:10000|mimes:jpeg,png,jpg';

        }

        if($request->hasFile('slider')){

            $campos['slider']='required|max:10000|mimes:jpeg,png,jpg';

        }

        $this->validate($request,$campos,$mensaje);

        $datosdegeneticabrahma = request()->except(['_token','_method']);

        $geneticabrahma = geneticabrahman::findOrFail($id);

        if($request->hasFile('imagen')){ 
            Storage::delete('public/'.$geneticabrahma->imagen);
            $datosdegeneticabrahma['imagen']=$request->file('imagen')->store('geneticabrahma','public');
        }

        if($request->hasFile('slider')){ 
            Storage::delete('public/'.$geneticabrahma->slider);
            $datosdegeneticabrahma['slider']=$request->file('slider')->store('geneticabrahma','public');
        }
        geneticabrahman::where('id','=',$id)->update($datosdegeneticabrahma);
        $mensaje = "Genetica Brahman modificado con éxito";
        return redirect('/admin_lasombrilla/geneticabrahman')->with(['mensaje'=>$mensaje,'geneticabrahma'=>$geneticabrahma]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\geneticabrahman  $geneticabrahman
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        geneticabrahman::destroy($id);
        return redirect('/admin_lasombrilla/geneticabrahman')->with('mensaje','Genetica Brahman eliminado con exito');
    }
}
