<?php

namespace App\Http\Controllers;

use App\Models\indexinformacion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class IndexinformacionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $informacions['informacions']=indexinformacion::paginate(5);
        return view('admin.index.informacion.index',$informacions);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.index.informacion.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $campos=[ 
            'titulo'=>'required|string|max:50',
            'parrafo'=>'required|string',
            'portada'=>'required|max:10000|mimes:jpeg,png,jpg',
        ];
        
        $mensaje=[
            'titulo.required'=>'El nombre es requerido',
            'parrafo.required'=>'El :attribute requerida',
            'portada.required'=>'La :attribute es requerido',
        ];

        
        $this->validate($request,$campos,$mensaje);
        

        $datosinformacion = request()->except('_token');

        if($request->hasFile('portada')){
            $datosinformacion['portada']=$request->file('portada')->store('datosinformacion','public');
        }

        indexinformacion::insert($datosinformacion);


        return redirect('/admin_lasombrilla/informacion')->with('mensaje','Informacion agregada con exito');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\indexinformacion  $indexinformacion
     * @return \Illuminate\Http\Response
     */
    public function show(indexinformacion $indexinformacion)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\indexinformacion  $indexinformacion
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $informacions=indexinformacion::findOrFail($id);
        return view('admin.index.informacion.edit', compact('informacions') );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\indexinformacion  $indexinformacion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $campos['titulo']='required|string|max:100';
        $campos['parrafo']='required|string';
       
        $mensaje=[

            'titulo.required'=>'El nombre es requerido',
            "portada.required" => "El :attribute es requerido",
            'parrafo.required'=>'El :attribute requerido',

        ];

        if($request->hasFile('portada')){

            $campos['portada']='required|max:10000|mimes:jpeg,png,jpg';

        }

        $this->validate($request,$campos,$mensaje);

        $datosinformacion = request()->except(['_token','_method']);

        if($request->hasFile('portada')){
            $informacion = indexinformacion::findOrFail($id);
            Storage::delete('public/'.$informacion->portada);
            $datosinformacion['portada']=$request->file('portada')->store('informacion','public');
        }

        indexinformacion::where('id','=',$id)->update($datosinformacion);
        $mensaje = "Titulo modificado con éxito";
        return redirect('/admin_lasombrilla/informacion')->with(['mensaje'=>$mensaje]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\indexinformacion  $indexinformacion
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        indexinformacion::destroy($id);
        return redirect('/admin_lasombrilla/informacion')->with('mensaje','Informacion eliminada con exito');
    }
}
