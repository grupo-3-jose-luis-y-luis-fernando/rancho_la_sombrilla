<?php

namespace App\Http\Controllers;

use App\Models\categorias;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class CategoriasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categorias['categorias']=categorias::paginate(5);
        return view('admin.categorias.index',$categorias);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.categorias.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $campos=[ 
            'nombre'=>'required|string|max:100',
        ];
        
        $mensaje=[
            'nombre.required'=>'El nombre es requerido',
        ];

        
        $this->validate($request,$campos,$mensaje);
        

        $categorias = request()->except('_token');

        categorias::insert($categorias);


        return redirect('/admin_lasombrilla/categoria')->with('mensaje','Categoria agregada con exito');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\categorias  $categorias
     * @return \Illuminate\Http\Response
     */
    public function show(categorias $categorias)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\categorias  $categorias
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categorias=categorias::findOrFail($id);
        return view('admin.categorias.edit', compact('categorias') );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\categorias  $categorias
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $campos=[

            'nombre'=>'required|string|max:100',
        ];
       
        $mensaje=[

            'nombre.required'=>'El nombre es requerido',
        ];

        $this->validate($request,$campos,$mensaje);

        $categorias = request()->except(['_token','_method']);
        categorias::where('id','=',$id)->update($categorias);

        $categorias=categorias::findOrFail($id);
        //return view('admin.razas.edit', compact('razas') );
        return redirect('/admin_lasombrilla/categoria')->with('mensaje','Categoria modificada con exito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\categorias  $categorias
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        categorias::destroy($id);
        return redirect('/admin_lasombrilla/categoria')->with('mensaje','Categoria eliminada con exito');
    }

    public function datatable(Request $request)
    {
        $categorias = categorias::all();
        return DataTables::of($categorias)
        ->addColumn('numero',function($categorias){
            return $categorias->id;
        })
        ->addColumn('btn', function($categorias){
            return "<a href='".route('admin.subcategoria.index',$categorias->id)."' class='btn btn-primary'>Subcategorías</a><a href='".route('admin.categoria.edit',$categorias->id)."' class='btn btn-warning'>Editar</a>
            <form method='POST' action='".route('admin.categoria.destroy',$categorias->id)."'>
            ".csrf_field()."
            ".method_field('DELETE')."
            <button type='submit' class='btn btn-danger'>Eliminar</button>
            </form>";
        })
        ->rawColumns(['btn'])
        ->toJson();
    }
}
