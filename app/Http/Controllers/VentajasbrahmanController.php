<?php

namespace App\Http\Controllers;

use App\Models\ventajasbrahman;
use Illuminate\Http\Request;

class VentajasbrahmanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ventajasbrahmans['ventajasbrahmans']=ventajasbrahman::paginate(5);
        return view('admin.nuestragenetica.ventajasbrahman.index',$ventajasbrahmans);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.nuestragenetica.ventajasbrahman.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $campos=[ 
            'titulo'=>'required|string|max:80',
            'parrafo'=>'required|string|max:500',
        ];
        
        $mensaje=[
            'titulo.required'=>'El titulo es requerido',
            'parrafo.required'=>'El parrafo es requerido',
        ];

        
        $this->validate($request,$campos,$mensaje);
        

        $ventajasbrahman = request()->except('_token');

        ventajasbrahman::insert($ventajasbrahman);


        return redirect('/admin_lasombrilla/ventajasbrahman')->with('mensaje','Ventaja agregada con exito');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ventajasbrahman  $ventajasbrahman
     * @return \Illuminate\Http\Response
     */
    public function show(ventajasbrahman $ventajasbrahman)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ventajasbrahman  $ventajasbrahman
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $ventajasbrahmans=ventajasbrahman::findOrFail($id);
        return view('admin.nuestragenetica.ventajasbrahman.edit', compact('ventajasbrahmans') );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ventajasbrahman  $ventajasbrahman
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $campos=[

            'titulo'=>'required|string|max:80',
            'parrafo'=>'required|string|max:500'
        ];
       
        $mensaje=[

            'titulo.required'=>'El titulo es requerido',
            'parrafo.required'=>'El parrafo es requerida',

        ];

        $this->validate($request,$campos,$mensaje);

        $ventajasbrahman = request()->except(['_token','_method']);
        ventajasbrahman::where('id','=',$id)->update($ventajasbrahman);

        $ventajasbrahman=ventajasbrahman::findOrFail($id);
        return redirect('/admin_lasombrilla/ventajasbrahman')->with('mensaje','Ventaja modificada con exito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ventajasbrahman  $ventajasbrahman
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        ventajasbrahman::destroy($id);
        return redirect('/admin_lasombrilla/ventajasbrahman')->with('mensaje','Ventaja eliminada con exito');
    }
}
