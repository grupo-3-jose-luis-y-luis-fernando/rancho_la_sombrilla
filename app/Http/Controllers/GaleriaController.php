<?php

namespace App\Http\Controllers;

use App\Models\Animales;
use App\Models\Galeria;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\Facades\DataTables;

class GaleriaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($animal_id)
    {
        $animal = Animales::find($animal_id);
        return view("admin.animales.galeria.index",[
            'animal'=>$animal
        ]);
    }

    public function datatable(Request $request, $animal_id)
    {
        $galeria = Galeria::where('animal_id',$animal_id)->get();
        return DataTables::of($galeria)
        ->addColumn('foto', function($galeria){
            return "<img src='".asset('storage/animales/galeria/'.$galeria->imagen)."' class='img-responsive'>";
        })
        ->addColumn('tipo',function($galeria){
            return $galeria->venta == 1? "Ventas" : "General";
        })
        ->addColumn('btn',function($galeria){
            return "<a href='".route('admin.galeria.edit',["animal_id"=>$galeria->animal_id,"galerium"=>$galeria->id])."' class='btn btn-warning'>Editar</a>
            <form data-alert='true' class='form' method='POST' action='".route('admin.galeria.destroy',["animal_id"=>$galeria->animal_id,"galerium"=>$galeria->id])."'>
            ".csrf_field()."
            ".method_field('DELETE')."
            <button type='submit' class='btn btn-danger'>Eliminar</button>
            </form>";
        })
        ->rawColumns(['foto','btn'])
        ->toJson();

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Animales $animal_id)
    {
        return view('admin.animales.galeria.form',[
            'form_edit' =>false,
            'animal'    =>$animal_id
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $animal_id)
    {
        $this->validate($request,[
            'imagen'    =>'required|mimes:png,jpg,jpeg|max:5120',
            'tipo'      =>'required'
        ],[
            'required'      =>'El :attribute es requerido',
            'imagen.mimes'  =>'Solo se permiten imágenes jpg y png',
            'imagen.max'    =>'El tamañano máximo de las imágenes es de 2MB'
        ]);

        $galeria = Galeria::create([
            'imagen'        =>$request->imagen,
            'venta'         =>$request->tipo,
            'animal_id'     =>$animal_id
        ]);

        $imagen = $request->file('imagen');
        $nueva_imagen = uniqid().".".$imagen->clientExtension();
        $imagen->storeAs('animales/galeria', $nueva_imagen, 'public');
        $galeria->imagen = $nueva_imagen;
        $galeria->save();
        
        return response()->json([
            'swal'  => true,
            'type'  => 'success', //success,error,warning
            'title' => 'Creado',
            'text'  => 'Foto agregada con exito',
            'load'  => route('admin.galeria.index',$animal_id),
        ],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Galeria  $galeria
     * @return \Illuminate\Http\Response
     */
    public function show(Galeria $galeria)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Galeria  $galeria
     * @return \Illuminate\Http\Response
     */
    public function edit(Animales $animal_id, Galeria $galerium)
    {
        return view('admin.animales.galeria.form',[
            'form_edit' =>true,
            'animal'    =>$animal_id,
            'galeria'   =>$galerium
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Galeria  $galeria
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $animal_id, Galeria $galerium)
    {
        $this->validate($request,[
            'imagen'    =>'nullable|mimes:png,jpg,jpeg|max:5120',
            'tipo'      =>'required'
        ],[
            'required'      =>'El :attribute es requerido',
            'imagen.mimes'  =>'Solo se permiten imágenes jpg y png',
            'imagen.max'    =>'El tamañano máximo de las imágenes es de 2MB'
        ]);

        if($request->hasFile('imagen')){
            $imagen = $request->file('imagen');
            $nueva_imagen = uniqid().".".$imagen->clientExtension();
            $imagen->storeAs('animales/galeria', $nueva_imagen, 'public');
            $galerium->imagen = $nueva_imagen;
        }
        $galerium->venta = $request->tipo;
        $galerium->save();

        return response()->json([
            'swal'  => true,
            'type'  => 'success', //success,error,warning
            'title' => 'Editado',
            'text'  => 'Foto editada con exito',
            'load'  => route('admin.galeria.index',$animal_id),
        ],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Galeria  $galeria
     * @return \Illuminate\Http\Response
     */
    public function destroy($animal_id, Galeria $galerium)
    {
        Storage::delete('animales/galeria/'.$galerium->imagen);
        $galerium->delete();
        return response()->json([
            'swal'=>true,
            'type'=>'success',//success,error,warning
            'title'=>'Éxito',
            'text'=>'Se eliminó la imagen',
            'dttable'=>"#dt_galeria"
        ],200);
    }
}
