<?php

namespace App\Http\Controllers;

use App\Models\categorias;
use App\Models\galeria1;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class Galeria1Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $galeria1s = galeria1::select('galeria1s.*','categorias.nombre as nombre_categoria')->join('categorias','categorias.id','=','galeria1s.categoria')->paginate(5);
        return view('admin.galeria1.index',['galeria1s'=>$galeria1s]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $galeria = galeria1::all();
        $categoria = categorias::all();
        return view('admin.galeria1.create',[
            'galeria'=>$galeria,
            'categoria'=>$categoria
         ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $campos=[
            
            'nombre'=>'required|string|max:100',
            'imagen'=>'required|max:10000|mimes:jpeg,png,jpg'
        ];
       
        $mensaje=[

            'imagen.required'=>'La :attribute es requerida'

        ];
        
        $this->validate($request,$campos,$mensaje);

        $datosgaleria = request()->except('_token');

        if($request->hasFile('imagen')){
            $datosgaleria['imagen']=$request->file('imagen')->store('galeria','public');
        }

        galeria1::insert($datosgaleria);

        return redirect('/admin_lasombrilla/galeria1')->with('mensaje','Imagen agregada con exito');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\galeria1  $galeria1
     * @return \Illuminate\Http\Response
     */
    public function show(galeria1 $galeria1)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\galeria1  $galeria1
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $galeria=galeria1::findOrFail($id);
        $categoria = categorias::all();
        return view('admin.galeria1.edit',[
            'galeria'=>$galeria,
            'categoria'=>$categoria
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\galeria1  $galeria1
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $campos['nombre']='required|string|max:100';
        $campos['categoria']='required';
       
        $mensaje = [
            "nombre.required"=>"El nombre es requerido",
            "imagen.required" => "El :attribute es requerido",
            "categoria.required"=>"La categoria es requerida",
        ];
        
        if($request->hasFile('imagen')){

            $campos['imagen']='required|max:10000|mimes:jpeg,png,jpg';

        }

        $this->validate($request,$campos,$mensaje);

        $datosdegaleria = request()->except(['_token','_method']);

        if($request->hasFile('imagen')){
            $galeria = galeria1::findOrFail($id);
            Storage::delete('public/'.$galeria->imagen);
            $datosdegaleria['imagen']=$request->file('imagen')->store('galeria','public');
        }

        galeria1::where('id','=',$id)->update($datosdegaleria);
        $mensaje = "Imagen modificada con éxito";
        $galerias = galeria1::select('galeria1s.*','categorias.nombre as nombre_categoria')->join('categorias','categorias.id','=','galeria1s.categoria')->paginate(5);
        return redirect('/admin_lasombrilla/galeria1')->with(['mensaje'=>$mensaje,'galerias'=>$galerias]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\galeria1  $galeria1
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $galeria =galeria1::findOrFail($id);
        if(Storage::delete('public/'.$galeria->imagen)){

            galeria1::destroy($id);

        }

        return redirect('/admin_lasombrilla/galeria1')->with('mensaje','Galeria eliminada con exito');
    }
}
