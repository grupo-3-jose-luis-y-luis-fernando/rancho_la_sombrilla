<?php

namespace App\Http\Controllers;

use App\Models\galeriareceptoras;
use App\Models\videosyt;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\Facades\DataTables;

class VideosytController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $videosyts['videosyts']=videosyt::paginate(5);
        return view('admin.videosyt.index',$videosyts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.videosyt.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $campos=[
            
            'nombre'=>'required|string|max:100',
            'miniatura'=>'required|max:10000|mimes:jpeg,png,jpg',
            'enlace'=>'required|string|max:100',
        ];
       
        $mensaje=[

            'nombre.required'=>'El :attribute es requerido',
            'miniatura.required'=>'La :attribute es requerida',
            'enlace.required'=>'El :attribute es requerido',            

        ];
        
        $this->validate($request,$campos,$mensaje);

        $datosvideo = request()->except('_token');

        if($request->hasFile('miniatura')){
            $datosvideo['miniatura']=$request->file('miniatura')->store('uploads','public');
        }

        videosyt::insert($datosvideo);

        return redirect('/admin_lasombrilla/videosyt')->with('mensaje','Video agregado con exito');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\videosyt  $videosyt
     * @return \Illuminate\Http\Response
     */
    public function show(videosyt $videosyt)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\videosyt  $videosyt
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $videosyt=videosyt::findOrFail($id);
        return view('admin.videosyt.edit', compact('videosyt') );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\videosyt  $videosyt
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $campos['nombre']='required|string|max:100';
        $campos['enlace']='required';
       

        $mensaje = [
            "nombre.required" => "El :attribute es requerido",
            "miniatura.required" => "La :attribute es requerida",
            "enlace.required" => "El :attribute es requerido",
        ];
        
        if($request->hasFile('miniatura')){

            $campos['miniatura']='required|max:10000|mimes:jpeg,png,jpg';

        }

        $this->validate($request,$campos,$mensaje);

        $datosvideosyt = request()->except(['_token','_method']);

        if($request->hasFile('miniatura')){
            $videosyt = videosyt::findOrFail($id);
            Storage::delete('public/'.$videosyt->miniatura);
            $datosvideosyt['miniatura']=$request->file('miniatura')->store('uploads','public');
        }

        videosyt::where('id','=',$id)->update($datosvideosyt);
        $mensaje = "Video modificado con éxito";
        return redirect('/admin_lasombrilla/videosyt')->with(['mensaje'=>$mensaje]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\videosyt  $videosyt
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $galeriareceptora = galeriareceptoras::where("video_id",$id)->get();
        foreach ($galeriareceptora as $item) {
            Storage::delete('receptoras/galeria/'.$item->imagen);
            $item->delete();
        }        
        Storage::delete('uploads/'.$item->imagen);
        videosyt::destroy($id);

        return response()->json([
            'swal'=>true,
            'type'=>'success',//success,error,warning
            'title'=>'Éxito',
            'text'=>'Se eliminó el video',
            'dttable'=>"#dt_videosyt"
        ],200);
    }

    public function datatable(Request $request)
    {
        $videosyt = videosyt::all();
        return DataTables::of($videosyt)
        ->addColumn('miniatura', function($videosyt){
            return "<img src=".asset('storage/'.$videosyt->miniatura)." class='img-responsive'>";
        })
        ->addColumn('btn', function($videosyt){
            return "<a href='".route('admin.videosyt.edit',$videosyt->id)."' class='btn btn-warning'>Editar</a>
            <form data-alert='true' class='form' method='POST' action='".route('admin.videosyt.destroy',$videosyt->id)."'>
            ".csrf_field()."
            ".method_field('DELETE')."
            <button type='submit' class='btn btn-danger'>Eliminar</button>
            </form>
            </form> <a href='".route('admin.galeriareceptoras.index',$videosyt->id)."' class='btn btn-primary'>Ver Galería</a>";
        })
        ->rawColumns(['miniatura','btn'])
        ->toJson();
    }
}
