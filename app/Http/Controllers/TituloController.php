<?php

namespace App\Http\Controllers;

use App\Models\titulo;
use Illuminate\Http\Request;

class TituloController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $titulo['titulos']=titulo::paginate(5);
        return view('admin.index.titulo.index',$titulo);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.index.titulo.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $campos=[ 
            'titulo'=>'required|string|max:50',
            'subtitulo'=>'required|string|max:100',
            'parrafo'=>'required|string|max:400',
        ];
        
        $mensaje=[
            'titulo.required'=>'El nombre es requerido',
            'subtitulo.required'=>'El nombre es requerido',
            'parrafo.required'=>'La raza es requerida',
        ];

        
        $this->validate($request,$campos,$mensaje);
        

        $titulo = request()->except('_token');

        titulo::insert($titulo);


        return redirect('/admin_lasombrilla/titulo')->with('mensaje','Titulo agregado con exito');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\titulo  $titulo
     * @return \Illuminate\Http\Response
     */
    public function show(titulo $titulo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\titulo  $titulo
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $titulos=titulo::findOrFail($id);
        return view('admin.index.titulo.edit', compact('titulos') );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\titulo  $titulo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $campos=[

            'titulo'=>'required|string|max:50',
            'subtitulo'=>'required|string|max:100',
            'parrafo'=>'required|string|max:400'
        ];
       
        $mensaje=[

            'titulo.required'=>'El Titulo es requerido',
            'subtitulo.required'=>'El Subtitulo es requerido',
            'parrafo.required'=>'El Parrafo es requerido',

        ];

        $this->validate($request,$campos,$mensaje);

        $titulo = request()->except(['_token','_method']);
        titulo::where('id','=',$id)->update($titulo);

        $titulo=titulo::findOrFail($id);
        return redirect('/admin_lasombrilla/titulo')->with('mensaje','Titulo modificado con exito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\titulo  $titulo
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        titulo::destroy($id);
        return redirect('/admin_lasombrilla/titulo')->with('mensaje','Titulo eliminado con exito');
    }
}
