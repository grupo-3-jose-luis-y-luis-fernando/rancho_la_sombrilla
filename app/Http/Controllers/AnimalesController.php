<?php

namespace App\Http\Controllers;

use App\Models\Animales;
use App\Models\categorias;
use App\Models\Galeria;
use App\Models\Subcategorias;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\Facades\DataTables;

class AnimalesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("admin.animales.index");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Animales $animale)
    {
        $categorias = categorias::all();
        $subcategorias = Subcategorias::where('categoria_id',$animale->categoria)->get();
        return view('admin.animales.form',[
            'form_edit'     =>false,
            'animal'        =>$animale,
            'categorias'    =>$categorias,
            'subcategorias' =>$subcategorias
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'nombre'        =>'required',
            'categoria'     =>'required',
            'subcategoria'  =>'required', 
            'imagen'        =>'required|mimes:png,jpg,jpeg|max:5120'
        ],[
            'nombre.required'           =>'El nombre del animal es requerido',
            'categoria.required'        =>'La categoria del animal es requerido',
            'subcategoria.required'     =>'La subcategoria del animal es requerido',
            'imagen.required'           =>'La imagen es requerida',
            'imagen.max'                =>'El tamaño máximo de la imagen es de 2 MB',
            'imagen.mimes'              =>'Solo se permiten imágenes jpg y png'
        ]);
        $animal = Animales::create([
            'nombre'        =>$request->nombre,
            'categoria'     =>$request->categoria,
            'subcategoria'  =>$request->subcategoria,
            'imagen'        =>$request->imagen
        ]);

        $imagen = $request->file('imagen');
        $nueva_imagen = uniqid().".".$imagen->clientExtension();
        $imagen->storeAs('animales', $nueva_imagen, 'public');
        $animal->imagen = $nueva_imagen;
        $animal->save();
        
        return response()->json([
            'swal'  => true,
            'type'  => 'success', //success,error,warning
            'title' => 'Creado',
            'text'  => 'Animal agregado con exito',
            'load'  => route('admin.animales.index'),
        ],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Animales  $animales
     * @return \Illuminate\Http\Response
     */
    public function show(Animales $animales)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Animales  $animales
     * @return \Illuminate\Http\Response
     */
    public function edit(Animales $animale)
    {        
        $categorias = categorias::all();
        $subcategorias = Subcategorias::where('categoria_id',$animale->categoria)->get();
        return view('admin.animales.form',[
            'form_edit'     =>true,
            'animal'        =>$animale,
            'categorias'    =>$categorias,
            'subcategorias' =>$subcategorias
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Animales  $animales
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Animales $animale)
    {
        $this->validate($request,[
            'nombre'        =>'required',
            'categoria'     =>'required',
            'subcategoria'  =>'required', 
            'imagen'        =>'nullable|mimes:png,jpg,jpeg|max:5120'
        ],[
            'nombre.required'           =>'El nombre del animal es requerido',
            'categoria.required'        =>'La categoria del animal es requerido',
            'subcategoria.required'     =>'La subcategoria del animal es requerido',
            'imagen.required'           =>'La imagen es requerida',
            'imagen.max'                =>'El tamaño máximo de la imagen es de 2 MB',
            'imagen.mimes'              =>'Solo se permiten imágenes jpg y png'
        ]);

        if($request->hasFile('imagen')){
            $imagen = $request->file('imagen');
            $nueva_imagen = uniqid().".".$imagen->clientExtension();
            $imagen->storeAs('animales', $nueva_imagen, 'public');
            $animale->imagen = $nueva_imagen;
        }
        $animale->nombre = $request->nombre;
        $animale->categoria = $request->categoria;
        $animale->subcategoria = $request->subcategoria;
        $animale->save();
        
        return response()->json([
            'swal'  => true,
            'type'  => 'success', //success,error,warning
            'title' => 'Editado',
            'text'  => 'Animal editado con exito',
            'load'  => route('admin.animales.index'),
        ],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Animales  $animales
     * @return \Illuminate\Http\Response
     */
    public function destroy(Animales $animale)
    {
        Storage::delete('animales/'.$animale->imagen);
        $galeria = Galeria::where('animal_id',$animale->id)->get();
        foreach ($galeria as $item) {
            Storage::delete('animales/galeria/'.$item->imagen);
            $item->delete();
        }
        $animale->delete();
        return response()->json([
            'swal'  =>true,
            'title' =>'Éxito',
            'type'  =>'success',
            'text'  =>'Se ha eliminado el animal',
            'dttable'=>'#dt_animales'
        ]);
    }

    public function datatable(Request $request)
    {
        $animales = Animales::select('animales.*','categorias.nombre as nombre_categoria','subcategorias.nombre as nombre_subcategoria')
        ->join("categorias",'categorias.id','animales.categoria')
        ->join('subcategorias','subcategorias.id','animales.subcategoria')
        ->get();
        return DataTables::of($animales)
        ->addColumn('numero', function($animales){
            return $animales->id;
        })
        ->addColumn('categoria', function($animales){
            return $animales->nombre_categoria;
        })
        ->addColumn('subcategoria', function($animales){
            return $animales->nombre_subcategoria;
        })
        ->addColumn('foto', function($animales){
            return "<img src=".asset('storage/animales/'.$animales->imagen)." class='img-responsive'>";
        })
        ->addColumn('btn', function($animales){
            return "<a href='".route('admin.animales.edit',$animales->id)."' class='btn btn-warning'>Editar</a>
            <form class='form' action='".route('admin.animales.destroy',$animales->id)."' data-alert='true'>
                ".csrf_field(). method_field('DELETE')."
                <input type='submit' class='btn btn-danger' value='Eliminar'>
            </form> <a href='".route('admin.galeria.index',$animales->id)."' class='btn btn-primary'>Ver Galería</a>";
        })
        ->rawColumns(['foto','btn'])
        ->toJson();
    }

    public function subcategorias(Request $request)
    {
        $subcategorias = Subcategorias::where('categoria_id',$request->categoria)->get();
        $html ="<option value=''>Selecciona una subcategoría</option>";
        foreach ($subcategorias as $item) {
            $html.="<option value='".$item->id."'>".$item->nombre."</option>";
        } 
        return $html;
    }
}
