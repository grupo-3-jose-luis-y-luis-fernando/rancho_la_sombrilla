<?php

namespace App\Http\Controllers;

use App\Models\nosotros;
use Illuminate\Http\Request;

class NosotrosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $nosotros['nosotros']=nosotros::paginate(5);
        return view('admin.index.nosotros.index',$nosotros);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.index.nosotros.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $campos=[ 
            'titulo'=>'required|string|max:50',
            'parrafo'=>'required|string|max:400',
        ];
        
        $mensaje=[
            'titulo.required'=>'El nombre es requerido',
            'parrafo.required'=>'La raza es requerida',
        ];

        
        $this->validate($request,$campos,$mensaje);
        

        $nosotros = request()->except('_token');

        nosotros::insert($nosotros);


        return redirect('/admin_lasombrilla/nosotros')->with('mensaje','Titulo agregado con exito');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\nosotros  $nosotros
     * @return \Illuminate\Http\Response
     */
    public function show(nosotros $nosotros)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\nosotros  $nosotros
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $nosotros=nosotros::findOrFail($id);
        return view('admin.index.nosotros.edit', compact('nosotros') );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\nosotros  $nosotros
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $campos=[

            'titulo'=>'required|string|max:50',
            'parrafo'=>'required|string|max:400'
        ];
       
        $mensaje=[

            'titulo.required'=>'El Titulo es requerido',
            'parrafo.required'=>'El Parrafo es requerido',

        ];

        $this->validate($request,$campos,$mensaje);

        $nosotros = request()->except(['_token','_method']);
        nosotros::where('id','=',$id)->update($nosotros);

        $nosotros=nosotros::findOrFail($id);
        return redirect('/admin_lasombrilla/nosotros')->with('mensaje','Nosotros modificado con exito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\nosotros  $nosotros
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        nosotros::destroy($id);
        return redirect('/admin_lasombrilla/nosotros')->with('mensaje','Nosotros eliminado con exito');
    }
}
