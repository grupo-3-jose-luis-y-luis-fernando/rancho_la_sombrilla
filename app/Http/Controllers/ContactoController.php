<?php

namespace App\Http\Controllers;

use App\Models\contacto;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class ContactoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contacto = contacto::find(1);
        return view('admin.contacto.index',[
            'contacto'  =>$contacto
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.contacto.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $campos=[ 
            'titulo'=>'required|string|max:50',
            'dato'=>'required|string|max:400',
        ];
        
        $mensaje=[
            'titulo.required'=>'El titulo es requerido',
            'dato.required'=>'El dato es requerido',
        ];

        
        $this->validate($request,$campos,$mensaje);
        

        $contacto = request()->except('_token');

        contacto::insert($contacto);


        return redirect('/admin_lasombrilla/contacto')->with('mensaje','Contacto agregado con exito');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\contacto  $contacto
     * @return \Illuminate\Http\Response
     */
    public function show(contacto $contacto)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\contacto  $contacto
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $contactos=contacto::findOrFail($id);
        return view('admin.contacto.edit', compact('contactos') );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\contacto  $contacto
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $campos=[
            'direccion' =>'required',
            'telefono'  =>'required',
            'correo'    =>'required',
        ];
       
        $mensaje=[
            'required'  =>'El :attribute es requerido'
        ];

        $this->validate($request,$campos,$mensaje);

        $contacto = request()->except(['_token','_method']);
        contacto::where('id','=',$id)->update($contacto);

        $contacto=contacto::findOrFail($id);
        return redirect('/admin_lasombrilla/contacto')->with('mensaje','Información de contacto actualizado con exito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\contacto  $contacto
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        contacto::destroy($id);
        return redirect('/admin_lasombrilla/contacto')->with('mensaje','Contacto eliminado con exito');
    }

    public function datatable(Request $request)
    {
        $contacto = contacto::all();
        return DataTables::of($contacto)
        ->addColumn('btn', function($contacto){
            return "<a href='".route('admin.contacto.edit',$contacto->id)."' class='btn btn-warning'>Editar</a>";
        })
        ->rawColumns(['btn'])
        ->toJson();
    }
}
