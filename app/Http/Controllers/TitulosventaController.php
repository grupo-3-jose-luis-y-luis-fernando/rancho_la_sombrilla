<?php

namespace App\Http\Controllers;

use App\Models\titulosventa;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class TitulosventaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $titulosventas['titulosventas']=titulosventa::paginate(5);
        return view('admin.index.titulosventa.index',$titulosventas);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.index.titulosventa.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $campos=[ 
            'titulo'=>'required|string|max:50',
            'portada'=>'required|max:10000|mimes:jpeg,png,jpg',
        ];
        
        $mensaje=[
            'titulo.required'=>'El nombre es requerido',
            'portada.required'=>'La :attribute es requerida',
        ];

        
        $this->validate($request,$campos,$mensaje);
        

        $titulosventa = request()->except('_token');

        if($request->hasFile('portada')){
            $titulosventa['portada']=$request->file('portada')->store('titulosventa','public');
        }

        titulosventa::insert($titulosventa);


        return redirect('/admin_lasombrilla/titulosventa')->with('mensaje','Titulo agregado con exito');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\titulosventa  $titulosventa
     * @return \Illuminate\Http\Response
     */
    public function show(titulosventa $titulosventa)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\titulosventa  $titulosventa
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $titulosventas=titulosventa::findOrFail($id);
        return view('admin.index.titulosventa.edit', compact('titulosventas') );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\titulosventa  $titulosventa
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $campos['titulo']='required|string|max:100';
       
        $mensaje=[

            'titulo.required'=>'El nombre es requerido',
            "portada.required" => "El :attribute es requerido",

        ];

        if($request->hasFile('portada')){

            $campos['portada']='required|max:10000|mimes:jpeg,png,jpg';

        }

        $this->validate($request,$campos,$mensaje);

        $datostitulosventa = request()->except(['_token','_method']);

        if($request->hasFile('portada')){
            $imagentitulosventa = titulosventa::findOrFail($id);
            Storage::delete('public/'.$imagentitulosventa->portada);
            $datostitulosventa['portada']=$request->file('portada')->store('titulosventa','public');
        }

        titulosventa::where('id','=',$id)->update($datostitulosventa);
        $mensaje = "Titulo modificado con éxito";
        return redirect('/admin_lasombrilla/titulosventa')->with(['mensaje'=>$mensaje]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\titulosventa  $titulosventa
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        titulosventa::destroy($id);
        return redirect('/admin_lasombrilla/titulosventa')->with('mensaje','Imagen eliminada con exito');
    }
}
