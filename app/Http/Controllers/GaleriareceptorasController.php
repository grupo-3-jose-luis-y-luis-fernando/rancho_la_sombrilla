<?php

namespace App\Http\Controllers;

use App\Models\galeriareceptoras;
use App\Models\videosyt;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\Facades\DataTables;

class GaleriareceptorasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($video_id)
    {
        $videosyts = videosyt::find($video_id);
        return view("admin.videosyt.galeria.index",[
            'videosyts'=>$videosyts
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(videosyt $video_id)
    {
        return view('admin.videosyt.galeria.form',[
            'form_edit' =>false,
            'video'    =>$video_id
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $video_id)
    {
        $this->validate($request,[
            'imagen'    =>'required|mimes:png,jpg,jpeg|max:5120',
        ],
        [
            'required'      =>'El :attribute es requerido',
            'imagen.mimes'  =>'Solo se permiten imágenes jpg y png',
            'imagen.max'    =>'El tamañano máximo de las imágenes es de 5MB'
        ]);

        $galeria = galeriareceptoras::create([
            'imagen'        =>$request->imagen,
            'video_id'      =>$video_id
        ]);

        $imagen = $request->file('imagen');
        $nueva_imagen = uniqid().".".$imagen->clientExtension();
        $imagen->storeAs('receptoras/galeria', $nueva_imagen, 'public');
        $galeria->imagen = $nueva_imagen;
        $galeria->save();
        
        return response()->json([
            'swal'  => true,
            'type'  => 'success', //success,error,warning
            'title' => 'Creado',
            'text'  => 'Foto agregada con exito',
            'load'  => route('admin.galeriareceptoras.index',$video_id),
        ],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\galeriareceptoras  $galeriareceptoras
     * @return \Illuminate\Http\Response
     */
    public function show(galeriareceptoras $galeriareceptoras)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\galeriareceptoras  $galeriareceptoras
     * @return \Illuminate\Http\Response
     */
    public function edit(videosyt $video_id, galeriareceptoras $galeriareceptora)
    {
        return view('admin.videosyt.galeria.form',[
            'form_edit'          =>true,
            'video'              =>$video_id,
            'galeriareceptoras'  =>$galeriareceptora
        ]);
    }
    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\galeriareceptoras  $galeriareceptoras
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $video_id, galeriareceptoras $galeriareceptoras)
    {
        $this->validate($request,[
            'imagen'    =>'nullable|mimes:png,jpg,jpeg|max:5120',
        ],
        [
            'required'      =>'El :attribute es requerido',
            'imagen.mimes'  =>'Solo se permiten imágenes jpg y png',
            'imagen.max'    =>'El tamañano máximo de las imágenes es de 2MB'
        ]);

        if($request->hasFile('imagen')){
            $imagen = $request->file('imagen');
            $nueva_imagen = uniqid().".".$imagen->clientExtension();
            $imagen->storeAs('animales/galeria', $nueva_imagen, 'public');
            $galeriareceptoras->imagen = $nueva_imagen;
        }
        $galeriareceptoras->save();

        return response()->json([
            'swal'  => true,
            'type'  => 'success', //success,error,warning
            'title' => 'Editado',
            'text'  => 'Foto editada con exito',
            'load'  => route('admin.galeria.index',$video_id),
        ],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\galeriareceptoras  $galeriareceptoras
     * @return \Illuminate\Http\Response
     */
    public function destroy($video_id, galeriareceptoras $galeriareceptora)
    { 
        Storage::delete('receptoras/galeria/'.$galeriareceptora->imagen);
        $galeriareceptora->delete();
        return response()->json([
            'swal'=>true,
            'type'=>'success',//success,error,warning
            'title'=>'Éxito',
            'text'=>'Se eliminó la imagen',
            'dttable'=>"#dt_galeriareceptoras"
        ],200);
    }

    public function datatable(Request $request, $video_id)
    {
        $galeria = galeriareceptoras::where('video_id',$video_id)->get();
        return DataTables::of($galeria)
        ->addColumn('foto', function($galeria){
            return "<img src='".asset('storage/receptoras/galeria/'.$galeria->imagen)."' class='img-responsive'>";
        })
/*         ->addColumn('tipo',function($galeria){
            return $galeria->venta == 1? "Ventas" : "General";
        }) */
        ->addColumn('btn',function($galeria){
            return "<a href='".route('admin.galeriareceptoras.edit',["video_id"=>$galeria->video_id,"galeriareceptora"=>$galeria->id])."' class='btn btn-warning'>Editar</a>
            <form data-alert='true' class='form' method='POST' action='".route('admin.galeriareceptoras.destroy',["video_id"=>$galeria->video_id,"galeriareceptora"=>$galeria->id])."'>
            ".csrf_field()."
            ".method_field('DELETE')."
            <button type='submit' class='btn btn-danger'>Eliminar</button>
            </form>";
        })
        ->rawColumns(['foto','btn'])
        ->toJson();

    }

}
