<?php

namespace App\Http\Controllers;

use App\Models\preguntasfrecuente;
use Illuminate\Http\Request;

class PreguntasfrecuenteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $preguntasfrecuentes['preguntasfrecuentes']=preguntasfrecuente::paginate(5);
        return view('admin.faq.index',$preguntasfrecuentes);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.faq.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $campos=[ 
            'pregunta'=>'required|string|max:100',
            'informacion'=>'required|string',
        ];
        
        $mensaje=[
            'pregunta.required'=>'La pregunta es requerida',
            'informacion.required'=>'La :attribute requerida',
        ];

        
        $this->validate($request,$campos,$mensaje);
        

        $datospreguntasfrecuente = request()->except('_token');


        preguntasfrecuente::insert($datospreguntasfrecuente);


        return redirect('/admin_lasombrilla/preguntasfrecuente')->with('mensaje','Informacion agregada con exito');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\preguntasfrecuente  $preguntasfrecuente
     * @return \Illuminate\Http\Response
     */
    public function show(preguntasfrecuente $preguntasfrecuente)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\preguntasfrecuente  $preguntasfrecuente
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $preguntasfrecuentes=preguntasfrecuente::findOrFail($id);
        return view('admin.faq.edit', compact('preguntasfrecuentes') );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\preguntasfrecuente  $preguntasfrecuente
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $campos=[ 
            'pregunta'=>'required|string|max:100',
            'informacion'=>'required|string',
        ];
       
        $mensaje=[

            'pregunta.required'=>'La pregunta es requerida',
            'informacion.required'=>'La :attribute requerida',

        ];

        $this->validate($request,$campos,$mensaje);

        $datospreguntasfrecuente = request()->except(['_token','_method']);

        preguntasfrecuente::where('id','=',$id)->update($datospreguntasfrecuente);
        $mensaje = "Pregunta modificada con éxito";
        return redirect('/admin_lasombrilla/preguntasfrecuente')->with(['mensaje'=>$mensaje]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\preguntasfrecuente  $preguntasfrecuente
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        preguntasfrecuente::destroy($id);
        return redirect('/admin_lasombrilla/preguntasfrecuente')->with('mensaje','Pregunta eliminada con exito');
    }
}