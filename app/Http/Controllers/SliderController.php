<?php

namespace App\Http\Controllers;

use App\Models\slider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class SliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sliders['sliders']=slider::paginate(5);
        return view('admin.index.slider.index',$sliders);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.index.slider.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $campos=[

            'titulo'=>'required|string|max:100',
            'imagen'=>'required|max:10000|mimes:jpeg,png,jpg',
        ];
       
        $mensaje=[

            'required'=>'El :attribute es requerido',
            'imagen.required'=>'La :attribute es requerida'

        ];
        
        $this->validate($request,$campos,$mensaje);

        $slider = request()->except('_token');

        if($request->hasFile('imagen')){
            $slider['imagen']=$request->file('imagen')->store('slider','public');
        }

        slider::insert($slider);

        return redirect('/admin_lasombrilla/slider')->with('mensaje','Slider agregado con exito');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function show(slider $slider)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $slider=slider::findOrFail($id);
        return view('admin.index.slider.edit', compact('slider') );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $campos['titulo']='required|string|max:100';
       
        $mensaje = [
            "titulo.required"=>"El nombre es requerido",
            "imagen.required" => "El :attribute es requerido",
        ];
        
        if($request->hasFile('imagen')){

            $campos['imagen']='required|max:10000|mimes:jpeg,png,jpg';

        }

        $this->validate($request,$campos,$mensaje);

        $datosslider = request()->except(['_token','_method']);

        if($request->hasFile('imagen')){
            $slider = slider::findOrFail($id);
            Storage::delete('public/'.$slider->imagen);
            $datosslider['imagen']=$request->file('imagen')->store('slider','public');
        }

        slider::where('id','=',$id)->update($datosslider);
        $mensaje = "Slider modificado con éxito";
        return redirect('/admin_lasombrilla/slider')->with(['mensaje'=>$mensaje,'slider'=>$datosslider]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        slider::destroy($id);
        return redirect('/admin_lasombrilla/slider')->with('mensaje','Slider eliminado con exito');
    }
}
