<?php

namespace App\Http\Controllers;

use App\Models\imagenventajasuizbu;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ImagenventajasuizbuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $imagenventajasuizbus['imagenventajasuizbus']=imagenventajasuizbu::paginate(5);
        return view('admin.nuestragenetica.imagenvsuizbu.index',$imagenventajasuizbus);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.nuestragenetica.imagenvsuizbu.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $campos=[

            'ventajas_economicas'=>'required|max:10000|mimes:jpeg,png,jpg',
        ];
       
        $mensaje=[

            'ventajas_economicas.required'=>'El :attribute es requerida'

        ];
        
        $this->validate($request,$campos,$mensaje);

        $imagenventajasuizbu = request()->except('_token');

        if($request->hasFile('ventajas_economicas')){
            $imagenventajasuizbu['ventajas_economicas']=$request->file('ventajas_economicas')->store('imagenventajasuizbu','public');
        }

        imagenventajasuizbu::insert($imagenventajasuizbu);

        return redirect('/admin_lasombrilla/imagenventajasuizbu')->with('mensaje','Imagen agregada con exito');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\imagenventajasuizbu  $imagenventajasuizbu
     * @return \Illuminate\Http\Response
     */
    public function show(imagenventajasuizbu $imagenventajasuizbu)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\imagenventajasuizbu  $imagenventajasuizbu
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $imagenventajasuizbus=imagenventajasuizbu::findOrFail($id);
        return view('admin.nuestragenetica.imagenvsuizbu.edit', compact('imagenventajasuizbus') );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\imagenventajasuizbu  $imagenventajasuizbu
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $mensaje = [

            "ventajas_economicas.required" => "El :attribute es requerido",
        ];
        
        if($request->hasFile('ventajas_economicas')){

            $campos['ventajas_economicas']='required|max:10000|mimes:jpeg,png,jpg';

        }

        $this->validate($request,$campos,$mensaje);

        $imagenventajasuizbu = request()->except(['_token','_method']);

        if($request->hasFile('ventajas_economicas')){
            $imagenesuizbu = imagenventajasuizbu::findOrFail($id);
            Storage::delete('public/'.$imagenesuizbu->ventajas_economicas);
            $imagenventajasuizbu['ventajas_economicas']=$request->file('ventajas_economicas')->store('imagenventajasuizbu','public');
        }

        imagenventajasuizbu::where('id','=',$id)->update($imagenventajasuizbu);
        $mensaje = "Slider modificado con éxito";
        return redirect('/admin_lasombrilla/imagenventajasuizbu')->with(['mensaje'=>$mensaje,'imagenesuizbu'=>$imagenesuizbu]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\imagenventajasuizbu  $imagenventajasuizbu
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        imagenventajasuizbu::destroy($id);
        return redirect('/admin_lasombrilla/imagenventajasuizbu')->with('mensaje','Imagen eliminada con exito');
    }
}
