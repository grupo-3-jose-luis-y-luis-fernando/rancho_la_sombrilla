<?php

namespace App\Http\Controllers;

use App\Mail\EnviarContacto;
use App\Models\Animales;
use App\Models\caballocuartodemilla;
use App\Models\Campeonatos;
use App\Models\categorias;
use App\Models\Fierros;
use App\Models\fotocontacto;
use App\Models\Galeria;
use App\Models\galeria1;
use App\Models\galeria2;
use App\Models\galeria3;
use App\Models\galeria4;
use App\Models\GaleriaCampeonatos;
use App\Models\galeriareceptoras;
use App\Models\genetica;
use App\Models\geneticabrahman;
use App\Models\geneticasuizbu;
use App\Models\imagenventajasbrahman;
use App\Models\imagenventajasuizbu;
use App\Models\index;
use App\Models\indexcaballos;
use App\Models\indexinformacion;
use App\Models\nosotros;
use App\Models\portadanosotros;
use App\Models\preguntasfrecuente;
use App\Models\slider;
use App\Models\titulo;
use App\Models\titulosventa;
use App\Models\ventajasbrahman;
use App\Models\ventajassuizbu;
use App\Models\video_campeonatos;
use App\Models\videosyt;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class IndexController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sliders            = slider::all();
        $titulo             = titulo::findOrFail('1');
        $geneticas          = genetica::findOrFail('1');
        $titulosventas      = titulosventa::all();
        $indexcaballos      = indexcaballos::findOrFail('1');
        $informacions       = indexinformacion::all();
        $portadanosotros    = portadanosotros::findOrFail('1');
        $nosotros           = nosotros::all();        
        $fierros            = Fierros::all();
        $logoprincipal   = $fierros[0];
        return view('index', compact('sliders','titulo','geneticas','titulosventas','indexcaballos','informacions','portadanosotros','nosotros','logoprincipal','fierros'));
    }

    public function geneticabrahman()
    {

        $geneticabrahmans       = geneticabrahman::findOrFail('1');
        $ventajasbrahmans       = ventajasbrahman::all();
        $imagenventajasbrahmans = imagenventajasbrahman::findOrFail('1');
        $categorias = categorias::all();
        $galeria1s = galeria1::all();
        

        return view('geneticabh', compact('geneticabrahmans','ventajasbrahmans','imagenventajasbrahmans','categorias','galeria1s'));
    }

    public function geneticasuizbu()
    {

        $geneticasuizbus       = geneticasuizbu::findOrFail('1');
        $ventajassuizbus       = ventajassuizbu::all();
        $imagenventajasuizbus  = imagenventajasuizbu::findOrFail('1');
        $categorias = categorias::all();
        $galeria2s = galeria2::all();


        return view('geneticasz', compact('geneticasuizbus','ventajassuizbus','imagenventajasuizbus','categorias','galeria2s'));
    }

    public function caballoscuartodemilla(){

        $caballocuartodemillas = caballocuartodemilla::findOrFail('1');
        $categorias = categorias::all();
        $galeria3s = galeria3::all();


        return view('cuartodemilla', compact('caballocuartodemillas','categorias','galeria3s'));
    }

    public function ventas($categoria_id,$subcategoria_id)
    {

        $animales = Animales::where([
            ['categoria',$categoria_id],
            ['subcategoria',$subcategoria_id]
        ])->get();
        $categoria = categorias::find($categoria_id);

        return view('layouts.ventas',[
            'animales'=>$animales,
            'categoria'=>$categoria]);

    }

    public function campeonatos()
    {
        $campeonato         = Campeonatos::all();
        $galeria_campeonato = GaleriaCampeonatos::all();
        $video_campeonato   = video_campeonatos::all();

        return view('campeonatos', [

            'campeonatos'         => $campeonato,
            'galeria_campeonatos' => $galeria_campeonato,
            'video_campeonatos'   => $video_campeonato

        ]);
    }

    public function galeria()
    {

        $categorias = categorias::all();

        $galeria1s = galeria1::all();
        $galeria2s = galeria2::all();
        $galeria3s = galeria3::all();
        $galeria4s = galeria4::all();

         

        return view('galeria', compact('categorias','galeria1s','galeria2s','galeria3s','galeria4s'));
        
    }

    public function preguntas()
    {

        $preguntas = preguntasfrecuente::all();
        return view('preguntas',['preguntas'=>$preguntas]);

    }

    public function ventasrf1()
    {
        $videosyts = videosyt::all();
        $galeriareceptoras = galeriareceptoras::all();

        return view('ventasrf1', compact('videosyts','galeriareceptoras'));

    }

    public function portadacontacto2()
    {
        $portadacontacto = fotocontacto::all()->first();

        return view('contacto', compact('portadacontacto'));
    }


    public function enviar_mensaje(Request $request)
    {
        $this->validate($request,[
            'nombre'    =>'required',
            'mensaje'   =>'required',
            'correo'    =>'required|email'
        ],[
            'required'  =>'El :attribute es requerido',
            'email'     =>'El formato del correo es incorrecto'
        ]);

        Mail::to("admin@ranchosombrilla.com")->send(new EnviarContacto($request));

        return response()->json([
            'swal'  =>true,
            'title' =>'Éxito',
            'type'  =>'success',
            'text'  =>'Se ha enviado el correo, en breve nos pondremos en contacto',
            'form_clear'=>true 
        ]);
    }
}
