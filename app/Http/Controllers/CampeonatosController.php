<?php

namespace App\Http\Controllers;

use App\Models\Campeonatos;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\Facades\DataTables;

class CampeonatosController extends Controller
{
    public function index()
    {
        return view('admin.campeonatos.index');
    }
    
    public function crear()
    {
        return view('admin.campeonatos.form',[
            'form_edit' =>false
        ]);
    }
        
    public function guardar(Request $request)
    {
        $this->validate($request,[
            'nombre'     =>'required',
            'premio'     =>'required',
            'campeonato' =>'required',
            'imagen'     =>'required|mimes:png,jpg,jpeg'
        ],[
            'required'            =>':attribute es requerido',
            'nombre.required'     =>'El nombre del Campeonato es requerido',
            'premio.required'     =>'El premio del Campeonato es requerido',
            'campeonato.required' =>'El campeonato del Campeonato es requerido',
            'imagen.mimes'        =>'Solo se permiten imágenes jpg y png'
        ]);

        $campeonato = Campeonatos::create([
            'nombre'     =>$request->nombre,
            'premio'     =>$request->premio,
            'campeonato' =>$request->campeonato,
            'imagen'     =>$request->imagen
        ]);

        $imagen = $request->file('imagen');
        $nueva_imagen = uniqid().".".$imagen->clientExtension();
        $imagen->storeAs('campeonatos', $nueva_imagen, 'public');
        $campeonato->imagen = $nueva_imagen;
        $campeonato->save();

        return response()->json([
            'swal'          =>'swal',
            'type'          =>'success', //success,error,warning para el icono de sweetalert
            'title'         =>'Éxito', //Título de la alerta
            'text'          =>'Campeonato creado con exíto', //Texto de la alerta
            'load'          => route('admin.campeonato'),
        ]);
    }
        
    public function editar($campeonato_id)
    {

        $campeonato = Campeonatos::find($campeonato_id);
        return view('admin.campeonatos.form',[
            'form_edit'       =>true,
            'campeonato'      =>$campeonato,
        ]);
    }
                
    public function actualizar(Request $request, $campeonato_id)
    {
        $this->validate($request,[
            'nombre'     =>'required',
            'premio'     =>'required',
            'campeonato' =>'required',
            'imagen'     =>'null|mimes:png,jpg,jpeg'
        ],[
            'required'            =>':attribute es requerido',
            'nombre.required'     =>'El nombre del Campeonato es requerido',
            'premio.required'     =>'El premio del Campeonato es requerido',
            'campeonato.required' =>'El campeonato del Campeonato es requerido',
            'imagen.mimes'        =>'Solo se permiten imágenes jpg y png'
        ]);

        $campeonato = Campeonatos::find($campeonato_id);

        if($request->hasFile('imagen')){
            $imagen = $request->file('imagen');
            $nueva_imagen = uniqid().".".$imagen->clientExtension();
            $imagen->storeAs('campeonatos', $nueva_imagen, 'public');
            $campeonato->imagen = $nueva_imagen;
        }

        $campeonato->nombre     = $request->nombre;
        $campeonato->premio     = $request->premio;
        $campeonato->campeonato = $request->campeonato;
        $campeonato->save();

        return response()->json([
            'swal'          =>'swal',
            'type'          =>'success', //success,error,warning para el icono de sweetalert
            'title'         =>'Éxito', //Título de la alerta
            'text'          =>'Campeonato actualizado con exíto', //Texto de la alerta
            'load'          => route('admin.campeonato'),
        ]);
    }
                
    public function eliminar($campeonato_id)
    {
        $campeonato = Campeonatos::find($campeonato_id);
        Storage::delete('campeonatos'.$campeonato->imagen);
        $campeonato->delete();
        return response()->json([
            'swal'=>true,
            'type'=>'success',//success,error,warning
            'title'=>'Éxito',
            'text'=>'Se eliminó la imagen',
            'dttable'=>"#dt_campeonato"
        ],200);
    }
                
    public function datatable()
    {
        $campeonato = Campeonatos::all();
        return DataTables::of($campeonato)
        ->addColumn('numero', function($campeonato){
            return $campeonato->id;
        })
        ->addColumn('imagen', function($campeonato){
            return "<img src=".asset('storage/campeonatos/'.$campeonato->imagen)." class='img-responsive'>";
        })
        ->addColumn('btn', function($campeonato){
            return "<a href=".route('admin.editar_campeonato', $campeonato->id)." class='btn btn-warning'>Editar</a>
            <form class='form' action='".route('admin.eliminar_campeonato',$campeonato->id)."' data-alert='true'>
                ".csrf_field()."
                    <input type='submit' class='btn btn-danger' value='Eliminar'>
            </form>
            <a href='".route('admin.galeria_campeonato',$campeonato->id)."' class='btn btn-primary'>Ver Galería</a>
            <a href='".route('admin.video_campeonato',$campeonato->id)."' class='btn btn-primary'>Ver Videos</a>
            ";
        })
        ->rawColumns(['imagen','btn'])
        ->toJson();
    }
}
