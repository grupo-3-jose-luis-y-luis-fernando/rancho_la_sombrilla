<?php

namespace App\Http\Controllers;

use App\Models\genetica;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class GeneticaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $geneticas['geneticas']=genetica::paginate(5);
        return view('admin.index.genetica.index',$geneticas);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.index.genetica.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $campos=[ 
            'titulo'=>'required|string|max:100',
            'parrafo'=>'required|string',
            'imagen'=>'required|max:8192|mimes:jpeg,png,jpg',
            'slider'=>'required|max:8192|mimes:jpeg,png,jpg'
        ];
        
        $mensaje=[
            'titulo.required'=>'El titulo es requerido',
            'parrafo.required'=>'El :attribute requerida', 
            'imagen.required'=>'El :attribute es requerida',
            'slider.required'=>'El :attribute es requerida',
            'imagen.max'     =>'El tamaño máximo permitido es de 8M',
            'slider.max'     =>'El tamaño máximo permitido es de 8M',
            'imagen.mimes'   =>'Solo se permiten imágenes jpg o png'
        ];

        
        $this->validate($request,$campos,$mensaje);
        try {
            $datosdegenetica = request()->except('_token'); 
            if($request->hasFile('imagen')){
                $datosdegenetica['imagen']=$request->file('imagen')->store('genetica','public');
            } 
            if($request->hasFile('slider')){
                $datosdegenetica['slider']=$request->file('slider')->store('genetica','public');
            } 
            genetica::insert($datosdegenetica);
            return redirect('/admin_lasombrilla/genetica')->with('mensaje','Genetica agregado con exito');
        } catch (\Throwable $th) {
            echo $th->getMessage();
        } 
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\genetica  $genetica
     * @return \Illuminate\Http\Response
     */
    public function show(genetica $genetica)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\genetica  $genetica
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $genetica=genetica::findOrFail($id);
        return view('admin.index.genetica.edit', compact('genetica') );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\genetica  $genetica
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $campos['titulo']='required|string|max:100';
        $campos['parrafo']='required|string'; 
        
        $mensaje=[ 
            'titulo.required'=>'El nombre es requerido',
            'parrafo.required'=>'El parrafo es requerido',
            'imagen.required' => 'El :attribute es requerido',
            'slider.required' => 'El :attribute es requerido', 
        ];

        if($request->hasFile('imagen')){

            $campos['imagen']='required|max:10000|mimes:jpeg,png,jpg';

        }

        if($request->hasFile('slider')){

            $campos['slider']='required|max:10000|mimes:jpeg,png,jpg';

        }

        $this->validate($request,$campos,$mensaje);

        $datosgenetica = request()->except(['_token','_method']);
        $genetica = genetica::findOrFail($id);
        if($request->hasFile('imagen')){ 
            Storage::delete('public/'.$genetica->imagen);
            $datosgenetica['imagen']=$request->file('imagen')->store('genetica','public');
        }

        if($request->hasFile('slider')){ 
            Storage::delete('public/'.$genetica->slider);
            $datosgenetica['slider']=$request->file('slider')->store('genetica','public');
        }
        genetica::where('id','=',$id)->update($datosgenetica);
        $mensaje = "Caballos modificado con éxito";
        return redirect('/admin_lasombrilla/genetica')->with(['mensaje'=>$mensaje,'genetica'=>$genetica]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\genetica  $genetica
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        genetica::destroy($id);
        return redirect('/admin_lasombrilla/genetica')->with('mensaje','Genetica eliminado con exito');
    }
}
