<?php

namespace App\Http\Controllers;

use App\Models\ventajassuizbu;
use Illuminate\Http\Request;

class VentajassuizbuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ventajassuizbus['ventajassuizbus']=ventajassuizbu::paginate(5);
        return view('admin.nuestragenetica.ventajassuizbu.index',$ventajassuizbus);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.nuestragenetica.ventajassuizbu.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $campos=[ 
            'titulo'=>'required|string|max:80',
            'parrafo'=>'required|string|max:500',
        ];
        
        $mensaje=[
            'titulo.required'=>'El titulo es requerido',
            'parrafo.required'=>'El parrafo es requerido',
        ];

        
        $this->validate($request,$campos,$mensaje);
        

        $ventajassuizbu = request()->except('_token');

        ventajassuizbu::insert($ventajassuizbu);


        return redirect('/admin_lasombrilla/ventajassuizbu')->with('mensaje','Ventaja agregada con exito');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ventajassuizbu  $ventajassuizbu
     * @return \Illuminate\Http\Response
     */
    public function show(ventajassuizbu $ventajassuizbu)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ventajassuizbu  $ventajassuizbu
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $ventajassuizbus=ventajassuizbu::findOrFail($id);
        return view('admin.nuestragenetica.ventajassuizbu.edit', compact('ventajassuizbus') );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ventajassuizbu  $ventajassuizbu
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $campos=[

            'titulo'=>'required|string|max:80',
            'parrafo'=>'required|string|max:500'
        ];
       
        $mensaje=[

            'titulo.required'=>'El titulo es requerido',
            'parrafo.required'=>'El parrafo es requerida',

        ];

        $this->validate($request,$campos,$mensaje);

        $ventajassuizbu = request()->except(['_token','_method']);
        ventajassuizbu::where('id','=',$id)->update($ventajassuizbu);

        $ventajassuizbu=ventajassuizbu::findOrFail($id);
        return redirect('/admin_lasombrilla/ventajassuizbu')->with('mensaje','Ventaja modificada con exito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ventajassuizbu  $ventajassuizbu
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        ventajassuizbu::destroy($id);
        return redirect('/admin_lasombrilla/ventajassuizbu')->with('mensaje','Ventaja eliminada con exito');
    }
}
