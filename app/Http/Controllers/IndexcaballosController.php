<?php

namespace App\Http\Controllers;

use App\Models\indexcaballos;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class IndexcaballosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $indexcaballos['indexcaballos']=indexcaballos::paginate(5);
        return view('admin.index.caballos.index',$indexcaballos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.index.caballos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $campos=[ 
            'titulo'=>'required|string|max:100',
            'parrafo'=>'required|string',
            'imagen'=>'required|max:8192|mimes:jpeg,png,jpg',
            'slider'=>'required|max:8192|mimes:jpeg,png,jpg'
        ];
        
        $mensaje=[
            'titulo.required'=>'El titulo es requerido',
            'parrafo.required'=>'El :attribute requerida', 
            'imagen.required'=>'El :attribute es requerida',
            'slider.required'=>'El :attribute es requerida',
            'imagen.max'     =>'El tamaño máximo permitido es de 8M',
            'slider.max'     =>'El tamaño máximo permitido es de 8M',
            'imagen.mimes'   =>'Solo se permiten imágenes jpg o png'
        ];

        
        $this->validate($request,$campos,$mensaje);
        try {
            $datosdeindexcaballos = request()->except('_token'); 

            if($request->hasFile('imagen')){
                $datosdeindexcaballos['imagen']=$request->file('imagen')->store('indexcaballos','public');
            } 

            if($request->hasFile('slider')){
                $datosdeindexcaballos['slider']=$request->file('slider')->store('indexcaballos','public');
            } 

            indexcaballos::insert($datosdeindexcaballos);
            return redirect('/admin_lasombrilla/indexcaballos')->with('mensaje','Caballos agregado con exito');
        } catch (\Throwable $th) {
            echo $th->getMessage();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\indexcaballos  $indexcaballos
     * @return \Illuminate\Http\Response
     */
    public function show(indexcaballos $indexcaballos)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\indexcaballos  $indexcaballos
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $indexcaballos=indexcaballos::findOrFail($id);
        return view('admin.index.caballos.edit', compact('indexcaballos') );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\indexcaballos  $indexcaballos
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $campos['titulo']='required|string|max:100';
        $campos['parrafo']='required|string'; 
        
        $mensaje=[ 
            'titulo.required'=>'El nombre es requerido',
            'parrafo.required'=>'El parrafo es requerido',
            'imagen.required' => 'El :attribute es requerido',
            'slider.required' => 'El :attribute es requerido', 
        ];

        if($request->hasFile('imagen')){

            $campos['imagen']='required|max:10000|mimes:jpeg,png,jpg';

        }

        if($request->hasFile('slider')){

            $campos['slider']='required|max:10000|mimes:jpeg,png,jpg';

        }

        $this->validate($request,$campos,$mensaje);

        $datosdeindexcaballos = request()->except(['_token','_method']);

        $indexcaballos = indexcaballos::findOrFail($id);

        if($request->hasFile('imagen')){ 
            Storage::delete('public/'.$indexcaballos->imagen);
            $datosdeindexcaballos['imagen']=$request->file('imagen')->store('indexcaballos','public');
        }

        if($request->hasFile('slider')){ 
            Storage::delete('public/'.$indexcaballos->slider);
            $datosdeindexcaballos['slider']=$request->file('slider')->store('indexcaballos','public');
        }
        indexcaballos::where('id','=',$id)->update($datosdeindexcaballos);
        $mensaje = "Caballos modificado con éxito";
        return redirect('/admin_lasombrilla/indexcaballos')->with(['mensaje'=>$mensaje,'indexcaballos'=>$indexcaballos]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\indexcaballos  $indexcaballos
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        indexcaballos::destroy($id);
        return redirect('/admin_lasombrilla/indexcaballos')->with('mensaje','Caballos eliminado con exito');
    }
}
