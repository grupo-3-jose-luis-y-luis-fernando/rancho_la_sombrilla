<?php

namespace App\Http\Controllers;

use App\Models\caballocuartodemilla;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class CaballocuartodemillaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $caballocuartodemillas['caballocuartodemillas']=caballocuartodemilla::paginate(5);
        return view('admin.caballos.index',$caballocuartodemillas);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.caballos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $campos=[ 
            'subtitulo'=>'required|string|max:100',
            'parrafo'=>'required|string',
            'imagen'=>'required|max:8192|mimes:jpeg,png,jpg',
            'slider'=>'required|max:8192|mimes:jpeg,png,jpg'
        ];
        
        $mensaje=[
            'subtitulo.required'=>'El titulo es requerido',
            'parrafo.required'=>'El :attribute requerida', 
            'imagen.required'=>'El :attribute es requerida',
            'slider.required'=>'El :attribute es requerida',
            'imagen.max'     =>'El tamaño máximo permitido es de 8M',
            'slider.max'     =>'El tamaño máximo permitido es de 8M',
            'imagen.mimes'   =>'Solo se permiten imágenes jpg o png'
        ];

        
        $this->validate($request,$campos,$mensaje);
        try {
            $datosdecaballocuartodemilla = request()->except('_token'); 

            if($request->hasFile('imagen')){
                $datosdecaballocuartodemilla['imagen']=$request->file('imagen')->store('caballocuartodemilla','public');
            } 

            if($request->hasFile('slider')){
                $datosdecaballocuartodemilla['slider']=$request->file('slider')->store('caballocuartodemilla','public');
            } 

            caballocuartodemilla::insert($datosdecaballocuartodemilla);
            return redirect('/admin_lasombrilla/caballocuartodemilla')->with('mensaje','Caballos cuarto de milla agregado con exito');
        } catch (\Throwable $th) {
            echo $th->getMessage();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\caballocuartodemilla  $caballocuartodemilla
     * @return \Illuminate\Http\Response
     */
    public function show(caballocuartodemilla $caballocuartodemilla)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\caballocuartodemilla  $caballocuartodemilla
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $caballocuartodemilla=caballocuartodemilla::findOrFail($id);
        return view('admin.caballos.edit', compact('caballocuartodemilla') );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\caballocuartodemilla  $caballocuartodemilla
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $campos['subtitulo']='required|string|max:100';
        $campos['parrafo']='required|string'; 
        
        $mensaje=[ 
            'subtitulo.required'=>'El nombre es requerido',
            'parrafo.required'=>'El parrafo es requerido',
            'imagen.required' => 'El :attribute es requerido',
            'slider.required' => 'El :attribute es requerido', 
        ];

        if($request->hasFile('imagen')){

            $campos['imagen']='required|max:10000|mimes:jpeg,png,jpg';

        }

        if($request->hasFile('slider')){

            $campos['slider']='required|max:10000|mimes:jpeg,png,jpg';

        }

        $this->validate($request,$campos,$mensaje);

        $datosdecaballocuartodemilla = request()->except(['_token','_method']);

        $caballocuartodemilla = caballocuartodemilla::findOrFail($id);

        if($request->hasFile('imagen')){ 
            Storage::delete('public/'.$caballocuartodemilla->imagen);
            $datosdecaballocuartodemilla['imagen']=$request->file('imagen')->store('caballocuartodemilla','public');
        }

        if($request->hasFile('slider')){ 
            Storage::delete('public/'.$caballocuartodemilla->slider);
            $datosdecaballocuartodemilla['slider']=$request->file('slider')->store('caballocuartodemilla','public');
        }
        caballocuartodemilla::where('id','=',$id)->update($datosdecaballocuartodemilla);
        $mensaje = "Caballos cuarto de milla modificado con éxito";
        return redirect('/admin_lasombrilla/caballocuartodemilla')->with(['mensaje'=>$mensaje,'caballocuartodemilla'=>$caballocuartodemilla]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\caballocuartodemilla  $caballocuartodemilla
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        caballocuartodemilla::destroy($id);
        return redirect('/admin_lasombrilla/caballocuartodemilla')->with('mensaje','Caballos cuarto de milla eliminado con exito');
    }
}
