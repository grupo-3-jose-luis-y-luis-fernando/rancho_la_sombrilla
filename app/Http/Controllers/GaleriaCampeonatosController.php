<?php

namespace App\Http\Controllers;

use App\Models\Campeonatos;
use App\Models\GaleriaCampeonatos;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\Facades\DataTables;

class GaleriaCampeonatosController extends Controller
{
    public function index($campeonato_id)
    {
        $campeonato = Campeonatos::findOrFail($campeonato_id);
        return view('admin.campeonatos.galeria.index',[
            'campeonato_id'=>$campeonato_id,
            'campeonato'    =>$campeonato
        ]);
    }
    
    public function crear($campeonato_id)
    {
        return view('admin.campeonatos.galeria.form',[
            'form_edit'         =>false,
            'campeonato_id'     =>$campeonato_id
        ]);
    }
    
    public function guardar(Request $request,$campeonato_id)
    {
        $this->validate($request,[
            'imagen'         =>'required|mimes:png,jpg,jpeg'
        ],[
            'required'       =>':attribute es requerido',
            'imagen.mimes'   =>'Solo se permiten imágenes jpg y png'
        ]);

        $galeria_campeonato = GaleriaCampeonatos::create([
            'imagen'     =>$request->imagen,
            'categoria'  =>$campeonato_id
        ]);
        $imagen = $request->file('imagen');
        $nueva_imagen = uniqid().".".$imagen->clientExtension();
        $imagen->storeAs('campeonatos-galeria', $nueva_imagen, 'public');
        $galeria_campeonato->imagen = $nueva_imagen;
        $galeria_campeonato->save();

        return response()->json([
            'swal'  => true,
            'type'  => 'success', //success,error,warning
            'title' => 'Creado',
            'text'  => 'Imagen agregada con éxito',
            'load'  => route('admin.galeria_campeonato',$campeonato_id),
        ],200);
    }
             
    public function editar($campeonato_id, $galeria_campeonato_id)
    { 
        $galeria_campeonato = GaleriaCampeonatos::findOrfail($galeria_campeonato_id);
        return view('admin.campeonatos.galeria.form',[
            'form_edit'             =>true,
            'galeria_campeonato'    =>$galeria_campeonato, 
            'campeonato_id'         =>$campeonato_id
        ]);
    }
              
    public function actualizar(Request $request, $campeonato_id, $galeria_campeonato_id)
    {
        $this->validate($request,[
            'imagen'         =>'required|mimes:png,jpg,jpeg'
        ],[
            'required'       =>':attribute es requerido',
            'imagen.mimes'   =>'Solo se permiten imágenes jpg y png'
        ]);

        $galeria_campeonato = GaleriaCampeonatos::findOrfail($galeria_campeonato_id);

        if($request->hasFile('imagen')){
            $imagen = $request->file('imagen');
            $nueva_imagen = uniqid().".".$imagen->clientExtension();
            $imagen->storeAs('campeonatos-galeria', $nueva_imagen, 'public');
            $galeria_campeonato->imagen = $nueva_imagen;
        }
        $galeria_campeonato->save();

        return response()->json([
            'swal'  => true,
            'type'  => 'success', //success,error,warning
            'title' => 'Editado',
            'text'  => 'Imagen editada con exito',
            'load'  => route('admin.galeria_campeonato',$campeonato_id),
        ],200);
    }
    
    public function eliminar($campeonato_id, $galeria_campeonato_id)
    {
        $galeria_campeonato = GaleriaCampeonatos::find($galeria_campeonato_id);
        Storage::delete('campeonatos-galeria'.$galeria_campeonato->imagen);
        $galeria_campeonato->delete();
        return response()->json([
            'swal'   => true,
            'type'   => 'success',//success,error,warning
            'title'  => 'Éxito',
            'text'   => 'Se eliminó la Imagen',
            'dttable'=> "#dt_galeria-campeonato"
        ],200);
    }
         
    public function datatable(Request $request, $campeonato_id)
    {
        $galeria_campeonato = GaleriaCampeonatos::select('galeriacampeonatos.*','campeonatos.nombre as nombre_categoria')
        ->join("campeonatos",'campeonatos.id','galeriacampeonatos.categoria')->where('campeonatos.id',$campeonato_id)->get();
        return DataTables::of($galeria_campeonato)
        ->addColumn('numero', function($galeria_campeonato){
            return $galeria_campeonato->id;
        })
        ->addColumn('imagen', function($galeria_campeonato){
            return "<img src=".asset('storage/campeonatos-galeria/'.$galeria_campeonato->imagen)." class='img-responsive'>";
        })
        ->addColumn('categoria', function($galeria_campeonato){
            return $galeria_campeonato->nombre_categoria;
        })
        ->addColumn('btn', function($galeria_campeonato){
            return "<a href=".route('admin.editar_galeria_campeonato', ['campeonato_id'=>$galeria_campeonato->categoria,'galeria_campeonato_id'=>$galeria_campeonato->id])." class='btn btn-warning'>Editar</a>
            <form class='form' action='".route('admin.eliminar_galeria_campeonato', ['campeonato_id'=>$galeria_campeonato->categoria,'galeria_campeonato_id'=>$galeria_campeonato->id])."' data-alert='true'>
                ".csrf_field()."
                <input type='submit' class='btn btn-danger' value='Eliminar'>
            </form>";
        })
        ->rawColumns(['numero','imagen','btn'])
        ->toJson();
    }
}