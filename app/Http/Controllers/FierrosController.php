<?php

namespace App\Http\Controllers;

use App\Models\Fierros;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\Facades\DataTables;

class FierrosController extends Controller
{
    public function index()
    {
        return view('admin.index.fierros.index');
    }

    public function crear()
    {
        return view('admin.index.fierros.form',[
            'form_edit' =>false
        ]);
    }
        
    public function guardar(Request $request)
    {
        $this->validate($request,[
            'imagen'         =>'required|mimes:png,jpg,jpeg'
        ],[
            'required'              =>':attribute es requerido',
            'imagen.mimes'          =>'Solo se permiten imágenes jpg y png'
        ]);

        $fierros = Fierros::create([
            'nombre'     =>$request->nombre,
            'imagen'     =>$request->imagen
        ]);

        $imagen = $request->file('imagen');
        $nueva_imagen = uniqid().".".$imagen->clientExtension();
        $imagen->storeAs('fierros', $nueva_imagen, 'public');
        $fierros->imagen = $nueva_imagen;
        $fierros->save();

        return response()->json([
            'swal'          =>'swal',
            'type'          =>'success', //success,error,warning para el icono de sweetalert
            'title'         =>'Éxito', //Título de la alerta
            'text'          =>'Fierro creado con exíto', //Texto de la alerta
            'load'          => route('admin.fierro'),
        ]);
    }
        
    public function editar($fierro_id)
    {
        $fierros = Fierros::findOrFail($fierro_id);
        return view('admin.index.fierros.form',[
            'form_edit'   => true,
            'fierros'     => $fierros

        ]);
    }
                
    public function actualizar(Request $request, $fierro_id)
    {
        $this->validate($request,[
            'imagen'         =>'nullable|mimes:png,jpg,jpeg'
        ],[
            'required'              =>':attribute es requerido',
            'imagen.mimes'          =>'Solo se permiten imágenes jpg y png'
        ]);

        $fierros = Fierros::find($fierro_id);

        if($request->hasFile('imagen')){
            $imagen = $request->file('imagen');
            $nueva_imagen = uniqid().".".$imagen->clientExtension();
            $imagen->storeAs('fierros', $nueva_imagen, 'public');
            $fierros->imagen = $nueva_imagen;
        }
        $fierros->save();

        return response()->json([
            'swal'          =>'swal',
            'type'          =>'success', //success,error,warning para el icono de sweetalert
            'title'         =>'Éxito', //Título de la alerta
            'text'          =>'Fierro actualizado con exíto', //Texto de la alerta
            'load'          => route('admin.fierro'),
        ]);
    }
                
    public function eliminar($fierro_id)
    {
        $fierros = Fierros::find($fierro_id);
        Storage::delete('fierros'.$fierros->imagen);
        $fierros->delete();
        return response()->json([
            'swal'=>true,
            'type'=>'success',//success,error,warning
            'title'=>'Éxito',
            'text'=>'Se eliminó el Fierro',
            'dttable'=>"#dt_fierro"
        ],200);
    }
                      
    public function datatable()
    {
        $fierros = Fierros::all();
        return DataTables::of($fierros)
        ->addColumn('numero', function($fierros){
            return $fierros->id;
        })
        ->addColumn('imagen', function($fierros){
            return "<img src=".asset('storage/fierros/'.$fierros->imagen)." class='img-responsive'>";
        })
        ->addColumn('btn', function($fierros){
            return "<a href=".route('admin.editar_fierro', $fierros->id)." class='btn btn-warning'>Editar</a>
            <form class='form' action='".route('admin.eliminar_fierro',$fierros->id)."' data-alert='true'>
                ".csrf_field()."
                    <input type='submit' class='btn btn-danger' value='Eliminar'>
            </form>
            ";
        })
        ->rawColumns(['numero','imagen','btn'])
        ->toJson();
    }
}