<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class titulosventa extends Model
{
    protected $table = 'titulosventas';

    protected $fillable = [
        'titulo','portada'
    ];

    public $timestamps = true;
}
