<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class caballocuartodemilla extends Model
{
    protected $table = 'caballocuartodemillas';

    protected $fillable = [
        'titulo','parrafo','imagen','slider'
    ];

    public $timestamps = true;
}
