<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class video_campeonatos extends Model
{
    protected $table = 'videoscampeonatos';
    protected $fillable = ['video','categoria'];
}
