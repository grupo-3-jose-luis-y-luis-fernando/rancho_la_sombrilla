<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class genetica extends Model
{
    protected $table = 'geneticas';

    protected $fillable = [
        'titulo','parrafo','imagen','slider'
    ];

    public $timestamps = true;
}
