<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class videosyt extends Model
{
    protected $table = 'videosyts';

    protected $fillable = [
        'nombre','miniatura','enlace',
    ];

    public $timestamps = true;
}
