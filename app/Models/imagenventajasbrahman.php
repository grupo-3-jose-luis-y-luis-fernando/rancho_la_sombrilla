<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class imagenventajasbrahman extends Model
{
    protected $table = 'imagenventajasbrahmen';

    protected $fillable = [
        'ventajas_economicas',
    ];

    public $timestamps = true;
}
