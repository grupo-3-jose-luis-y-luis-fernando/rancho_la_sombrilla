<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class portadanosotros extends Model
{
    protected $table = 'portadanosotros';

    protected $fillable = [
        'portada',
    ];

    public $timestamps = true;
}
