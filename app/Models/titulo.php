<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class titulo extends Model
{
    protected $table = 'titulos';

    protected $fillable = [
        'titulo','subtitulo','parrafo',
    ];

    public $timestamps = true;
}