<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class indexcaballos extends Model
{
    protected $table = 'indexcaballos';

    protected $fillable = [
        'titulo','parrafo','imagen','slider'
    ];

    public $timestamps = true;
}
