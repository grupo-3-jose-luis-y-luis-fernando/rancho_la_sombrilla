<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ventajasbrahman extends Model
{
    protected $table = 'ventajasbrahmen';

    protected $fillable = [
        'titulo','parrafo',
    ];

    public $timestamps = true;
}
