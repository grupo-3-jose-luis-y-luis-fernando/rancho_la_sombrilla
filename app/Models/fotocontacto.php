<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class fotocontacto extends Model
{
    protected $table = 'fotocontactos';

    protected $fillable = [
        'portada',
    ];

    public $timestamps = true;
}
