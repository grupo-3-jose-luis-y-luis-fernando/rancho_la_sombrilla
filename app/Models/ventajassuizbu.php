<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ventajassuizbu extends Model
{
    protected $table = 'ventajassuizbus';

    protected $fillable = [
        'titulo','parrafo',
    ];

    public $timestamps = true;
}