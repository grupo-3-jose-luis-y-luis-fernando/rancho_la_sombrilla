<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class indexinformacion extends Model
{
    protected $table = 'indexinformacions';

    protected $fillable = [
        'titulo','parrafo','portada',
    ];

    public $timestamps = true;
}
