<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GaleriaCampeonatos extends Model
{
    protected $table = 'galeriacampeonatos';
    protected $fillable = ['imagen','categoria'];
}
