<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class imagenventajasuizbu extends Model
{
    protected $table = 'imagenventajasuizbus';

    protected $fillable = [
        'ventajas_economicas',
    ];

    public $timestamps = true;
}
