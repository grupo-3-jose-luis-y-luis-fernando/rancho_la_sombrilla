<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class galeriareceptoras extends Model
{
    use HasFactory;
    protected $fillable = ['imagen','video_id'];
}
