<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class geneticabrahman extends Model
{
    protected $table = 'geneticabrahmen';

    protected $fillable = [
        'titulo','parrafo','imagen','slider'
    ];

    public $timestamps = true;
}