<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class geneticasuizbu extends Model
{
    protected $table = 'geneticasuizbus';

    protected $fillable = [
        'titulo','parrafo','imagen','slider'
    ];

    public $timestamps = true;
}
