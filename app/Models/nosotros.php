<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class nosotros extends Model
{
    protected $table = 'nosotros';

    protected $fillable = [
        'titulo','parrafo',
    ];

    public $timestamps = true;
}
