<?php

namespace App\Providers;

use App\Models\categorias;
use App\Models\contacto;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $contacto = contacto::find(1);
        view()->share('contacto',$contacto);
        $categorias = categorias::all();
        view()->share('categorias', $categorias);
    }
}
