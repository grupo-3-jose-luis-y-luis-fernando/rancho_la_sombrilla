$("#dt_categorias").DataTable({
    processing: true,
    serverSide: true,
    responsive: true,
    ajax: {
        url: $("#dt_categorias").data('url'),
        dataType: "json",
        type: "POST",
        data: { _token: $("meta[name='csrf-token'] ").attr('content') }
    },
    language: {
        url: "https://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
    },
    columns: [
        { data: "numero"},
        { data: "nombre" },
        { data: "btn" }
    ]
});

$("#dt_subcategorias").DataTable({
    processing: true,
    serverSide: true,
    responsive: true,
    ajax: {
        url: $("#dt_subcategorias").data('url'),
        dataType: "json",
        type: "POST",
        data: { _token: $("meta[name='csrf-token'] ").attr('content') }
    },
    language: {
        url: "https://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
    },
    columns: [
        { data: "numero"},
        { data: "nombre" },
        { data: "btn" }
    ]
});

$("#dt_animales").DataTable({
    processing: true,
    serverSide: true,
    responsive: true,
    ajax: {
        url: $("#dt_animales").data('url'),
        dataType: "json",
        type: "POST",
        data: { _token: $("meta[name='csrf-token'] ").attr('content') }
    },
    language: {
        url: "https://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
    },
    columns: [
        { data: "numero" },
        { data: "categoria" },
        { data: "subcategoria" },
        { data: "foto" },
        { data: "nombre" },
        { data: "btn" }
    ]
});

$("#dt_galeria").DataTable({
    processing: true,
    serverSide: true,
    responsive: true,
    ajax: {
        url: $("#dt_galeria").data('url'),
        dataType: "json",
        type: "POST",
        data: { _token: $("meta[name='csrf-token'] ").attr('content') }
    },
    language: {
        url: "https://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
    },
    columns: [
        { data: "foto" },
        { data: "tipo" },
        { data: "btn" }
    ]
});

$("#dt_galeriareceptoras").DataTable({
    processing: true,
    serverSide: true,
    responsive: true,
    ajax: {
        url: $("#dt_galeriareceptoras").data('url'),
        dataType: "json",
        type: "POST",
        data: { _token: $("meta[name='csrf-token'] ").attr('content') }
    },
    language: {
        url: "https://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
    },
    columns: [
        { data: "foto" },
        { data: "btn" }
    ]
});

$("#dt_contacto").DataTable({
    processing: true,
    serverSide: true,
    responsive: true,
    ajax: {
        url: $("#dt_contacto").data('url'),
        dataType: "json",
        type: "POST",
        data: { _token: $("meta[name='csrf-token'] ").attr('content') }
    },
    language: {
        url: "https://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
    },
    columns: [
        { data: "direccion" },
        { data: "telefono" },
        { data: "telefono2" },
        { data: "telefono3" },
        { data: "correo" },
        { data: "facebook" },
        { data: "youtube" },
        { data: "instagram" },
        { data: "btn" }
    ]
});

$("#dt_videosyt").DataTable({
    processing: true,
    serverSide: true,
    responsive: true,
    ajax: {
        url: $("#dt_videosyt").data('url'),
        dataType: "json",
        type: "POST",
        data: { _token: $("meta[name='csrf-token'] ").attr('content') }
    },
    language: {
        url: "https://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
    },
    columns: [
        { data: "nombre" },
        { data: "miniatura" },
        { data: "enlace" },
        { data: "btn" }
    ]
});

$("#dt_fierro").DataTable({
    processing: true,
    serverSide: true,
    responsive: true,
    ajax: {
        url: $("#dt_fierro").data('url'),
        dataType: "json",
        type: "POST",
        data: { _token: $("meta[name='csrf-token'] ").attr('content') }
    },
    language: {
        url: "https://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
    },
    columns: [
        { data: "numero" },
        { data: "imagen" },
        { data: "btn" }
    ]
});

$("#dt_campeonato").DataTable({
    processing: true,
    serverSide: true,
    responsive: true,
    ajax: {
        url: $("#dt_campeonato").data('url'),
        dataType: "json",
        type: "POST",
        data: { _token: $("meta[name='csrf-token'] ").attr('content') }
    },
    language: {
        url: "https://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
    },
    columns: [
        { data: "numero" },
        { data: "nombre" },
        { data: "premio" },
        { data: "campeonato" },
        { data: "imagen" },
        { data: "btn" }
    ]
});

$("#dt_galeria-campeonato").DataTable({
    processing: true,
    serverSide: true,
    responsive: true,
    ajax: {
        url: $("#dt_galeria-campeonato").data('url'),
        dataType: "json",
        type: "POST",
        data: { _token: $("meta[name='csrf-token'] ").attr('content') }
    },
    language: {
        url: "https://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
    },
    columns: [
        { data: "numero" },
        { data: "imagen" },
        { data: "categoria" },
        { data: "btn" }
    ]
});

$("#dt_video-campeonato").DataTable({
    processing: true,
    serverSide: true,
    responsive: true,
    ajax: {
        url: $("#dt_video-campeonato").data('url'),
        dataType: "json",
        type: "POST",
        data: { _token: $("meta[name='csrf-token'] ").attr('content') }
    },
    language: {
        url: "https://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
    },
    columns: [
        { data: "numero" },
        { data: "video" },
        { data: "categoria" },
        { data: "btn" }
    ]
});