<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCaballocuartodemillasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('caballocuartodemillas', function (Blueprint $table) {
            $table->id();

            $table->string('subtitulo');
            $table->text('parrafo');
            $table->string('imagen');
            $table->string('slider');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('caballocuartodemillas');
    }
}
