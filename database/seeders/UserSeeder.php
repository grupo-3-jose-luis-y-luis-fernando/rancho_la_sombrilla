<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            ['name'     =>'Super Administrador Rancho La Sombrilla',
            'email'     =>'soporte@rancholasombrilla.com',
            'password'  =>bcrypt('rancholasombrilla2021')]
        ]);
        
        DB::table('users')->insert([
            ['name'     =>'Administrador Rancho La Sombrilla',
            'email'     =>'admin@rancholasombrilla.com',
            'password'  =>bcrypt('rancholasombrilla2021')]
        ]);
    }
}