<?php

use App\Http\Controllers\HomeController;
use App\Http\Controllers\IndexController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Artisan;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', [IndexController::class, 'index'])->name('index');

Route::get('geneticabh', [IndexController::class, 'geneticabrahman'])->name('geneticabh');

Route::get('geneticasz', [IndexController::class, 'geneticasuizbu'])->name('geneticasz');

Route::get('ventas/{categoria_id}/{subcategoria_id}', [IndexController::class, 'ventas'])->name('ventas');

Route::get('ventasrf1', [IndexController::class, 'ventasrf1'])->name('ventasrf1');

Route::get('cuartodemilla', [IndexController::class, 'caballoscuartodemilla'])->name('cuartodemilla');

Route::get('preguntas', [IndexController::class, 'preguntas'])->name('preguntas');

Route::get('campeonatos', [IndexController::class, 'campeonatos'])->name('campeonatos');

Route::get('galeria', [IndexController::class, 'galeria'])->name('galeria');

Route::get('contactame', [IndexController::class, 'portadacontacto2'])->name('contactame');

Route::post('enviar-mensaje',[IndexController::class, 'enviar_mensaje'])->name('enviar_mensaje');

Route::get('/home', [HomeController::class, 'index'])->name('home');

Route::get('/mantenimiento', function(){
    return Artisan::call("down --secret='lasombrilla2021' ");
});

Route::get('/no-mantenimiento', function(){
    return Artisan::call("up");
});

Route::get('storage-link', function(){
    Artisan::call('storage:link');
    return "Storage Link";
});
