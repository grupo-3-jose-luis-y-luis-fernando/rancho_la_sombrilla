<?php

use App\Http\Controllers\AnimalesController;
use App\Http\Controllers\CaballocuartodemillaController;
use App\Http\Controllers\CampeonatosController;
use App\Http\Controllers\CategoriasController;
use App\Http\Controllers\ContactoController;
use App\Http\Controllers\FierrosController;
use App\Http\Controllers\Galeria1Controller;
use App\Http\Controllers\Galeria2Controller;
use App\Http\Controllers\Galeria3Controller;
use App\Http\Controllers\Galeria4Controller;
use App\Http\Controllers\GaleriaController;
use App\Http\Controllers\GeneticabrahmanController;
use App\Http\Controllers\GeneticaController;
use App\Http\Controllers\GeneticasuizbuController;
use App\Http\Controllers\ImagenventajasbrahmanController;
use App\Http\Controllers\ImagenventajasuizbuController;
use App\Http\Controllers\IndexcaballosController;
use App\Http\Controllers\IndexinformacionController;
use App\Http\Controllers\NosotrosController;
use App\Http\Controllers\PortadanosotrosController;
use App\Http\Controllers\PreguntasfrecuenteController;
use App\Http\Controllers\SliderController;
use App\Http\Controllers\SubcategoriasController;
use App\Http\Controllers\TituloController;
use App\Http\Controllers\TitulosventaController;
use App\Http\Controllers\VentajasbrahmanController;
use App\Http\Controllers\VentajassuizbuController;
use App\Http\Controllers\VideosytController;
use App\Http\Controllers\FotocontactoController;
use App\Http\Controllers\GaleriaCampeonatosController;
use App\Http\Controllers\GaleriareceptorasController;
use App\Http\Controllers\VideosCampeonatosController;
use App\Models\GaleriaCampeonatos;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Auth::routes();
Route::name('admin.')->prefix('admin_lasombrilla')->middleware(['auth'])->group(function () {
    Route::get('/inicio',[App\Http\Controllers\HomeController::class, 'index'])->name('home');

    Route::resource('slider',SliderController::class)->middleware('auth');
    Route::resource('titulo',TituloController::class)->middleware('auth');
    Route::resource('genetica',GeneticaController::class)->middleware('auth');
    Route::resource('titulosventa',TitulosventaController::class)->middleware('auth');
    Route::resource('indexcaballos',IndexcaballosController::class)->middleware('auth');
    Route::resource('informacion',IndexinformacionController::class)->middleware('auth');
    Route::resource('nosotros',NosotrosController::class)->middleware('auth');
    Route::resource('portadanosotros',PortadanosotrosController::class)->middleware('auth');
    Route::resource('geneticabrahman',GeneticabrahmanController::class)->middleware('auth');
    Route::resource('geneticasuizbu',GeneticasuizbuController::class)->middleware('auth');
    Route::resource('ventajasbrahman',VentajasbrahmanController::class)->middleware('auth');
    Route::resource('ventajassuizbu',VentajassuizbuController::class)->middleware('auth');
    Route::resource('imagenventajasbrahman',ImagenventajasbrahmanController::class)->middleware('auth');
    Route::resource('imagenventajasuizbu',ImagenventajasuizbuController::class)->middleware('auth');
    Route::resource('caballocuartodemilla',CaballocuartodemillaController::class)->middleware('auth');
    Route::resource('preguntasfrecuente',PreguntasfrecuenteController::class)->middleware('auth');

    Route::resource('contacto',ContactoController::class)->middleware('auth');
    Route::resource('galeria1',Galeria1Controller::class)->middleware('auth');
    Route::resource('galeria2',Galeria2Controller::class)->middleware('auth');
    Route::resource('galeria3',Galeria3Controller::class)->middleware('auth');
    Route::resource('galeria4',Galeria4Controller::class)->middleware('auth');
    Route::resource('videosyt',VideosytController::class)->middleware('auth');
    Route::resource('fotocontacto',FotocontactoController::class)->middleware('auth');

    Route::post('categoria/datatable',[CategoriasController::class,'datatable'])->name('dt_categorias');
    Route::resource('categoria',CategoriasController::class)->middleware('auth');

    Route::prefix('categoria/{categoria_id}')->group(function () {
         Route::resource('subcategoria',SubcategoriasController::class);
    });

    Route::post('animales/datatable',[AnimalesController::class,'datatable'])->name('dt_animales');
    Route::post('animales/subcategorias',[AnimalesController::class,'subcategorias'])->name('animales_subcategorias');
    Route::resource('animales', AnimalesController::class);
   
    Route::prefix('animales/{animal_id}')->group(function(){
        Route::post('galeria/datatable',[GaleriaController::class,'datatable'])->name('dt_galeria');
        Route::resource('galeria',GaleriaController::class);
    });
    
    Route::prefix('videosyt/{video_id}')->group(function(){
        Route::post('galeriareceptoras/datatable',[GaleriareceptorasController::class,'datatable'])->name('dt_galeriareceptoras');
        Route::resource('galeriareceptoras',GaleriareceptorasController::class);
    });

    Route::post('videosyt/datatable',[VideosytController::class,'datatable'])->name('dt_videosyt');
    Route::post('contacto/datatable',[ContactoController::class, 'datatable'])->name('dt_contacto');

    Route::prefix('fierros')->group(function () {
            
        Route::get('/',[FierrosController::class,'index'])->name('fierro');
        Route::get('crear',[FierrosController::class,'crear'])->name('crear_fierro');
        Route::post('guardar',[FierrosController::class, 'guardar'])->name('guardar_fierro');
        Route::get('editar/{fierro_id}',[FierrosController::class,'editar'])->name('editar_fierro');
        Route::post('actualizar/{fierro_id}',[FierrosController::class, 'actualizar'])->name('actualizar_fierro');
        Route::post('eliminar/{fierro_id}',[FierrosController::class, 'eliminar'])->name('eliminar_fierro');
        Route::post('datatable',[FierrosController::class,'datatable'])->name('dt_fierro');

    });
    
    Route::prefix('campeonatos')->group(function () {
            
        Route::get('/',[CampeonatosController::class,'index'])->name('campeonato');
        Route::get('crear',[CampeonatosController::class,'crear'])->name('crear_campeonato');
        Route::post('guardar',[CampeonatosController::class, 'guardar'])->name('guardar_campeonato');
        Route::get('editar/{campeonato_id}',[CampeonatosController::class,'editar'])->name('editar_campeonato');
        Route::post('actualizar/{campeonato_id}',[CampeonatosController::class, 'actualizar'])->name('actualizar_campeonato');
        Route::post('eliminar/{campeonato_id}',[CampeonatosController::class, 'eliminar'])->name('eliminar_campeonato');
        Route::post('datatable',[CampeonatosController::class,'datatable'])->name('dt_campeonato');
            
        Route::prefix('{campeonato_id}/galeria_campeonatos')->group(function () {
            
            Route::get('/',[GaleriaCampeonatosController::class,'index'])->name('galeria_campeonato');
            Route::get('crear',[GaleriaCampeonatosController::class,'crear'])->name('crear_galeria_campeonato');
            Route::post('guardar',[GaleriaCampeonatosController::class, 'guardar'])->name('guardar_galeria_campeonato');
            Route::get('editar/{galeria_campeonato_id}',[GaleriaCampeonatosController::class,'editar'])->name('editar_galeria_campeonato');
            Route::post('actualizar/{galeria_campeonato_id}',[GaleriaCampeonatosController::class, 'actualizar'])->name('actualizar_galeria_campeonato');
            Route::post('eliminar/{galeria_campeonato_id}',[GaleriaCampeonatosController::class, 'eliminar'])->name('eliminar_galeria_campeonato');
            Route::post('datatable',[GaleriaCampeonatosController::class,'datatable'])->name('dt_galeria-campeonato');

        });
        Route::prefix('{campeonato_id}/video_campeonatos')->group(function () {
            
            Route::get('/',[VideosCampeonatosController::class,'index'])->name('video_campeonato');
            Route::get('crear',[VideosCampeonatosController::class,'crear'])->name('crear_video_campeonato');
            Route::post('guardar',[VideosCampeonatosController::class, 'guardar'])->name('guardar_video_campeonato');
            Route::get('editar/{video_campeonato_id}',[VideosCampeonatosController::class,'editar'])->name('editar_video_campeonato');
            Route::post('actualizar/{video_campeonato_id}',[VideosCampeonatosController::class, 'actualizar'])->name('actualizar_video_campeonato');
            Route::post('eliminar/{video_campeonato_id}',[VideosCampeonatosController::class, 'eliminar'])->name('eliminar_video_campeonato');
            Route::post('datatable',[VideosCampeonatosController::class,'datatable'])->name('dt_video-campeonato');

        });

    });
});