@extends('layouts.caballos')
@section('title', 'Receptoras F1 | Rancho La Sombrilla')
@section('metas')
<meta name="language" content="spanish">
<meta name="copyright" content="Rancho La Sombrilla">
<meta name="author" content="CYBAC">
<meta name="audience" content="all">
<meta name="description" content="Nos caracterizamos por producir excelentes receptoras de la
raza F1.">
<meta name="keywords" content="rancho, sombrilla, receptoras F1">
<meta name="robots" content="index, all, follow">
<meta name="category" content="ranch">
@show
@section('main')
<div class="top_panel_title top_panel_style_1 title_present breadcrumbs_present scheme_original">
    <div class="bg_cust_12 top_panel_title_inner top_panel_inner_style_1  title_present_inner breadcrumbs_present_inner"
        style="background-color: #ffffff !important; @mobile margin-top: 5rem !important; @endmobile">
        <div class="content_wrap">
            <h1 class="page_title" style="color: #2b4276;">Receptoras F1</h1>
        </div>
    </div>
</div>
<div class="page_content_wrap page_paddings_yes fondo @mobile page_content_wrap_m @endmobile">
    <div class="content_wrap @mobile tamaño_m @endmobile">
        <div class="content">
            <div class="sc_section_content_wrap">
                <div class="sc_section sc_section_block margin_bottom_large aligncenter mw800">
                    <div class="sc_section_inner">
                        <div class="sc_section_content_wrap">
                            <h3 class="sc_highlight fst_1">Nos caracterizamos por producir excelentes receptoras de la
                                raza F1</h3>
                        </div>
                        <div class="sc_section_content_wrap">
                            <p class="sc_highlight fst_1">Mediante nuestros programas reproductivos de inseminación
                                artificial IA, FIV y montas directas en donde seleccionamos los mejores semantales para
                                cada una de los vientres que forman parte del hato. Incrementos en pesos al destete
                                garantizados.</p>
                        </div>
                    </div>
                </div>
                <div class="vc_empty_space h_6r">
                    <span class="vc_empty_space_inner"></span>
                </div>
            </div>
            <article
                class="post_item post_item_single post_featured_default post_format_standard page type-page hentry">
                <section class="post_content">
                    <div class="vc_row wpb_row vc_row-fluid">
                        <div class="wpb_column vc_column_container vc_col-sm-12">
                            <div class="vc_column-inner ">
                                <div class="sc_section_inner">
                                    <h2 class="sc_section_title sc_item_title sc_item_title_without_descr"
                                        style="margin-top: 0px">Videos<span></span></h2>
                                </div>
                                <div class="wpb_wrapper">
                                    @if (count($videosyts) > 0)
                                    <div class="owl-carousel">
                                        @foreach ($videosyts as $videosyt)
                                        <div class="post_featured img ">
                                            <a data-fancybox="receptora-{{ $videosyt->id }}"
                                                href="{{ $videosyt->enlace}}">
                                                <img alt="{{ $videosyt->nombre }}" width="100%" src="{{ asset('storage').'/'.$videosyt->miniatura }}">
                                            </a>
                                            @foreach ($galeriareceptoras->where('video_id',$videosyt->id) as $item)
                                            <a class="hidden" data-fancybox="receptora-{{ $videosyt->id }}"
                                                href="{{ asset('storage/receptoras/galeria/'.$item->imagen)}}">
                                                <img alt="{{ $videosyt->nombre }}" width="100%" src="{{ asset('storage/receptoras/galeria/'.$item->imagen)}}">
                                            </a>
                                            @endforeach
                                        </div>
                                        @endforeach
                                    </div>
                                    @else
                                    <h4 class="sc_section_title sc_item_title_without_descr"
                                        style="text-align: center; margin-bottom: 200px;">Próximamente más contenido en
                                        venta</h4>
                                    @endif
                                </div>
                                <div class="sc_section_inner" style="margin-top: 100px;">
                                    <h2 class="sc_section_title sc_item_title sc_item_title_without_descr"
                                        style="margin-top: 0px">Fotos<span></span></h2>
                                </div>
                                <div class="wpb_wrapper">
                                    @if (count($videosyts) > 0)
                                    <div class="owl-carousel">
                                        @foreach ($videosyts as $videosyt)
                                        <div class="post_featured img ">
                                            @foreach ($galeriareceptoras->where('video_id',$videosyt->id) as $item)
                                            <a class="{{ $loop->first ? "" : "hide" }}" data-fancybox="receptora-{{ $videosyt->id }}" href="{{ asset('storage/receptoras/galeria/'.$item->imagen)}}">
                                                <img alt="{{ $videosyt->nombre }}" width="100%" src="{{ asset('storage/receptoras/galeria/'.$item->imagen)}}">
                                            </a>
                                            @endforeach
                                        </div>
                                        @endforeach
                                    </div>
                                    @else
                                    <h4 class="sc_section_title sc_item_title_without_descr"
                                        style="text-align: center; margin-bottom: 200px;">Próximamente más contenido en
                                        venta</h4>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </article>
            <section class="related_wrap related_wrap_empty"></section>
        </div>
    </div>
</div>
@endsection
