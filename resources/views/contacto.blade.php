@extends('layouts.web')
@section('title', 'Contacto | Rancho La Sombrilla')
@section('metas')
<meta name="language" content="spanish">
<meta name="copyright" content="Rancho La Sombrilla">
<meta name="author" content="CYBAC">
<meta name="audience" content="all">
<meta name="description" content="¿Necesitas Ayuda? Contáctanos, e-mail o vía telefónica y te brindaremos la información que necesitas.">
<meta name="keywords" content="rancho, sombrilla, contactanos, contacto, correo, telefono, direccion">
<meta name="robots" content="index, all, follow">
<meta name="category" content="ranch">
@show
@section('main')
<div class="top_panel_title top_panel_style_1 title_present breadcrumbs_present scheme_original">
    <div  class="bg_cust_11 top_panel_title_inner top_panel_inner_style_1 title_present_inner breadcrumbs_present_inner" style="background-color: #ffffff !important; @mobile margin-top: 5rem !important; @endmobile">
        <div class="content_wrap">
            <h1 class="page_title" style="color: #2b4276;">Contacto</h1>
        </div>
    </div>
</div>
<div class="page_content_wrap page_paddings_no">
    <div class="sc_section_content_wrap">
        <div class="sc_promo sc_promo_size_large">
            <div class="sc_promo_inner">
                <div class="sc_promo_image" style="background-image:url({{ asset('storage').'/'.$portadacontacto->portada }}) !important;"></div>
                <div class="sc_promo_block sc_align_left">
                    <div class="sc_promo_block_inner">
                        <div id="sc_form_282_wrap" class="sc_form_wrap">
                            <div id="sc_form_282" class="sc_form sc_form_style_form_1">
                                <h2 class="sc_form_title sc_item_title @mobile sc_item_title1 @endmobile sc_item_title0 sc_item_title_without_descr">Contáctanos</h2>
                                <form class="form" method="post" action="{{ route('enviar_mensaje') }}">
                                    @csrf
                                    <div class="sc_form_info">
                                        <div class="sc_form_item sc_form_field label_over">
                                            <input type="text" name="nombre" placeholder="Nombre" id="nombre">
                                            <span class="invalid-feedback"></span>
                                        </div>
                                        <div class="sc_form_item sc_form_field label_over">
                                            <input type="text" name="correo" placeholder="Correo Electronico" id="correo">
                                            <span class="invalid-feedback"></span>
                                        </div>
                                    </div>
                                    <div class="sc_form_item sc_form_message">
                                        <textarea name="mensaje" placeholder="Mensaje" id="mensaje"></textarea>
                                        <span class="invalid-feedback"></span>
                                    </div>
                                    <div class="sc_form_item sc_form_button">
                                        <button type="submit">Enviar</button>
                                    </div>
                                    <div class="result sc_infobox"></div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content_wrap">
        <div class="content">
            <article class="post_item post_item_single post_featured_default post_format_standard page type-page hentry">
                <section class="post_content">
                    <div class="vc_row wpb_row vc_row-fluid">
                        <div class="wpb_column vc_column_container vc_col-sm-12">
                            <div class="vc_column-inner ">
                                <div class="wpb_wrapper">
                                    <div class="columns_wrap sc_columns columns_nofluid sc_columns_count_3 margin_top_huge margin_bottom_huge @mobile margin_top_huge1 @endmobile">
                                        <div class="column-1_3 sc_column_item sc_column_item_2 even centext">
                                            <div class="sc_section section_style_bordered_section">
                                                <div class="sc_section_inner" style="margin-bottom: 1.28em;">
                                                    <div class="sc_section_content_wrap">
                                                        <h4 class="sc_title sc_title_regular sc_align_center">Teléfono México #1</h4>
                                                        <span class="sc_highlight" style="font-size: 1.10em;">{{ $contacto->telefono }}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="column-1_3 sc_column_item sc_column_item_2 even centext">
                                            <div class="sc_section section_style_bordered_section">
                                                <div class="sc_section_inner" style="margin-bottom: 1.28em;">
                                                    <div class="sc_section_content_wrap">
                                                        <h4 class="sc_title sc_title_regular sc_align_center">Teléfono México #2</h4>
                                                        <span class="sc_highlight" style="font-size: 1.10em;">{{ $contacto->telefono2 }}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="column-1_3 sc_column_item sc_column_item_2 even centext">
                                            <div class="sc_section section_style_bordered_section">
                                                <div class="sc_section_inner" style="margin-bottom: 1.28em;">
                                                    <div class="sc_section_content_wrap">
                                                        <h4 class="sc_title sc_title_regular sc_align_center">Teléfono USA</h4>
                                                        <span class="sc_highlight" style="font-size: 1.10em;">{{ $contacto->telefono3 }}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="column-1_2 sc_column_item sc_column_item_3 odd centext" style="margin-top: 20px;">
                                            <div class="sc_section section_style_bordered_section">
                                                <div class="sc_section_inner" style="margin-bottom: 1.28em;">
                                                    <div class="sc_section_content_wrap">
                                                        <h4 class="sc_title sc_title_regular sc_align_center">Correo Electrónico</h4>
                                                        <span class="sc_highlight" style="font-size: 1.10em;"> {{ $contacto->correo }}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="column-1_2 sc_column_item sc_column_item_1 odd first centext" style="margin-top: 20px;">
                                            <div class="sc_section section_style_bordered_section">
                                                <div class="sc_section_inner" style="margin-bottom: 1.28em;">
                                                    <div class="sc_section_content_wrap">
                                                        <h4 class="sc_title sc_title_regular sc_align_center">Dirección</h4>
                                                        <span class="sc_highlight" style="font-size: 1.10em;">{{ $contacto->direccion }}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </section>
            </article>
            <section class="related_wrap related_wrap_empty"></section>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script src="{{ asset('administrador/js/sweetalert2.min.js') }}"></script>
    <script src="{{ asset('administrador/js/app.js') }}"></script>
@endsection