@extends('layouts.caballos')
@section('title', 'Cuartos de Milla | Rancho La Sombrilla')
@section('metas')
<meta name="language" content="spanish">
<meta name="copyright" content="Rancho La Sombrilla">
<meta name="author" content="CYBAC">
<meta name="audience" content="all">
<meta name="description" content="Nos caracterizamos por producir excelentes ejemplares de la raza de Caballos Cuarto de Milla.">
<meta name="keywords" content="rancho, sombrilla, caballos, caballo, cuarto de milla, raza cuarto de milla">
<meta name="robots" content="index, all, follow">
<meta name="category" content="ranch">
@show
@section('main')
<div class="top_panel_title top_panel_style_1 title_present breadcrumbs_present scheme_original">
    <div class="top_panel_title_inner top_panel_inner_style_1 title_present_inner breadcrumbs_present_inner" style="background-color: #ffffff; @mobile margin-top: 5rem !important; @endmobile">
        <div class="content_wrap">
            <h1 class="page_title" style="color: #2b4276;">Caballos Cuartos de Milla</h1>
        </div>
    </div>
</div>
<div class="page_content_wrap page_paddings_no">
    <div class="content_wrap">
        <div class="content">
            <article
                class="post_item post_item_single post_featured_default post_format_standard page type-page hentry">
                <section class="post_content">
                    <div data-vc-full-width="true" data-vc-full-width-init="false"
                        class="vc_row wpb_row vc_row-fluid vc_custom_1475048754773 inverse_colors" style="background-image:url({{ asset('storage').'/'.$caballocuartodemillas->slider }}) !important;">
                        <div class="wpb_column vc_column_container vc_col-sm-1">
                            <div class="vc_column-inner ">
                                <div class="wpb_wrapper"></div>
                            </div>
                        </div>
                        <div class="wpb_column vc_column_container vc_col-sm-4 @mobile ocultar_movil @endmobile">
                            <div class="vc_column-inner vc_custom_1475049444491">
                                <div class="wpb_wrapper">
                                    <figure class="sc_image sc_image_shape_square margin_top_huge  @mobile sc_item_title1 @endmobile"
                                        data-animation="animated fadeInLeft normal">
                                        <img src="{{ asset('storage').'/'.$caballocuartodemillas->imagen }}" width="100%" alt="Caballo Cuarto de Milla" />
                                    </figure>
                                </div>
                            </div>
                        </div>
                        <div class="wpb_column vc_column_container vc_col-sm-1">
                            <div class="vc_column-inner ">
                                <div class="wpb_wrapper"></div>
                            </div>
                        </div>
                        <div class="wpb_column vc_column_container vc_col-sm-6">
                            <div class="vc_column-inner vc_custom_1475049863739">
                                <div class="wpb_wrapper">
                                    <div class="sc_section sc_section_block "
                                        data-animation="animated fadeInRight normal">
                                        <div class="sc_section_inner">
                                            <div class="sc_section_content_wrap">
                                                <div class="sc_section margin_bottom_tiny- alignleft">
                                                    <div class="sc_section_inner">
                                                        <h2
                                                            class="sc_section_title sc_item_title sc_item_title3-1 sc_item_title_without_descr @mobile sc_item_title_m @endmobile">
                                                            Caballos Cuarto de Milla<span></span></h2>
                                                        <div class="sc_section_content_wrap"></div>
                                                    </div>
                                                </div>
                                                <div class="sc_section margin_bottom_huge section_style_call_section">
                                                    <div class="sc_section_inner">
                                                        <h2
                                                            class="sc_section_title sc_item_title sc_item_title_without_descr">{{ $caballocuartodemillas->subtitulo}}<span></span></h2>
                                                        <div class="sc_section_descr sc_item_descr" style="text-align: justify;">{{ $caballocuartodemillas->parrafo}}</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="vc_row-full-width"></div>
                </section>
            </article>
            <section class="related_wrap related_wrap_empty"></section>
        </div>
    </div>
</div>
<div class="page_content_wrap page_paddings_yes" style="padding-bottom: 0;">
    <div class="content_wrap">
        <div class="content">
            <article
                class="post_item post_item_single post_featured_default post_format_standard page type-page hentry">
                <section class="post_content">
                    <div class="vc_row wpb_row vc_row-fluid">
                        <div class="wpb_column vc_column_container vc_col-sm-12">
                            <div class="vc_column-inner " style="padding-top: 150px; padding-bottom: 150px; @mobile padding-top: 0px !important; padding-bottom: 50px !important; @endmobile">
                                <div class="content_wrap">
                                    <div class="sc_section_inner">
                                        <h2 class="sc_section_title sc_item_title sc_item_title3 sc_item_title_without_descr" style="margin-top: 0px">
                                            Galeria de Caballos Cuarto de Milla<span></span></h2>
                                    </div>
                                </div>
                                <div class="wpb_wrapper">
                                    <div class="owl-carousel owl-theme owl-loaded">
                                        <div class=" owl-stage-outer">
                                            <div class="owl-stage">
                                                @foreach ($galeria3s as $galeria3)
                                                @foreach ($categorias as $categoria)
                                                @if ($galeria3->categoria == $categoria->id && $categoria->id == '3')
                                                <div class="owl-item">
                                                    <div class="post_featured img ">
                                                        <a data-fancybox="brahman" href="{{ asset('storage').'/'.$galeria3->imagen }}">
                                                            <img alt="{{ $galeria3->nombre }}" width="100%" src="{{ asset('storage').'/'.$galeria3->imagen }}">
                                                        </a> 
                                                    </div>
                                                </div>     
                                                @endif
                                                @endforeach
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>      
                            </div>
                        </div>
                </section>
            </article>
            <section class="related_wrap related_wrap_empty"></section>
        </div>
    </div>
</div>
@endsection