@extends('layouts.web')
@section('title', 'Suizbu | Rancho La Sombrilla')
@section('metas')
<meta name="language" content="spanish">
<meta name="copyright" content="Rancho La Sombrilla">
<meta name="author" content="CYBAC">
<meta name="audience" content="all">
<meta name="description" content="Genética Suizbu">
<meta name="keywords" content="rancho, sombrilla, suizbu, raza suizbu, ganado suizbu">
<meta name="robots" content="index, all, follow">
<meta name="category" content="ranch">
@show
@section('main')
<div class="top_panel_title top_panel_style_1 title_present breadcrumbs_present scheme_original">
    <div class="top_panel_title_inner top_panel_inner_style_1 title_present_inner breadcrumbs_present_inner" style="background-color: #ffffff; @mobile margin-top: 5rem !important; @endmobile">
        <div class="content_wrap">
            <h1 class="page_title" style="color: #2b4276;">Raza Suizbu</h1>
        </div>
    </div>
</div>
<div class="page_content_wrap page_paddings_no">
    <div class="content_wrap">
        <div class="content">
            <article
                class="post_item post_item_single post_featured_default post_format_standard page type-page hentry">
                <section class="post_content">
                    <div data-vc-full-width="true" data-vc-full-width-init="false"
                        class="vc_row wpb_row vc_row-fluid vc_custom_1475048754773-2 inverse_colors @mobile padding_20_20 @endmobile" style="background-image:url({{ asset('storage').'/'.$geneticasuizbus->slider }}) !important; background-attachment: fixed !important; padding-top: 50px !important; padding-bottom: 50px !important;">
                        <div class="wpb_column vc_column_container vc_col-sm-1">
                            <div class="vc_column-inner ">
                                <div class="wpb_wrapper"></div>
                            </div>
                        </div>
                        <div class="wpb_column vc_column_container vc_col-sm-4 @mobile ocultar_movil @endmobile">
                            <div class="vc_column-inner vc_custom_1475049444491">
                                <div class="wpb_wrapper">
                                    <figure class="sc_image  sc_image_shape_square margin_top_huge"
                                        data-animation="animated fadeInLeft normal" style="margin-top: 9rem !important;">
                                        <img src="{{ asset('storage').'/'.$geneticasuizbus->imagen }}" width="100%" alt="Ganado Suizbu" />
                                    </figure>
                                </div>
                            </div>
                        </div>
                        <div class="wpb_column vc_column_container vc_col-sm-1" @mobile style="display: none;" @endmobile>
                            <div class="vc_column-inner ">
                                <div class="wpb_wrapper"></div>
                            </div>
                        </div>
                        <div class="wpb_column vc_column_container vc_col-sm-6">
                            <div class="vc_column-inner vc_custom_1475049863739 @mobile vc_custom_1475049863739_m @endmobile">
                                <div class="wpb_wrapper">
                                    <div class="sc_section sc_section_block "
                                        data-animation="animated fadeInRight normal">
                                        <div class="sc_section_inner">
                                            <div class="sc_section_content_wrap">
                                                <div class="sc_section margin_bottom_tiny- alignleft @mobile alignleft3_m @endmobile">
                                                    <div class="sc_section_inner">
                                                        <h2
                                                            class="sc_section_title sc_item_title sc_item_title4-2 sc_item_title_without_descr espaciopantalla @mobile espaciopantalla_m sc_item_title_m @endmobile">
                                                            Genética Suizbu<span></span></h2>
                                                        <div class="sc_section_content_wrap"></div>
                                                    </div>
                                                </div>
                                                <div class="sc_section margin_bottom_huge section_style_call_section">
                                                    <div class="sc_section_inner">
                                                        <h2
                                                            class="sc_section_title sc_item_title sc_item_title_without_descr">{{ $geneticasuizbus->subtitulo}}<span></span></h2>
                                                        <div class="sc_section_descr sc_item_descr" style="text-align: justify;">{{ $geneticasuizbus->parrafo}}</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="vc_row-full-width"></div>
                </section>
            </article>
            <section class="related_wrap related_wrap_empty"></section>
        </div>
    </div>
</div>
<div class="page_content_wrap page_paddings_no">
    <div class="content_wrap">
        <div class="content">
            <article
                class="post_item post_item_single post_featured_default post_format_standard page type-page hentry">
                <section class="post_content">
                    <div data-vc-full-width="true" data-vc-full-width-init="false"
                        class="vc_row wpb_row vc_row-fluid vc_custom_1475054487397 inverse_colors @mobile padding_20_20 @endmobile">
                        <div class="wpb_column vc_column_container vc_col-sm-12">
                            <div class="vc_column-inner vc_custom_1475060512699">
                                <div class="wpb_wrapper">
                                    <div class="sc_section section_style_white_text c_white"
                                        data-animation="animated fadeInUp normal">
                                        <div class="sc_section_inner">
                                            <h2 class="sc_section_title sc_item_title sc_item_title_without_descr">
                                                Ventajas Económicas<span></span></h2>
                                            <div class="sc_section_content_wrap">
                                                <div id="sc_services_438_wrap" class="sc_services_wrap">
                                                    <div id="sc_services_438"
                                                        class="sc_services sc_services_style_services-5 sc_services_type_icons margin_bottom_large">
                                                        <div class="sc_service_container sc_align_">
                                                            <div class="sc_services_image">
                                                                <img src="{{ asset('storage').'/'.$imagenventajasuizbus->ventajas_economicas }}" width="100%" alt="Semental Suizbu">
                                                            </div>
                                                            <div class="sc_columns columns_wrap">
                                                                @foreach ($ventajassuizbus as $ventajassuizbu)
                                                                @if ($ventajassuizbu->id == '1')
                                                                <div class="column-1_2 column_padding_bottom" @mobile style="margin-bottom: 25px" @endmobile>
                                                                    <div id="sc_services_438_1"
                                                                        class="sc_services_item sc_services_item_1 odd first">
                                                                        <a class="@mobile ocultar_movil @endmobile">
                                                                            <span class="sc_icon icon-book-open"></span>
                                                                        </a>
                                                                        <div class="sc_services_item_content">
                                                                            <h4 class="sc_services_item_title tituloventaja">
                                                                                <a>{{ $ventajassuizbu->titulo}}</a>
                                                                            </h4>
                                                                            <div class="sc_services_item_description espaciosuizbu">
                                                                                <p class="textoventajas alinacionventajaleft">{{ $ventajassuizbu->parrafo}}</p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                @endif
                                                                @endforeach
                                                                @foreach ($ventajassuizbus as $ventajassuizbu)
                                                                @if ($ventajassuizbu->id == '2')
                                                                <div class="column-1_2 column_padding_bottom" @mobile style="margin-bottom: 25px" @endmobile>
                                                                    <div id="sc_services_438_1"
                                                                        class="sc_services_item sc_services_item_1 odd first">
                                                                        <a class="@mobile ocultar_movil @endmobile">
                                                                            <span class="sc_icon icon-book-open"></span>
                                                                        </a>
                                                                        <div class="sc_services_item_content">
                                                                            <h4 class="sc_services_item_title tituloventaja">
                                                                                <a>{{ $ventajassuizbu->titulo}}</a>
                                                                            </h4>
                                                                            <div class="sc_services_item_description">
                                                                                <p class="textoventajas alinacionventajaright">{{ $ventajassuizbu->parrafo}}</p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                @endif
                                                                @endforeach
                                                                @foreach ($ventajassuizbus as $ventajassuizbu)
                                                                @if ($ventajassuizbu->id == '3')
                                                                <div class="column-1_2 column_padding_bottom" @mobile style="margin-bottom: 25px" @endmobile>
                                                                    <div id="sc_services_438_1"
                                                                        class="sc_services_item sc_services_item_1 odd first">
                                                                        <a class="@mobile ocultar_movil @endmobile">
                                                                            <span class="sc_icon icon-book-open"></span>
                                                                        </a>
                                                                        <div class="sc_services_item_content">
                                                                            <h4 class="sc_services_item_title tituloventaja">
                                                                                <a>{{ $ventajassuizbu->titulo}}</a>
                                                                            </h4>
                                                                            <div class="sc_services_item_description">
                                                                                <p class="textoventajas alinacionventajaleft">{{ $ventajassuizbu->parrafo}}</p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                @endif
                                                                @endforeach
                                                                {{-- @foreach ($ventajassuizbus as $ventajassuizbu)
                                                                @if ($ventajassuizbu->id == '4')
                                                                <div class="column-1_2 column_padding_bottom">
                                                                    <div id="sc_services_438_1"
                                                                        class="sc_services_item sc_services_item_1 odd first">
                                                                        <a>
                                                                            <span class="sc_icon icon-book-open"></span>
                                                                        </a>
                                                                        <div class="sc_services_item_content">
                                                                            <h4 class="sc_services_item_title">
                                                                                <a>{{ $ventajassuizbu->titulo}}</a>
                                                                            </h4>
                                                                            <div class="sc_services_item_description">
                                                                                <p style="text-align: right;">{{ $ventajassuizbu->parrafo}}</p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                @endif
                                                                @endforeach --}}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="vc_row-full-width"></div>
                    <div class="vc_row wpb_row vc_row-fluid">
                        <div class="wpb_column vc_column_container vc_col-sm-12">
                            <div class="vc_column-inner " style="padding-top: 150px; padding-bottom: 150px;">
                                <div class="content_wrap">
                                    <div class="sc_section_inner">
                                        <h2 class="sc_section_title sc_item_title sc_item_title_without_descr" style="margin-top: 0px">
                                            Galeria Suizbu<span></span></h2>
                                    </div>
                                </div>
                                <div class="wpb_wrapper">
                                    <div class="owl-carousel owl-theme owl-loaded">
                                        <div class=" owl-stage-outer">
                                            <div class="owl-stage">
                                                @foreach ($galeria2s as $galeria2)
                                                @foreach ($categorias as $categoria)
                                                @if ($galeria2->categoria == $categoria->id && $categoria->id == '2')
                                                <div class="owl-item">
                                                    <div class="post_featured img ">
                                                        <a data-fancybox="brahman" href="{{ asset('storage').'/'.$galeria2->imagen }}">
                                                            <img alt="{{ $galeria2->nombre }}" width="100%" src="{{ asset('storage').'/'.$galeria2->imagen }}">
                                                        </a> 
                                                    </div>
                                                </div>     
                                                @endif
                                                @endforeach
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </article>
        </div>
    </div>
</div>
<section class="related_wrap related_wrap_empty"></section>
@endsection
