<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Correo de Contacto</title>
    <style>
        html,body{
            width: 100%;
            height: 100%;
            margin: 0;
            padding: 0;
            
        }
        #contenido{ 
            background-color: #b7c3a8;
            max-width: 600px;
            height: auto;
            margin: auto;
            display: flex;
            justify-content: center;
            flex-wrap: wrap;
            align-content: baseline;
            border-radius: 50px;
            padding: 20px;
            color: #2b4276;
        }
        #logo{
            margin-top: 50px; 
        }
        .w-100{
            width: 100%;
        }
        .text-center{
            text-align: center;
        }
        .text-justify{
            text-align: justify;
        }
        .px-2{
            padding-left: 2rem;
            padding-right: 2rem;
        }
    </style>
</head>
<body>
    <div id="contenido">
        <div id="logo" class="w-100 text-center">
            <img src="{{ asset('images/logo.png') }}" alt="Logo">
        </div> 
        <h1 class="text-center px-2">¡Has recibido un mensaje de contacto de {{ $datos->nombre }}!</h1>
        <div class="text-justify w-100 px-2">
            {{ $datos->mensaje }}
        </div> 
    </div>
</body>
</html>