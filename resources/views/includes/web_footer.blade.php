<footer class="footer_wrap widget_area scheme_original">
    <div class="footer_wrap_inner widget_area_inner">
        <div class="content_wrap">
            <div class="columns_wrap">
                <aside class="column-1_4 widget widget_socials">
                    <div class="widget_inner">
                        <div class="logo" style="text-align: center">
                            <a href="{{route('index')}}">
                                <img src="{{ asset('images/footer.png') }}" class="logo_main" alt="Rancho La Sombrilla Footer" width="74" height="74">
                            </a>
                        </div>
                    </div>
                </aside>
                <aside class="column-2_4 widget widget_text" style="text-align: center">
                    <div class="textwidget">Dirección: {{ $contacto->direccion }}
                        <br>
                        <span class="accent1">Telefonos: <br> <img src="{{ asset('images/mex.png') }}" alt="" style="margin-right: 5px"> {{ $contacto->telefono }} <br> <img src="{{ asset('images/mex.png') }}" alt="México" style="margin-right: 5px"> {{ $contacto->telefono2 }} <br> <img src="{{ asset('images/usa.png') }}" alt="México" style="margin-right: 5px"> {{ $contacto->telefono3 }}</span>
                        <br>Correo Electrónico: <a href="mailto:{{ $contacto->correo }}">{{ $contacto->correo }}</a>
                    </div>
                </aside>
                {{-- <aside class="column-1_4 widget widget_text">
                    <div class="textwidget"></div>
                </aside> --}}
                <aside class="column-1_4 widget widget_socials">
                    <div class="widget_inner" style="text-align: center">
                        <div
                            class="sc_socials sc_socials_type_icons sc_socials_shape_round sc_socials_size_tiny">
                            <div class="sc_socials_item">
                                <a href="{{ $contacto->youtube }}" target="_blank" class="social_icons social_twitter">
                                    <i class="fab fa-youtube" alt="Youtube" style="margin: 8px;"></i>
                                </a>
                            </div>
                            <div class="sc_socials_item">
                                <a href="{{ $contacto->facebook }}" target="_blank" class="social_icons social_facebook">
                                    <span class="icon-facebook"></span>
                                </a>
                            </div>
                            <div class="sc_socials_item">
                                <a href="{{ $contacto->instagram }}" target="_blank" class="social_icons social_gplus-1">
                                    <i class="fab fa-instagram" alt="Instagram" style="margin: 8px;"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </aside>
            </div>
        </div>
    </div>
</footer>
<div class="copyright_wrap copyright_style_menu scheme_original">
    <div class="copyright_wrap_inner">
        <div class="content_wrap">
            <div class="copyright_text">
                <p>Derechos Reservados "Rancho La Sombrilla 2021" | Hosting y Diseño <a
                        href="https://www.grupocybac.com/">CYBAC</a>
                </p>
            </div>
        </div>
    </div>
</div>