<li class="menu-item menu-item-has-children">
    <a href="#"><span>Ventas</span></a>
    <ul class="sub-menu">
        @foreach ($categorias->where('id','<>',4) as $item)
        <li class="menu-item menu-item-has-children">
            <a href="#"><span>{{ $item->nombre }}</span></a>
            <ul class="sub-menu">
                @php
                    $subcategorias = \App\Models\Subcategorias::where('categoria_id',$item->id)->get(); 
                @endphp
                @foreach ($subcategorias as $items)
                    <li class="menu-item"><a href="{{ route('ventas',['categoria_id'=>$item->id,'subcategoria_id'=>$items->id])}}"><span>{{ $items->nombre }}</span></a></li>    
                @endforeach
            </ul>
        </li>   
        @endforeach 
        <li class="menu-item"><a href="{{ route('ventasrf1')}}"><span>Receptoras F1</span></a>
        </li> 
    </ul>
</li>