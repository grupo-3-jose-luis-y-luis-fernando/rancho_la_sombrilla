<div id="sidebar">
    <!-- Wrapper for scrolling functionality -->
    <div id="sidebar-scroll">
        <!-- Sidebar Content -->
        <div class="sidebar-content">
            <!-- User Info -->
            <div class="sidebar-section sidebar-user clearfix sidebar-nav-mini-hide">
                <img style="width: 100%" src="{{ asset('images/logo.png')}}">
                <div class="sidebar-user-links" style="margin-top: 10px">
                    <button style="background: transparent; border: none;" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" data-toggle="tooltip" data-placement="bottom" title="Salir">
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">@csrf</form>
                        <a><i class="gi gi-exit"></i> Cerrar sesión</a>
                </div>
            </div>
            <ul class="sidebar-nav">
                <li>
                    <a href="{{ route('admin.home') }}">
                        <span class="sidebar-nav-mini-hide">Inicio del Administrador</span>
                    </a>
                </li>
            </ul>
            <ul class="sidebar-nav">
                <li>
                    <a href="#" class="sidebar-nav-menu">
                        <i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i>
                        <span class="sidebar-nav-mini-hide">Inicio</span>
                    </a>
                    <ul>
                        <li>
                            <a href="{{ route('admin.slider.index') }}">Slider</a>
                        </li>
                        <li>
                            <a href="{{ route('admin.fierro') }}">Logo Principal y Fierros</a>
                        </li>
                        <li>
                            <a href="{{ route('admin.titulo.index') }}">Titulo Principal</a>
                        </li>
                        <li>
                            <a href="{{ route('admin.genetica.index') }}">Genetica</a>
                        </li>
                        <li>
                            <a href="{{ route('admin.titulosventa.index') }}">Ventas</a>
                        </li>
                        <li>
                            <a href="{{ route('admin.indexcaballos.index') }}">Caballos Cuarto de Milla</a>
                        </li>
                        <li>
                            <a href="{{ route('admin.informacion.index') }}">Información</a>
                        </li>
                        <li>
                            <a href="{{ route('admin.nosotros.index') }}">Nosotros</a>
                        </li>
                        <li>
                            <a href="{{ route('admin.portadanosotros.index') }}">Portada de Seccion Nosotros</a>
                        </li>

                    </ul>
                </li>
            </ul>
            <ul class="sidebar-nav">
                <li>
                    <a href="#" class="sidebar-nav-menu">
                        <i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i>
                        <span class="sidebar-nav-mini-hide">Nuestra Genética</span>
                    </a>
                    <ul>
                        <li>
                            <a href="#" class="sidebar-nav-submenu">
                                <i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i>
                                <span class="sidebar-nav-mini-hide">Brahman</span>
                            </a>
                            <ul>
                                <li>
                                    <a href="{{ route('admin.geneticabrahman.index') }}">Descripción</a>
                                </li>
                                <li>
                                    <a href="{{ route('admin.ventajasbrahman.index') }}">Ventajas Economicas</a>
                                </li>
                                <li>
                                    <a href="{{ route('admin.imagenventajasbrahman.index') }}">Imagen de Ventajas Economicas</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="#" class="sidebar-nav-submenu">
                                <i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i>
                                <span class="sidebar-nav-mini-hide">Suizbu</span>
                            </a>
                            <ul>
                                <li>
                                    <a href="{{ route('admin.geneticasuizbu.index') }}">Descripción</a>
                                </li>
                                <li>
                                    <a href="{{ route('admin.ventajassuizbu.index') }}">Ventajas Economicas</a>
                                </li>
                                <li>
                                    <a href="{{ route('admin.imagenventajasuizbu.index') }}">Imagen Ventajas Economicas</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
            </ul>
            <ul class="sidebar-nav">
                <li>
                    <a href="{{ route('admin.caballocuartodemilla.index') }}">
                        <span class="sidebar-nav-mini-hide">Caballos Cuarto de Milla</span>
                    </a>
                </li>
            </ul>
            <ul class="sidebar-nav">
                <li>
                    <a href="{{ route('admin.campeonato') }}">
                        <span class="sidebar-nav-mini-hide">Campeonatos</span>
                    </a>
                </li>
            </ul>
            <ul class="sidebar-nav">
                <li>
                    <a href="{{ route('admin.preguntasfrecuente.index') }}">
                        <span class="sidebar-nav-mini-hide">Preguntas Frecuentes</span>
                    </a>
                </li>
            </ul>
            <ul class="sidebar-nav">
                <li>
                    <a href="#" class="sidebar-nav-menu">
                        <i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i>
                        <span class="sidebar-nav-mini-hide">Ventas</span>
                    </a>
                    <ul>
                        <li>
                            <a href="{{ route('admin.categoria.index') }}">Categorías</a>
                        </li> 
                        <li>
                            <a href="{{ route('admin.animales.index') }}">Animal</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <ul class="sidebar-nav">
                <li>
                    <a href="#" class="sidebar-nav-menu">
                        <i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i>
                        <span class="sidebar-nav-mini-hide">Galería</span>
                    </a>
                    <ul>
                        <li>
                            <a href="{{ route('admin.galeria1.index') }}">Brahman</a>
                        </li> 
                        <li>
                            <a href="{{ route('admin.galeria2.index') }}">Suizbu</a>
                        </li>
                        <li>
                            <a href="{{ route('admin.galeria3.index') }}">Caballos</a>
                        </li>
                        <li>
                            <a href="{{ route('admin.galeria4.index') }}">Rancho</a>
                        </li>
{{--                         <li>
                            <a href="{{ route('admin.galeriareceptoras.index') }}">Receptoras</a>
                        </li> --}}
                    </ul>
                </li>
            </ul>
            <ul class="sidebar-nav">
                <li>
                    <a href="{{ route('admin.videosyt.index') }}">
                        <span class="sidebar-nav-mini-hide">Videos Receptoras F1</span>
                    </a>
                </li>
            </ul> 
            <ul class="sidebar-nav">
                <li>
                    <a href="#" class="sidebar-nav-menu">
                        <i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i>
                        <span class="sidebar-nav-mini-hide">Contacto</span>
                    </a>
                    <ul>
                        <li>
                            <a href="{{ route('admin.contacto.index') }}">Datos de Contacto</a>
                        </li> 
                        <li>
                            <a href="{{ route('admin.fotocontacto.index') }}">Foto de Portada</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div> 
    </div> 
</div>