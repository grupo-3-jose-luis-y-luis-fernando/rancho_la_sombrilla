@extends('layouts.caballos')
@section('title', 'Galería | Rancho La Sombrilla')
@section('metas')
<meta name="language" content="spanish">
<meta name="copyright" content="Rancho La Sombrilla">
<meta name="author" content="CYBAC">
<meta name="audience" content="all">
<meta name="description" content="Visita nuestra galería para ver imágenes de nuestro rancho y de nuestros mejores ejemplares de la raza Suizbu, Brahman Gris y Caballos Cuarto de Milla.">
<meta name="keywords" content="rancho, sombrilla, galeria, imagenes, suizbu, brahman gris, cuarto de milla">
<meta name="robots" content="index, all, follow">
<meta name="category" content="ranch">
@show
@section('main')
<div class="top_panel_title top_panel_style_1 title_present breadcrumbs_present scheme_original">
    <div class="bg_cust_14 top_panel_title_inner top_panel_inner_style_1 title_present_inner breadcrumbs_present_inner" style="background-color: #ffffff !important; @mobile margin-top: 5rem !important; @endmobile">
        <div class="content_wrap">
            <h1 class="page_title" style="color: #2b4276;">Galería</h1>
        </div>
    </div>
</div>
<div class="page_content_wrap page_paddings_yes fondo @mobile page_content_wrap1_m @endmobile">
    <div class="content_wrap">
        <div class="content">
            <article
                class="post_item post_item_single post_featured_default post_format_standard page type-page hentry">
                <section class="post_content">
                    <div class="vc_row wpb_row vc_row-fluid">
                        <div class="wpb_column vc_column_container vc_col-sm-12">
                            <div class="vc_column-inner">
                                <div class="content_wrap">
                                    <div class="sc_section_inner">
                                        <h2 class="sc_section_title sc_item_title sc_item_title2 sc_item_title_without_descr" style="margin-top: 0px">
                                            Galeria Brahman<span></span></h2>
                                    </div>
                                </div>
                                <div class="wpb_wrapper">
                                    <div class="owl-carousel owl-theme owl-loaded">
                                        <div class=" owl-stage-outer">
                                            <div class="owl-stage">
                                                @foreach ($galeria1s as $galeria1)
                                                @foreach ($categorias as $categoria)
                                                @if ($galeria1->categoria == $categoria->id && $categoria->id == '1')
                                                <div class="owl-item">
                                                    <div class="post_featured img ">
                                                        <a data-fancybox="brahman" href="{{ asset('storage').'/'.$galeria1->imagen }}">
                                                            <img alt="{{ $galeria1->nombre }}" width="100%" src="{{ asset('storage').'/'.$galeria1->imagen }}">
                                                        </a> 
                                                    </div>
                                                </div>     
                                                @endif
                                                @endforeach
                
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>     

                                
                            </div>
                        </div>
                    </div>
                </section>
            </article>
            <section class="related_wrap related_wrap_empty"></section>
        </div>
    </div>
</div>
<div class="page_content_wrap page_paddings_yes @mobile page_content_wrap1_m @endmobile">
    <div class="content_wrap">
        <div class="content">
            <article
                class="post_item post_item_single post_featured_default post_format_standard page type-page hentry">
                <section class="post_content">
                    <div class="vc_row wpb_row vc_row-fluid">
                        <div class="wpb_column vc_column_container vc_col-sm-12">
                            <div class="vc_column-inner">
                                <div class="content_wrap">
                                    <div class="sc_section_inner">
                                        <h2 class="sc_section_title sc_item_title sc_item_title_without_descr" style="margin-top: 0px">
                                            Galeria Suizbu<span></span></h2>
                                    </div>
                                </div>
                                <div class="wpb_wrapper">
                                    <div class="owl-carousel owl-theme owl-loaded">
                                        <div class=" owl-stage-outer">
                                            <div class="owl-stage">
                                                @foreach ($galeria2s as $galeria2)
                                                @foreach ($categorias as $categoria)
                                                @if ($galeria2->categoria == $categoria->id && $categoria->id == '2')
                                                <div class="owl-item">
                                                    <div class="post_featured img ">
                                                        <a data-fancybox="suizbu" href="{{ asset('storage').'/'.$galeria2->imagen }}">
                                                            <img alt="{{ $galeria2->nombre }}" width="100%" src="{{ asset('storage').'/'.$galeria2->imagen }}">
                                                        </a> 
                                                    </div>
                                                </div>     
                                                @endif
                                                @endforeach
                
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>     
                            </div>
                        </div>
                    </div>
                </section>
            </article>
            <section class="related_wrap related_wrap_empty"></section>
        </div>
    </div>
</div>
<div class="page_content_wrap page_paddings_yes fondo @mobile page_content_wrap1_m @endmobile">
    <div class="content_wrap">
        <div class="content">
            <article
                class="post_item post_item_single post_featured_default post_format_standard page type-page hentry">
                <section class="post_content">
                    <div class="vc_row wpb_row vc_row-fluid">
                        <div class="wpb_column vc_column_container vc_col-sm-12">
                            <div class="vc_column-inner">
                                <div class="content_wrap">
                                    <div class="sc_section_inner">
                                        <h2 class="sc_section_title sc_item_title sc_item_title3 sc_item_title_without_descr" style="margin-top: 0px">
                                            Galeria de Caballos Cuarto de Milla<span></span></h2>
                                    </div>
                                </div>
                                <div class="wpb_wrapper">
                                    <div class="owl-carousel owl-theme owl-loaded">
                                        <div class=" owl-stage-outer">
                                            <div class="owl-stage">
                                                @foreach ($galeria3s as $galeria3)
                                                @foreach ($categorias as $categoria)
                                                @if ($galeria3->categoria == $categoria->id && $categoria->id == '3')
                                                <div class="owl-item">
                                                    <div class="post_featured img ">
                                                        <a data-fancybox="caballos" href="{{ asset('storage').'/'.$galeria3->imagen }}">
                                                            <img alt="{{ $galeria3->nombre }}" width="100%" src="{{ asset('storage').'/'.$galeria3->imagen }}">
                                                        </a> 
                                                    </div>
                                                </div>     
                                                @endif
                                                @endforeach
                
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>     
                            </div>
                        </div>
                    </div>
                </section>
            </article>
            <section class="related_wrap related_wrap_empty"></section>
        </div>
    </div>
</div>
<div class="page_content_wrap page_paddings_yes @mobile page_content_wrap1_m @endmobile">
    <div class="content_wrap">
        <div class="content">
            <article
                class="post_item post_item_single post_featured_default post_format_standard page type-page hentry">
                <section class="post_content">
                    <div class="vc_row wpb_row vc_row-fluid">
                        <div class="wpb_column vc_column_container vc_col-sm-12">
                            <div class="vc_column-inner">
                                <div class="content_wrap">
                                    <div class="sc_section_inner">
                                        <h2 class="sc_section_title sc_item_title sc_item_title_without_descr" style="margin-top: 0px">
                                            Galeria del Rancho<span></span></h2>
                                    </div>
                                </div>
                                <div class="wpb_wrapper">
                                    <div class="owl-carousel owl-theme owl-loaded">
                                        <div class=" owl-stage-outer">
                                            <div class="owl-stage">
                                                @foreach ($galeria4s as $galeria4)
                                                @foreach ($categorias as $categoria)
                                                @if ($galeria4->categoria == $categoria->id && $categoria->id == '4')
                                                <div class="owl-item">
                                                    <div class="post_featured img ">
                                                        <a data-fancybox="rancho" href="{{ asset('storage').'/'.$galeria4->imagen }}">
                                                            <img alt="{{ $galeria4->nombre }}" width="100%" src="{{ asset('storage').'/'.$galeria4->imagen }}">
                                                        </a> 
                                                    </div>
                                                </div>     
                                                @endif
                                                @endforeach
                
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>     

                                
                            </div>
                        </div>
                    </div>
                </section>
            </article>
            <section class="related_wrap related_wrap_empty"></section>
        </div>
    </div>
</div>
@endsection