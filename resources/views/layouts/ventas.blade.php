<!DOCTYPE html>
<html lang="es" class="scheme_original">

<head>
    <title>Ventas | Rancho La Sombrilla</title>
    <meta charset="UTF-8" />
    @yield('metas')
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=2">
    <meta name="format-detection" content="telephone=yes">
    <link href="images/favicon.ico" rel="icon" type="image/x-icon">
    <link rel='stylesheet'
        href='https://fonts.googleapis.com/css?family=Average|Droid+Serif:400,700|Libre+Baskerville:400,400i,700|Open+Sans:300,400,600,700,800|Oswald:300,400,700|Raleway:100,200,300,400,500,600,700,800,900&amp;subset=latin-ext'
        type='text/css' media='all' />
    <link rel='stylesheet' href='{{ asset('css/layout.css?v='.uniqid()) }}' type='text/css' media='all' />
    <link rel='stylesheet' href='{{ asset('css/fontello/css/fontello.css?v='.uniqid()) }}' type='text/css' media='all' />
    <link rel='stylesheet' href='{{ asset('css/style.css?v='.uniqid()) }}' type='text/css' media='all' />
    <link rel='stylesheet' href='{{ asset('css/core.animation.css?v='.uniqid()) }}' type='text/css' media='all' />
    <link rel='stylesheet' href='{{ asset('css/shortcodes.css?v='.uniqid()) }}' type='text/css' media='all' />
    <link rel='stylesheet' href='{{ asset('css/theme.css?v='.uniqid()) }}' type='text/css' media='all' />
    <link rel='stylesheet' href='{{ asset('js/vendor/woo/plugin.woocommerce.css?v='.uniqid()) }}' type='text/css' media='all' />
    <link rel='stylesheet' href='{{ asset('css/custom.css?v='.uniqid()) }}' type='text/css' media='all' />
    <link rel='stylesheet' href='{{ asset('css/responsive.css?v='.uniqid()) }}' type='text/css' media='all' />
    <link rel='stylesheet' href='{{ asset('js/vendor/comp/comp.min.css?v='.uniqid()) }}' type='text/css' media='all' />
    <link rel='stylesheet' href='{{ asset('css/core.portfolio.css?v='.uniqid()) }}' type='text/css' media='all' />
    <script src="https://kit.fontawesome.com/a98a892b4b.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
        <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-EKRHEN9EV0"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-EKRHEN9EV0');
    </script>

    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-NWFPG9T');</script>
        <!-- End Google Tag Manager -->
</head>

<body
    class="page gridpg body_style_wide body_filled article_style_stretch layout_single-standard template_single-standard scheme_original top_panel_show top_panel_above sidebar_hide sidebar_outer_hide vc_responsive" style="width: 100% !important">
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NWFPG9T"
        height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <!-- End Google Tag Manager (noscript) -->
    <a id="toc_home" class="sc_anchor" title="Home"
        data-description="&lt;i&gt;Return to Home&lt;/i&gt; - &lt;br&gt;navigate to home page of the site"
        data-icon="icon-home" data-url="{{route('index')}}" data-separator="yes"></a>
    <a id="toc_top" class="sc_anchor" title="To Top"
        data-description="&lt;i&gt;Back to top&lt;/i&gt; - &lt;br&gt;scroll to top of the page"
        data-icon="icon-double-up" data-url="" data-separator="yes"></a>

    <div class="body_wrap">
        <div class="page_wrap">
            <div class="top_panel_fixed_wrap"></div>
            <header class="top_panel_wrap top_panel_style_1 scheme_original">
                <div class="top_panel_wrap_inner top_panel_inner_style_1 top_panel_position_above">
                    <div class="top_panel_top">
                        <div class="content_wrap clearfix">
                            
                            <div class="top_panel_top_contact_area" style="margin-right: 10px">
                                <img src="{{ asset('images/mex.png') }}" alt="México 1" style="margin-right: 5px">{{ $contacto->telefono }}
                            </div>
                            <div class="top_panel_top_contact_area" style="margin-right: 10px">
                                <img src="{{ asset('images/mex.png') }}" alt="México 2" style="margin-right: 5px">{{ $contacto->telefono2 }}
                            </div>
                            <div class="top_panel_top_contact_area" style="margin-right: 10px">
                                <img src="{{ asset('images/usa.png') }}" alt="Estados Unidos" style="margin-right: 5px">{{ $contacto->telefono3 }}
                            </div>
                            <div class="top_panel_top_open_hours" style="text-transform: none !important;"><i class="fas fa-envelope iconheader"></i>{{ $contacto->correo }}</div>
                            <div class="top_panel_top_user_area">
                                <div class="top_panel_top_socials">
                                    <div
                                        class="sc_socials sc_socials_type_icons sc_socials_shape_square sc_socials_size_tiny">
                                        <div class="sc_socials_item">
                                            <a href="{{ $contacto->youtube }}" target="_blank" class="social_icons social_twitter">
                                                <i class="fab fa-youtube" alt="Youtube"></i>
                                            </a>
                                        </div>
                                        <div class="sc_socials_item">
                                            <a href="{{ $contacto->facebook }}" target="_blank" class="social_icons social_facebook">
                                                <span class="icon-facebook" alt="Facebook"></span>
                                            </a>
                                        </div>
                                        <div class="sc_socials_item">
                                            <a href="#{{ $contacto->instagram }}" target="_blank" class="social_icons social_gplus-1">
                                                <i class="fab fa-instagram" alt="Instagram"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <ul id="menu_user" class="menu_user_nav"></ul>
                            </div>
                        </div>
                    </div>
                    <div class="top_panel_middle">
                        <div class="content_wrap">
                            <div class="columns_wrap columns_fluid">
                                <div class="column-5_5 contact_logo">
                                    <div class="logo">
                                        <a href="{{route('index')}}">
                                            <img src="{{ asset('images/logo.png') }}" class="logo_main" alt="Logo Rancho La Sombrilla Header" width="239" height="59">
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="top_panel_bottom">
                        <div class="content_wrap clearfix">
                            <nav class="menu_main_nav_area menu_hover_fade">
                                <ul id="menu_main" class="menu_main_nav">
                                    <li
                                        class="menu-item current-menu-ancestor current-menu-parent menu-item-has-children">
                                        <a href="{{route('index')}}"><span>Inicio</span></a>
                                    </li>
                                    <li class="menu-item menu-item-has-children"><a href="#"><span>Nuestra
                                                Genética</span></a>
                                        <ul class="sub-menu">
                                            <li class="menu-item"><a
                                                    href="{{ route('geneticabh') }}"><span>Brahman</span></a></li>
                                            <li class="menu-item"><a
                                                    href="{{ route('geneticasz') }}"><span>Suizbu</span></a></li>
                                        </ul>
                                    </li>
                                    @include('includes.web_menu')
                                    <li class="menu-item"><a href="{{ route('cuartodemilla')}}"><span>Caballos cuartos de milla</span></a></li>
                                    <li class="menu-item"><a href="{{ route('campeonatos') }}"><span>Campeonatos</span></a></li>
                                    <li class="menu-item"><a href="{{ route('galeria')}}"><span>Galería</span></a></li>
                                    <li class="menu-item"><a href="{{ route('preguntas')}}"><span>Preguntas frecuentes</span></a>
                                    </li>
                                    {{-- <li class="menu-item"><a href="{{ route('blog')}}"><span>Blog</span></a></li> --}}
                                    <li class="menu-item"><a href="{{ route('contactame')}}"><span>Contacto</span></a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </header>
            <div class="header_mobile scroll_fixed sombra">
                <div class="content_wrap">
                    <div class="menu_button icon-menu"></div>
                    <div class="logo">
                        <a href="{{route('index')}}">
                            <img src="{{ asset('images/logo.png') }}" class="logo_main" alt="Logo Rancho La Sombrilla Header Movil" width="239" height="59">
                        </a>
                    </div>
                </div>
                <div class="side_wrap">
                    <div class="close">Cerrar</div>
                    <div class="panel_top">
                        <nav class="menu_main_nav_area menu_hover_fade">
                            <ul id="menu_main" class="menu_main_nav">
                                <li
                                    class="menu-item current-menu-ancestor current-menu-parent">
                                    <a href="{{route('index')}}"><span>Inicio</span></a>
                                </li>
                                <li class="menu-item menu-item-has-children"><a href="#"><span>Nuestra
                                            Genética</span></a>
                                    <ul class="sub-menu">
                                        <li class="menu-item"><a
                                                href="{{ route('geneticabh') }}"><span>Brahman</span></a></li>
                                        <li class="menu-item"><a
                                                href="{{ route('geneticasz') }}"><span>Suizbu</span></a></li>
                                    </ul>
                                </li>
                                @include('includes.web_menu')
                                <li class="menu-item"><a href="{{ route('cuartodemilla')}}"><span>Caballos cuartos de milla</span></a></li>
                                <li class="menu-item"><a href="{{ route('campeonatos') }}"><span>Campeonatos</span></a></li>
                                <li class="menu-item"><a href="{{ route('galeria')}}"><span>Galería</span></a></li>
                                <li class="menu-item"><a href="{{ route('preguntas')}}"><span>Preguntas frecuentes</span></a>
                                </li>
                                {{-- <li class="menu-item"><a href="{{ route('blog')}}"><span>Blog</span></a></li> --}}
                                <li class="menu-item"><a href="{{ route('contactame')}}"><span>Contacto</span></a></li>
                            </ul>
                        </nav>
                    </div>
                    <div class="panel_bottom">
                    </div>
                </div>
                <div class="mask"></div>
            </div>
            <div class="top_panel_title top_panel_style_1 title_present breadcrumbs_present scheme_original">
                <div  class="bg_cust_11 top_panel_title_inner top_panel_inner_style_1  title_present_inner breadcrumbs_present_inner"  style="background-color: #ffffff !important; @mobile margin-top: 5rem !important; @endmobile">
                    <div class="content_wrap">
                        <h1 class="page_title" style="color: #2b4276;">{{ $categoria->nombre }}</h1>
                        <div class="breadcrumbs">
                            <a class="breadcrumbs_item home" href="{{route('index')}}" style="color: #2b4276;">Inicio</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="page_content_wrap page_paddings_yes fondo">
                <div class="content_wrap">
                    <div class="content">
                        <article class="post_item post_item_single post_featured_default post_format_standard page type-page hentry">
                            <section class="post_content">
                                <div class="vc_row wpb_row vc_row-fluid">
                                    <div class="wpb_column vc_column_container vc_col-sm-12">
                                        <div class="vc_column-inner ">
                                            <div class="wpb_wrapper">
                                                @if (count($animales) > 0)
                                                <div id="sc_blogger_822" class="sc_blogger layout_portfolio_3 template_portfolio sc_blogger_horizontal no_description" style="display: flex; flex-wrap: wrap;"> 
                                                        @foreach ($animales as $animal) 
                                                            <div class="vc_col-md-4">
                                                                <div class="post_content isotope_item_content ih-item colored square effect_shift left_to_right">
                                                                    <div class="post_featured img ">
                                                                        <a href="#">
                                                                            <img alt="{{ $animal->nombre }}" src="{{ asset('storage/animales/'.$animal->imagen) }}" class='img-responsive'>
                                                                        </a>
                                                                    </div>
                                                                    <div class="post_info_wrap info @mobile info2_m @endmobile">
                                                                        <div class="info-back">
                                                                            <h4 class="post_title">
                                                                                {{ $animal->nombre }}
                                                                            </h4>
                                                                            <div class="post_descr">
                                                                                <p class="post_info">
                                                                                    <span class="post_info_item post_info_posted">
                                                                                        <a data-fancybox="animal-{{ $animal->id }}" href="{{ asset('storage/animales/'.$animal->imagen)}}" class="post_info_date">Ver Fotos</a> 
                                                                                        @php
                                                                                            $galerias = \App\Models\Galeria::where([
                                                                                                ['animal_id',$animal->id],
                                                                                                ['venta',1]
                                                                                            ])->get(); 
                                                                                        @endphp
                                                                                        @foreach ($galerias as $galeria)
                                                                                        <a data-fancybox="animal-{{ $animal->id }}" href="{{ asset('storage/animales/galeria/'.$galeria->imagen)}}" class="post_info_date hidden"></a>  
                                                                                        @endforeach
                                                                                    </span>
                                                                                </p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <h4 class="sc_title sc_title_regular cmrg_2" style="text-align: center;">
                                                                        <a href="#">{{ $animal->nombre }}</a>
                                                                    </h4>
                                                                </div>
                                                                 
                                                            </div>
                                                            
                                                        @endforeach  
                                                </div>
                                                @else
                                                    <h4 class="sc_section_title sc_item_title_without_descr" style="text-align: center; margin-bottom: 200px;">Próximamente más contenido en venta</h4>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </article>
                        <section class="related_wrap related_wrap_empty"></section>
                    </div>
                </div>
            </div>
            @include('includes.web_footer')
        </div>
    </div>

    <a href="#" class="scroll_to_top icon-up" title="Scroll to top"></a>
    <div class="custom_html_section"></div>
    <script type='text/javascript' src='{{ asset('js/vendor/jquery/jquery.js?v='.uniqid()) }}'></script>
    <script type='text/javascript' src='{{ asset('js/vendor/jquery/jquery-migrate.min.js?v='.uniqid()) }}'></script>
    <script type='text/javascript' src='{{ asset('js/custom/custom.js?v='.uniqid()) }}'></script>
    <script type='text/javascript' src='{{ asset('js/vendor/modernizr.min.js?v='.uniqid()) }}'></script>
    <script type='text/javascript' src='{{ asset('js/vendor/jquery/js.cookie.min.js?v='.uniqid()) }}'></script>
    <script type='text/javascript' src='{{ asset('js/vendor/superfish.js?v='.uniqid()) }}'></script>
    <script type='text/javascript' src='{{ asset('js/custom/core.utils.js?v='.uniqid()) }}'></script>
    <script type='text/javascript' src='{{ asset('js/custom/core.init.js?v='.uniqid()) }}'></script>
    <script type='text/javascript' src='{{ asset('js/custom/init.js?v='.uniqid()) }}'></script>
    <script type='text/javascript' src='{{ asset('js/custom/core.debug.js?v='.uniqid()) }}'></script>
    <script type='text/javascript' src='{{ asset('js/custom/embed.min.js?v='.uniqid()) }}'></script>
    <script type='text/javascript' src='{{ asset('js/custom/shortcodes.js?v='.uniqid()) }}'></script>
    <script type='text/javascript' src='{{ asset('js/vendor/comp/comp_front.min.js?v='.uniqid()) }}'></script>
    <script type='text/javascript' src='{{ asset('js/vendor/isotope.pkgd.min.js?v='.uniqid()) }}'></script>
    <script type='text/javascript' src='{{ asset('js/vendor/jquery/jquery.hoverdir.js?v='.uniqid()) }}'></script>
    <script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script></body>

</html>
