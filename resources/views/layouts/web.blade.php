<!DOCTYPE html>
<html lang="es" class="scheme_original">

<head>
    <title>@yield('title')</title>
    <meta charset="UTF-8" />
    @yield('metas')
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=5">
    <meta name="format-detection" content="telephone=yes">
    <link href="images/favicon.ico" rel="icon" type="image/x-icon">
    <link rel='stylesheet'
        href='https://fonts.googleapis.com/css?family=Average|Droid+Serif:400,700|Libre+Baskerville:400,400i,700|Open+Sans:300,400,600,700,800|Oswald:300,400,700|Raleway:100,200,300,400,500,600,700,800,900&amp;subset=latin-ext'
        type='text/css' media='all' />
    <link rel='stylesheet' href="{{ asset('css/layout.css?v='.uniqid())}}" type='text/css' media='all' />
    <link rel='stylesheet' href="{{ asset('js/vendor/revslider/settings.css?v='.uniqid())}}" type='text/css' media='all' />
    <link rel='stylesheet' href="{{ asset('js/vendor/woo/woocommerce-layout.css?v='.uniqid())}}" type='text/css' media='all' />
    <link rel='stylesheet' href="{{ asset('js/vendor/woo/woocommerce-smallscreen.css?v='.uniqid())}}" type='text/css'
        media='only screen and (max-width: 768px)' />
    <link rel='stylesheet' href="{{ asset('js/vendor/woo/woocommerce.css?v='.uniqid())}}" type='text/css' media='all' />
    <link rel='stylesheet' href="{{ asset('css/fontello/css/fontello.css?v='.uniqid())}}" type='text/css' media='all' />
    <link rel='stylesheet' href="{{ asset('css/style.css?v='.uniqid())}}" type='text/css' media='all' />
    <link rel='stylesheet' href="{{ asset('css/core.animation.css?v='.uniqid())}}" type='text/css' media='all' />
    <link rel='stylesheet' href="{{ asset('css/shortcodes.css?v='.uniqid())}}" type='text/css' media='all' />
    <link rel='stylesheet' href="{{ asset('css/theme.css?v='.uniqid())}}" type='text/css' media='all' />
    <link rel='stylesheet' href="{{ asset('js/vendor/woo/plugin.woocommerce.css?v='.uniqid())}}" type='text/css' media='all' />
    <link rel='stylesheet' href="{{ asset('css/custom.css?v='.uniqid())}}" type='text/css' media='all' />
    <link rel='stylesheet' href="{{ asset('css/responsive.css?v='.uniqid())}}" type='text/css' media='all' />
    <link rel='stylesheet' href="{{ asset('js/vendor/comp/comp.min.css?v='.uniqid())}}" type='text/css' media='all' />
    <link rel='stylesheet' href="{{ asset('js/vendor/swiper/swiper.css?v='.uniqid())}}" type='text/css' media='all' />
    <link rel="stylesheet" href="{{ asset('owlcarousel/dist/assets/owl.carousel.min.css?v='.uniqid())}}">
    <link rel="stylesheet" href="{{ asset('owlcarousel/dist/assets/owl.theme.default.min.css?v='.uniqid())}}">
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-EKRHEN9EV0"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-EKRHEN9EV0');
    </script>
    
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-NWFPG9T');</script>
        <!-- End Google Tag Manager -->
</head>

<body
    class="page home3 body_style_wide body_filled article_style_stretch layout_single-standard template_single-standard scheme_original top_panel_show top_panel_above sidebar_hide sidebar_outer_hide vc_responsive" style="width: 100% !important">
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NWFPG9T"
        height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <!-- End Google Tag Manager (noscript) -->
    <a id="toc_home" class="sc_anchor" title="Home"
        data-description="&lt;i&gt;Return to Home&lt;/i&gt; - &lt;br&gt;navigate to home page of the site"
        data-icon="icon-home" data-url="{{route('index')}}" data-separator="yes"></a>
    <a id="toc_top" class="sc_anchor" title="To Top"
        data-description="&lt;i&gt;Back to top&lt;/i&gt; - &lt;br&gt;scroll to top of the page"
        data-icon="icon-double-up" data-url="" data-separator="yes"></a>

    <div class="body_wrap">
        <div class="page_wrap">
            <div class="top_panel_fixed_wrap"></div>
            <header class="top_panel_wrap top_panel_style_1 scheme_original">
                <div class="top_panel_wrap_inner top_panel_inner_style_1 top_panel_position_above">
                    <div class="top_panel_top">
                        <div class="content_wrap clearfix">
                            <div class="top_panel_top_contact_area" style="margin-right: 10px">
                                <img src="{{ asset('images/mex.png') }}" alt="México 1" style="margin-right: 5px">{{ $contacto->telefono }}
                            </div>
                            <div class="top_panel_top_contact_area" style="margin-right: 10px">
                                <img src="{{ asset('images/mex.png') }}" alt="México 2" style="margin-right: 5px">{{ $contacto->telefono2 }}
                            </div>
                            <div class="top_panel_top_contact_area" style="margin-right: 10px">
                                <img src="{{ asset('images/usa.png') }}" alt="Estados Unidos" style="margin-right: 5px">{{ $contacto->telefono3 }}
                            </div>
                            <div class="top_panel_top_open_hours" style="text-transform: none !important;"><i class="fas fa-envelope iconheader" alt="mail"></i>{{ $contacto->correo }}</div>
                            <div class="top_panel_top_user_area">
                                <div class="top_panel_top_socials">
                                    <div
                                        class="sc_socials sc_socials_type_icons sc_socials_shape_square sc_socials_size_tiny">
                                        <div class="sc_socials_item">
                                            <a href="{{ $contacto->youtube }}" target="_blank" class="social_icons social_twitter">
                                                <i class="fab fa-youtube" alt="Youtube"></i>
                                            </a>
                                        </div>
                                        <div class="sc_socials_item">
                                            <a href="{{ $contacto->facebook }}" target="_blank" class="social_icons social_facebook">
                                                <span class="icon-facebook" alt="Facebook"></span>
                                            </a>
                                        </div>
                                        <div class="sc_socials_item">
                                            <a href="{{ $contacto->instagram }}" target="_blank" class="social_icons social_gplus-1">
                                                <i class="fab fa-instagram" alt="Instagram"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <ul id="menu_user" class="menu_user_nav"></ul>
                            </div>
                        </div>
                    </div>
                    <div class="top_panel_middle">
                        <div class="content_wrap">
                            <div class="columns_wrap columns_fluid">
                                <div class="column-5_5 contact_logo">
                                    <div class="logo">
                                        <a href="{{route('index')}}">
                                            <img src="{{ asset('images/logo.png') }}" class="logo_main" alt="Logo Rancho La Sombrilla Header Movil" width="239" height="59">
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="top_panel_bottom">
                        <div class="content_wrap clearfix">
                            <nav class="menu_main_nav_area menu_hover_fade">
                                <ul id="menu_main" class="menu_main_nav">
                                    <li
                                        class="menu-item current-menu-ancestor current-menu-parent menu-item-has-children">
                                        <a href="{{route('index')}}"><span>Inicio</span></a>
                                    </li>
                                    <li class="menu-item menu-item-has-children"><a href="#"><span>Nuestra
                                                Genética</span></a>
                                        <ul class="sub-menu">
                                            <li class="menu-item"><a
                                                    href="{{ route('geneticabh') }}"><span>Brahman</span></a></li>
                                            <li class="menu-item"><a
                                                    href="{{ route('geneticasz') }}"><span>Suizbu</span></a></li>
                                        </ul>
                                    </li>
                                    @include('includes.web_menu')
                                    <li class="menu-item"><a href="{{ route('cuartodemilla')}}"><span>Caballos cuartos de milla</span></a></li>
                                    <li class="menu-item"><a href="{{ route('campeonatos') }}"><span>Campeonatos</span></a></li>
                                    <li class="menu-item"><a href="{{ route('galeria')}}"><span>Galería</span></a></li>
                                    <li class="menu-item"><a href="{{ route('preguntas')}}"><span>Preguntas frecuentes</span></a>
                                    </li>
                                    {{-- <li class="menu-item"><a href="{{ route('blog')}}"><span>Blog</span></a></li> --}}
                                    <li class="menu-item"><a href="{{ url('contactame')}}"><span>Contacto</span></a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </header>
            <div class="header_mobile scroll_fixed sombra">
                <div class="content_wrap">
                    <div class="menu_button icon-menu"></div>
                    <div class="logo">
                        <a href="{{route('index')}}">
                            <img src="{{ asset('images/logo.png') }}" class="logo_main" alt="Logo Rancho La Sombrilla Header Movil" width="239" height="59">
                        </a>
                    </div>
                </div>
                <div class="side_wrap">
                    <div class="close">Cerrar</div>
                    <div class="panel_top">
                        <nav class="menu_main_nav_area menu_hover_fade">
                            <ul id="menu_main" class="menu_main_nav">
                                <li
                                    class="menu-item current-menu-ancestor current-menu-parent">
                                    <a href="{{route('index')}}"><span>Inicio</span></a>
                                </li>
                                <li class="menu-item menu-item-has-children"><a href="#"><span>Nuestra
                                            Genética</span></a>
                                    <ul class="sub-menu">
                                        <li class="menu-item"><a
                                                href="{{ route('geneticabh') }}"><span>Brahman</span></a></li>
                                        <li class="menu-item"><a
                                                href="{{ route('geneticasz') }}"><span>Suizbu</span></a></li>
                                    </ul>
                                </li>
                                @include('includes.web_menu')
                                <li class="menu-item"><a href="{{ route('cuartodemilla')}}"><span>Caballos cuartos de milla</span></a></li>
                                <li class="menu-item"><a href="{{ route('campeonatos') }}"><span>Campeonatos</span></a></li>
                                <li class="menu-item"><a href="{{ route('galeria')}}"><span>Galería</span></a></li>
                                <li class="menu-item"><a href="{{ route('preguntas')}}"><span>Preguntas frecuentes</span></a>
                                </li>
                                {{-- <li class="menu-item"><a href="{{ route('blog')}}"><span>Blog</span></a></li> --}}
                                <li class="menu-item"><a href="{{ route('contactame')}}"><span>Contacto</span></a></li>
                            </ul>
                        </nav>
                    </div>
                    <div class="panel_bottom">
                    </div>
                </div>
                <div class="mask"></div>
            </div>
            @yield('main')
            @include('includes.web_footer')
        </div>
    </div>

    <a href="#" class="scroll_to_top icon-up" title="Scroll to top"></a>
    <div class="custom_html_section"></div>
    <script type='text/javascript' src='{{ asset('js/vendor/jquery/jquery.js?v='.uniqid()) }}'></script>
    <script type='text/javascript' src='{{ asset('js/vendor/jquery/jquery-migrate.min.js?v='.uniqid()) }}'></script>
    <script type='text/javascript' src='{{ asset('js/custom/custom.js?v='.uniqid()) }}'></script>
    <script type='text/javascript' src='{{ asset('js/vendor/esg/jquery.themepunch.tools.min.js?v='.uniqid()) }}'></script>
    <script type='text/javascript' src='{{ asset('js/vendor/revslider/jquery.themepunch.revolution.min.js?v='.uniqid()) }}'></script>
    <script type='text/javascript' src='{{ asset('js/vendor/revslider/extensions/revolution.extension.actions.min.js?v='.uniqid()) }}'></script>
    <script type='text/javascript' src='{{ asset('js/vendor/revslider/extensions/revolution.extension.layeranimation.min.js?v='.uniqid()) }}'></script>
    <script type='text/javascript' src='{{ asset('js/vendor/revslider/extensions/revolution.extension.navigation.min.js?v='.uniqid()) }}'></script>
    <script type='text/javascript' src='{{ asset('js/vendor/revslider/extensions/revolution.extension.slideanims.min.js?v='.uniqid()) }}'></script>
    <script type='text/javascript' src='{{ asset('js/vendor/modernizr.min.js?v='.uniqid()) }}'></script>
    <script type='text/javascript' src='{{ asset('js/vendor/jquery/js.cookie.min.js?v='.uniqid()) }}'></script>
    <script type='text/javascript' src='{{ asset('js/vendor/superfish.js?v='.uniqid()) }}'></script>
    <script type='text/javascript' src='{{ asset('js/custom/core.utils.js?v='.uniqid()) }}'></script>
    <script type='text/javascript' src='{{ asset('js/custom/core.init.js?v='.uniqid()) }}'></script>
    <script type='text/javascript' src='{{ asset('js/custom/init.ja?v='.uniqid()) }}'></script>
    <script type='text/javascript' src='{{ asset('js/custom/core.debug.js?v='.uniqid()) }}'></script>
    <script type='text/javascript' src='{{ asset('js/custom/embed.min.js?v='.uniqid()) }}'></script>
    <script type='text/javascript' src='{{ asset('js/custom/shortcodes.js?v='.uniqid()) }}'></script>
    <script type='text/javascript' src='{{ asset('js/vendor/comp/comp_front.min.js?v='.uniqid()) }}'></script>
    <script type='text/javascript' src='{{ asset('js/vendor/ui/core.min.js?v='.uniqid()) }}'></script>
    <script type='text/javascript' src='{{ asset('js/vendor/ui/widget.min.js?v='.uniqid()) }}'></script>
    <script type='text/javascript' src='{{ asset('js/vendor/ui/tabs.min.js?v='.uniqid()) }}'></script>
    <script type='text/javascript' src='{{ asset('js/vendor/isotope.pkgd.min.js?v='.uniqid()) }}'></script>
    <script type='text/javascript' src='{{ asset('js/vendor/swiper/swiper.js?v='.uniqid()) }}'></script>
    <script src="https://kit.fontawesome.com/a98a892b4b.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
    <script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
    <script src="{{ asset('owlcarousel/dist/owl.carousel.min.js')}}"></script>

    <script>
        jQuery(document).ready(function () {
            jQuery(".owl-carousel").owlCarousel({
                loop: true,
                margin: 10,
                autoplay: true,
                autoplayTimeout: 2000,
                autoplayHoverPause: true,
                responsiveClass:true,
                dots:false,
                nav:true,
                responsive:{
                    0:{
                        items:1,
                        
                    },
                    600:{
                        items:2,
                        
                    },
                    1000:{
                        items:3,
                    }
                }
            });
        });
    </script>
    @yield('scripts')
</body>

</html>
