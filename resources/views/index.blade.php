@extends('layouts.web')
@section('title', 'Inicio | Rancho La Sombrilla')
@section('metas')
<meta name="language" content="spanish">
<meta name="copyright" content="Rancho La Sombrilla">
<meta name="author" content="CYBAC">
<meta name="audience" content="all">
<meta name="description" content="En Rancho La Sombrilla trabajamos siempre para que el ganado que es herrado con nuestros fierros se caracterice por contar con la mejor genética y solución.">
<meta name="keywords" content="rancho, sombrilla, brahman, brahman gris, suizbu, cuarto de milla, quarter horse, vacas, carne, caballos, receptoras F1">
<meta name="robots" content="index, all, follow">
<meta name="category" content="ranch">
@show
@section('main')
<section class="slider_wrap slider_fullwide slider_engine_revo slider_alias_slider-1">
    <div id="rev_slider_1_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" style="@mobile margin-top: 5rem !important; @endmobile">
        <div id="rev_slider_1_1" class="rev_slider fullwidthabanner" data-version="5.2.6">
            <ul>
                @foreach ($sliders as $slider)
                <li data-index="rs-{{ $loop->iteration }}" data-transition="cube" data-slotamount="default" data-hideafterloop="0"
                    data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="default"
                    data-thumb="images/home-1-slide-1-100x50.jpg" data-rotate="0" data-saveperformance="off"
                    data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5=""
                    data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                    <img src="{{ asset('storage').'/'.$slider->imagen }}" width="100%" alt="slider-{{ $loop->iteration }}" title="home-1-slide-1" width="1903" height="873"
                        data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg"
                        data-no-retina>
                        <div class="tp-caption BigWhiteText tp-resizeme" id="slide-1-layer-1" data-x="center"
                        data-hoffset="" data-y="center" data-voffset="10" data-width="['auto']" data-height="['auto']"
                        data-transform_idle="o:1;" data-transform_in="y:50px;opacity:0;s:800;e:Power2.easeInOut;"
                        data-transform_out="opacity:0;s:300;" data-start="1500" data-splitin="none" data-splitout="none"
                        data-responsive_offset="on">{!! $slider->titulo!!}</div>
                </li>
                @endforeach
            </ul>
            <div class="tp-bannertimer tp-bottom"></div>
        </div>
    </div>
</section>
<div class="page_content_wrap page_paddings_no">
    <div class="content_wrap">
        <div class="content">
            <article
                class="post_item post_item_single post_featured_default post_format_standard page type-page hentry">
                <section class="post_content">
                    <div data-vc-full-width="true" data-vc-full-width-init="false"
                        class="vc_row wpb_row vc_row-fluid vc_custom_1475063547001">
                        <div class="wpb_column vc_column_container vc_col-sm-12">
                            <div class="vc_column-inner ">
                                <div class="wpb_wrapper">
                                    <div class="sc_section margin_bottom_small section_style_dark_text"
                                        data-animation="animated fadeInUp normal">
                                        <div class="sc_section_inner">
                                            <h2 class="sc_section_title sc_item_title sc_item_title0 sc_item_title_without_descr">{!! $titulo->titulo !!}<span></span></h2>
                                            <div class="sc_section_content_wrap">
                                                <div
                                                    class="sc_section sc_section_block margin_bottom_large aligncenter mw800">
                                                    <div class="sc_section_inner">
                                                        <div class="sc_section_content_wrap">
                                                            <h3 class="sc_highlight fst_1">{{ $titulo->subtitulo }}</h3>
                                                        </div>
                                                        <div class="sc_section_content_wrap">
                                                            <p class="sc_highlight fst_2">{{ $titulo->parrafo}}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="vc_empty_space h_6r">
                                                    <span class="vc_empty_space_inner"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="vc_row-full-width"></div>
                    <div class="vc_row wpb_row vc_row-fluid" id="aqui">
                        <div class="wpb_column vc_column_container vc_col-sm-12">
                            <div class="vc_column-inner " style="margin-bottom: 50px !important;">
                                <div class="wpb_wrapper">
                                    <div class="sc_section aligncenter" data-animation="animated fadeInUp normal">
                                        <div class="sc_section_inner">
                                            <div class="sc_section_content_wrap">
                                                <figure class="sc_image sc_image_shape_square margin_top_huge-">
                                                    <img src="{{ asset("storage/fierros/$logoprincipal->imagen") }}" width="100%" alt="Rancho la Sombrilla" style="width: 60%; @mobile width: 100% !important; @endmobile"/>
                                                </figure>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @foreach ($fierros as $fierro)
                        @if ($fierro->id > 1)
                        <div class="wpb_column vc_column_container {{ count($fierros) > 1 ? "vc_col-xs-4" : "vc_col-xs-6" }}">
                            <div class="vc_column-inner " style="margin-bottom: 50px !important; margin-top: 50px !important;">
                                <div class="wpb_wrapper">
                                    <div class="sc_section aligncenter" data-animation="animated fadeInUp normal">
                                        <div class="sc_section_inner">
                                            <div class="sc_section_content_wrap">
                                                <figure class="sc_image sc_image_shape_square margin_top_huge-">
                                                    <img src="{{ asset("storage/fierros/$fierro->imagen") }}" alt="fierro-{{ $loop->iteration }}" @desktop style="width: 50%;" @enddesktop @mobile style="width: 100%;" @endmobile/>
                                                </figure>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif
                        @endforeach
                    </div>
                    @desktop
                    <div data-vc-full-width="true" data-vc-full-width-init="false"
                        class="vc_row wpb_row vc_row-fluid vc_custom_1475148381788 inverse_colors" style="background-image:url({{ asset('storage').'/'.$geneticas->slider }}) !important; background-attachment: fixed !important;">
                        <div class="wpb_column vc_column_container vc_col-sm-6">
                            <div class="vc_column-inner vc_custom_1475235191339">
                                <div class="wpb_wrapper">
                                    <div class="sc_section sc_section_block "
                                        data-animation="animated fadeInLeft normal">
                                        <div class="sc_section_inner">
                                            <div class="sc_section_content_wrap">
                                                <div class="sc_section margin_bottom_tiny- alignleft fwidth">
                                                    <div class="sc_section_inner">
                                                        <h2
                                                            class="sc_section_title sc_item_title sc_item_title00 sc_item_title_without_descr">
                                                            Nuestra Genética<span></span></h2>
                                                        <div class="sc_section_content_wrap"></div>
                                                    </div>
                                                </div>
                                                <div class="sc_section margin_bottom_huge section_style_call_section">
                                                    <div class="sc_section_inner">
                                                        <h2
                                                            class="sc_section_title sc_item_title sc_item_title_without_descr">{{ $geneticas->titulo}}<span></span>
                                                        </h2>
                                                        <div class="sc_section_descr sc_item_descr" style="text-align: justify;">{{ $geneticas->parrafo}}</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="wpb_column vc_column_container vc_col-sm-5">
                            <div class="vc_column-inner vc_custom_1475235352066 modificacion1">
                                <div class="wpb_wrapper">
                                    <figure class="sc_image sc_image_shape_square margin_top_huge"
                                        data-animation="animated fadeInRight normal">
                                        <img src="{{ asset('storage').'/'.$geneticas->imagen }}" alt="" />
                                    </figure>
                                </div>
                            </div>
                        </div>
                        <div class="wpb_column vc_column_container vc_col-sm-1">
                            <div class="vc_column-inner ">
                                <div class="wpb_wrapper"></div>
                            </div>
                        </div>
                    </div>
                    @enddesktop
                    @mobile
                    <div data-vc-full-width="true" data-vc-full-width-init="false"
                        class="vc_row wpb_row vc_row-fluid vc_custom_1475148381788 inverse_colors" style="background-image:url({{ asset('storage').'/'.$geneticas->slider }}) !important;background-attachment: fixed !important;">
                        <div class="wpb_column vc_column_container vc_col-sm-6">
                            <div class="vc_column-inner vc_custom_1475235191339_m">
                                <div class="wpb_wrapper">
                                    <div class="sc_section sc_section_block "
                                        data-animation="animated fadeInLeft normal">
                                        <div class="sc_section_inner">
                                            <div class="sc_section_content_wrap">
                                                <div class="sc_section margin_bottom_tiny- alignleft fwidth">
                                                    <div class="sc_section_inner">
                                                        <h2
                                                            class="sc_section_title sc_item_title sc_item_title00 sc_item_title_without_descr sc_item_title_m">
                                                            Nuestra Genética<span></span></h2>
                                                        <div class="sc_section_content_wrap"></div>
                                                    </div>
                                                </div>
                                                <div class="sc_section margin_bottom_huge section_style_call_section">
                                                    <div class="sc_section_inner">
                                                        <h2
                                                            class="sc_section_title sc_item_title sc_item_title_without_descr">{{ $geneticas->titulo}}<span></span>
                                                        </h2>
                                                        <div class="sc_section_descr sc_item_descr" style="text-align: justify;">{{ $geneticas->parrafo}}</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="wpb_column vc_column_container vc_col-sm-5 ocultar_movil">
                            <div class="vc_column-inner vc_custom_1475235352066 modificacion1">
                                <div class="wpb_wrapper">
                                    <figure class="sc_image sc_image_shape_square margin_top_huge"
                                        data-animation="animated fadeInRight normal">
                                        <img src="{{ asset('storage').'/'.$geneticas->imagen }}" alt="" />
                                    </figure>
                                </div>
                            </div>
                        </div>
                        <div class="wpb_column vc_column_container vc_col-sm-1 ocultar_movil">
                            <div class="vc_column-inner ">
                                <div class="wpb_wrapper"></div>
                            </div>
                        </div>
                    </div>
                    @endmobile
                    <div class="vc_row-full-width"></div>
                    <div class="vc_row wpb_row vc_row-fluid">
                        <div class="wpb_column vc_column_container vc_col-sm-12">
                            <div class="vc_column-inner ">
                                <div class="wpb_wrapper">
                                    <div class="sc_section sc_section_block margin_top_huge margin_bottom_huge @mobile margin_top_huge_m @endmobile aligncenter"
                                        data-animation="animated fadeInUp normal" style="margin-top: 2em !important">
                                        <div class="sc_section_inner">
                                            <h2 class="sc_section_title sc_item_title sc_item_title0 sc_item_title_without_descr">
                                                Ventas<span></span></h2>
                                            <div class="sc_section_content_wrap">
                                                <div class="columns_wrap sc_columns columns_nofluid sc_columns_count_4">
                                                    <div class="column-1_4 sc_column_item sc_column_item_1 odd first">
                                                        @php 
                                                            $animal = \App\Models\Animales::where('categoria',1)->first(); 
                                                        @endphp
                                                        @foreach ($titulosventas as $titulosventa)
                                                            @if ($titulosventa->id == '1')
                                                            <figure class="sc_image sc_image_shape_round">
                                                                <a href="{{ route('ventas',['categoria_id'=>$animal->categoria, 'subcategoria_id'=>$animal->subcategoria]) }}">
                                                                    <img src="{{ asset('storage').'/'.$titulosventa->portada }}" alt="{{ $titulosventa->titulo}}" />
                                                                </a>
                                                            </figure>
                                                            <h4 class="sc_title sc_title_regular cmrg_2">
                                                                <a href="{{ route('ventas',['categoria_id'=>$animal->categoria, 'subcategoria_id'=>$animal->subcategoria]) }}">{{ $titulosventa->titulo}}</a>
                                                            </h4>
                                                            @endif
                                                        @endforeach
                                                    </div>
                                                    <div class="column-1_4 sc_column_item sc_column_item_2 even">
                                                        @php 
                                                            $animal = \App\Models\Animales::where('categoria',2)->first(); 
                                                        @endphp
                                                        @foreach ($titulosventas as $titulosventa)
                                                        @if ($titulosventa->id == '2')
                                                        <figure class="sc_image sc_image_shape_round">
                                                            <a href="{{ route('ventas',['categoria_id'=>$animal->categoria, 'subcategoria_id'=>$animal->subcategoria]) }}">
                                                                <img src="{{ asset('storage').'/'.$titulosventa->portada }}" alt="{{ $titulosventa->titulo}}" />
                                                            </a>
                                                        </figure>
                                                        <h4 class="sc_title sc_title_regular cmrg_2">
                                                            <a href="{{ route('ventas',['categoria_id'=>$animal->categoria, 'subcategoria_id'=>$animal->subcategoria]) }}">{{ $titulosventa->titulo}}</a>
                                                        </h4>
                                                        @endif
                                                        @endforeach
                                                    </div>
                                                    <div class="column-1_4 sc_column_item sc_column_item_3 odd">
                                                        @foreach ($titulosventas as $titulosventa)
                                                        @if ($titulosventa->id == '3')
                                                        <figure class="sc_image sc_image_shape_round">
                                                            <a href="{{ route('ventasrf1')}}">
                                                                <img src="{{ asset('storage').'/'.$titulosventa->portada }}" alt="{{ $titulosventa->titulo}}" />
                                                            </a>
                                                        </figure>
                                                        <h4 class="sc_title sc_title_regular cmrg_2">
                                                            <a href="{{ route('ventasrf1')}}">{{ $titulosventa->titulo}}</a>
                                                        </h4>
                                                        @endif
                                                        @endforeach
                                                    </div>
                                                    <div class="column-1_4 sc_column_item sc_column_item_4 even">
                                                        @php 
                                                            $animal = \App\Models\Animales::where('categoria',3)->first(); 
                                                        @endphp
                                                        @foreach ($titulosventas as $titulosventa)
                                                        @if ($titulosventa->id == '4')
                                                        <figure class="sc_image sc_image_shape_round">
                                                            <a href="{{ route('ventas',['categoria_id'=>$animal->categoria, 'subcategoria_id'=>$animal->subcategoria]) }}">
                                                                <img src="{{ asset('storage').'/'.$titulosventa->portada }}" alt="{{ $titulosventa->titulo}}" />
                                                            </a>
                                                        </figure>
                                                        <h4 class="sc_title sc_title_regular cmrg_2">
                                                            <a href="{{ route('ventas',['categoria_id'=>$animal->categoria, 'subcategoria_id'=>$animal->subcategoria]) }}">{{ $titulosventa->titulo}}</a>
                                                        </h4>
                                                        @endif
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-vc-full-width="true" data-vc-full-width-init="false"
                        class="vc_row wpb_row vc_row-fluid vc_custom_1475048754773 inverse_colors" style="background-image:url({{ asset('storage').'/'.$indexcaballos->slider }}) !important;background-attachment: fixed !important;">
                        <div class="wpb_column vc_column_container vc_col-sm-1">
                            <div class="vc_column-inner ">
                                <div class="wpb_wrapper"></div>
                            </div>
                        </div>
                        <div class="wpb_column vc_column_container vc_col-sm-4 @mobile ocultar_movil @endmobile">
                            <div class="vc_column-inner vc_custom_1475049444491">
                                <div class="wpb_wrapper">
                                    <figure class="sc_image sc_image_shape_square margin_top_huge"
                                        data-animation="animated fadeInLeft normal">
                                        <img src="{{ asset('storage').'/'.$indexcaballos->imagen }}" alt="caballo" />
                                    </figure>
                                </div>
                            </div>
                        </div>
                        <div class="wpb_column vc_column_container vc_col-sm-1">
                            <div class="vc_column-inner ">
                                <div class="wpb_wrapper"></div>
                            </div>
                        </div>
                        <div class="wpb_column vc_column_container vc_col-sm-6">
                            <div class="vc_column-inner vc_custom_1475049863739">
                                <div class="wpb_wrapper">
                                    <div class="sc_section sc_section_block "
                                        data-animation="animated fadeInRight normal">
                                        <div class="sc_section_inner">
                                            <div class="sc_section_content_wrap">
                                                <div class="sc_section margin_bottom_tiny- alignleft">
                                                    <div class="sc_section_inner">
                                                        <h2
                                                            class="sc_section_title sc_item_title sc_item_title3-1 sc_item_title_without_descr @mobile sc_item_title_m @endmobile">
                                                            Caballos Cuarto de Milla<span></span></h2>
                                                        <div class="sc_section_content_wrap"></div>
                                                    </div>
                                                </div>
                                                <div class="sc_section margin_bottom_huge section_style_call_section">
                                                    <div class="sc_section_inner">
                                                        <h2
                                                            class="sc_section_title sc_item_title sc_item_title_without_descr">{{ $indexcaballos->titulo}}<span></span></h2>
                                                        <div class="sc_section_descr sc_item_descr" style="text-align: justify;">{{ $indexcaballos->parrafo}}</div>
                                                        <div class="sc_section_content_wrap">
                                                            <a href="{{ route('cuartodemilla') }}"
                                                                class="sc_button sc_button_square sc_button_style_border sc_button_size_small">Ver Más</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="vc_row-full-width"></div>
                    <div class="vc_row wpb_row vc_row-fluid">
                        <div class="wpb_column vc_column_container vc_col-sm-12">
                            <div class="vc_column-inner ">
                                <div class="wpb_wrapper">
                                    <div class="sc_section margin_top_huge @mobile margin_top_huge_m @endmobile" data-animation="animated fadeInUp normal" style="margin-top: 2em !important">
                                        <div class="sc_section_inner">
                                            <h2 class="sc_section_title sc_item_title sc_item_title0 sc_item_title_without_descr">Mas Información<span></span></h2>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="vc_row wpb_row vc_row-fluid">
                        <div class="wpb_column vc_column_container vc_col-sm-12">
                            <div class="vc_column-inner ">
                                <div class="wpb_wrapper">
                                    <div class="columns_wrap sc_columns columns_nofluid sc_columns_count_12 margin_top_small margin_bottom_huge"
                                        data-animation="animated fadeInUp normal">
                                        <div class="column-7_12 sc_column_item sc_column_item_1 odd first span_7">
                                            <a href="{{ route('preguntas')}}">
                                                @foreach ($informacions as $informacion)
                                                @if ($informacion->id == '1')
                                                <div class="sc_intro intro_1" style="background-image:url({{ asset('storage').'/'.$informacion->portada }}) !important;">
                                                    <div class="sc_intro_inner  sc_intro_style_2">
                                                        <div class="sc_intro_content" @mobile style="text-align: center;" @endmobile>
                                                            <h2 class="sc_intro_title">{{ $informacion->titulo}}</h2>
                                                        </div>
                                                    </div>
                                                </div>
                                                @endif
                                                @endforeach
                                            </a>
                                        </div>
                                        <div class="column-5_12 sc_column_item sc_column_item_8 even span_5">
                                            <a href="{{ route('galeria')}}">
                                            @foreach ($informacions as $informacion)
                                            @if ($informacion->id == '2')
                                            <div class="sc_intro intro_2" style="background-image:url({{ asset('storage').'/'.$informacion->portada }}) !important; padding-bottom: 7.35em; @mobile padding-bottom: 3em !important; @endmobile">
                                                <div class="sc_intro_inner  sc_intro_style_2">
                                                    <div class="sc_intro_content" @mobile style="text-align: center;" @endmobile>
                                                        <h2 class="sc_intro_title">{{ $informacion->titulo}}</h2>
                                                    </div>
                                                </div>
                                            </div>
                                            @endif
                                            @endforeach
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>                    
                </section>
            </article>
            <section class="related_wrap related_wrap_empty"></section>
        </div>
    </div>

    <div class="vc_row wpb_row vc_row-fluid" style="width: 100% !important; margin-left: 0px !important; margin-right: 0px !important;">
        <div class="wpb_column vc_column_container vc_col-sm-12">
            <div class="vc_column-inner" style="padding-left: 0 !important; padding-right: 0 !important;">
                <div class="wpb_wrapper">
                    <div class="sc_section sc_section_block margin_top_huge @mobile margin_top_huge_m @endmobile margin_bottom_null aligncenter"
                        data-animation="animated fadeInUp normal" style="margin-top: 2em !important;">
                        <div class="sc_section_inner">
                            <div class="sc_section_content_wrap">
                                <div class="sc_promo sc_promo_size_large">
                                    <div class="sc_promo_inner">
                                        <div class="sc_promo_image" style="background-image:url( {{ asset('storage').'/'.$portadanosotros->portada }} ) !important;"></div>
                                        <div class="sc_promo_block sc_align_left">
                                            <div class="sc_promo_block_inner">
                                                <div class="sc_promo_content">
                                                    <div id="sc_tabs_583"
                                                        class="sc_tabs sc_tabs_style_1" data-active="0">
                                                        <ul class="sc_tabs_titles">
                                                            @foreach ($nosotros as $nosotro)
                                                            <li class="sc_tabs_title first">
                                                                <a href="#sc_tab_315_1_19{{ $nosotro->id}}"
                                                                    class="theme_button"
                                                                    id="sc_tab_315_1_19{{ $nosotro->id}}_tab">{{ $nosotro->id}}</a>
                                                            </li>
                                                            @endforeach
                                                        </ul>
                                                        @foreach ($nosotros as $nosotro)
                                                        <div id="sc_tab_315_1_19{{ $nosotro->id}}"
                                                            class="sc_tabs_content odd first">
                                                            <div class="sc_section aligncenter">
                                                                <div class="sc_section_inner">
                                                                    <div
                                                                    class="sc_section_content_wrap">
                                                                        <h4
                                                                            class="sc_title sc_title_regular cmrg_1">{{ $nosotro->titulo}}</h4>
                                                                        <div
                                                                            class="wpb_text_column wpb_content_element ">
                                                                            <div class="wpb_wrapper" style="min-height: 100px !important;  height: 100px !important; overflow-y: auto !important;">
                                                                                <p style="font-size: 1.20em;">{{ $nosotro->parrafo}}</p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        @endforeach
                                                    </div>
                                                    <div class="vc_empty_space h_11r">
                                                        <span class="vc_empty_space_inner"></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content_wrap @mobile ocultar_movil @endmobile">
        <div class="content">
            <div class="vc_row wpb_row vc_row-fluid">
                <div class="wpb_column vc_column_container vc_col-sm-12">
                    <div class="vc_column-inner ">
                        <div class="wpb_wrapper">
                            <div class="sc_section margin_top_huge margin_bottom_huge"
                                data-animation="animated fadeInUp normal" style="margin-top: -4em !important;">
                                {{-- <div class="sc_section_inner">
                                    <h2 class="sc_section_title sc_item_title sc_item_title_without_descr">
                                        Blog<span></span></h2>
                                    <div class="sc_section_content_wrap">
                                        <div id="sc_blogger_221"
                                            class="sc_blogger layout_classic_3 template_masonry  sc_blogger_horizontal">
                                            <div class="isotope_wrap" data-columns="3">
                                                <div
                                                    class="isotope_item isotope_item_classic isotope_item_classic_3 isotope_column_3">
                                                    <div
                                                        class="post_item post_item_classic post_item_classic_3 post_format_standard odd">
                                                        <div class="post_featured">
                                                            <div class="post_thumb"
                                                                data-image="images/post-6.jpg"
                                                                data-title="It Was Delicious">
                                                                <a class="hover_icon hover_icon_link"
                                                                    href="#">
                                                                    <img alt="" src="images/blog1.jpg">
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div class="post_content isotope_item_content">
                                                            <div class="post_info">
                                                                <span class="post_info_item post_info_posted">
                                                                    <a href="#"
                                                                        class="post_info_date">Agosto 23,
                                                                        2021</a>
                                                                </span>
                                                            </div>
                                                            <h4 class="post_title">
                                                                <a href="#">Nota 1</a>
                                                            </h4>
                                                            <div class="post_descr">
                                                                <p>Lorem ipsum dolor sit amet, consectetuer
                                                                    adipiscing elit. Praesent vestibulum
                                                                    molestie lacus. Aenean nonummy hendrerit
                                                                    mauris. Phasellus porta.</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div
                                                    class="isotope_item isotope_item_classic isotope_item_classic_3 isotope_column_3">
                                                    <div
                                                        class="post_item post_item_classic post_item_classic_3 post_format_standard even">
                                                        <div class="post_featured">
                                                            <div class="post_thumb"
                                                                data-image="images/post-5.jpg"
                                                                data-title="A Better Cream Life">
                                                                <a class="hover_icon hover_icon_link"
                                                                    href="#">
                                                                    <img alt="" src="images/blog2.jpg">
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div class="post_content isotope_item_content">
                                                            <div class="post_info">
                                                                <span class="post_info_item post_info_posted">
                                                                    <a href="#"
                                                                        class="post_info_date">Agosto 14,
                                                                        2021</a>
                                                                </span>
                                                            </div>
                                                            <h4 class="post_title">
                                                                <a href="#">Nota 2</a>
                                                            </h4>
                                                            <div class="post_descr">
                                                                <p>Lorem ipsum dolor sit amet, consectetuer
                                                                    adipiscing elit. Praesent vestibulum
                                                                    molestie lacus. Aenean nonummy hendrerit
                                                                    mauris. Phasellus porta.</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div
                                                    class="isotope_item isotope_item_classic isotope_item_classic_3 isotope_column_3">
                                                    <div
                                                        class="post_item post_item_classic post_item_classic_3 post_format_standard odd last">
                                                        <div class="post_featured">
                                                            <div class="post_thumb"
                                                                data-image="images/blog3.jpg"
                                                                data-title="Butter of a Dream">
                                                                <a class="hover_icon hover_icon_link"
                                                                    href="#">
                                                                    <img alt="" src="images/blog3.jpg">
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div class="post_content isotope_item_content">
                                                            <div class="post_info">
                                                                <span class="post_info_item post_info_posted">
                                                                    <a href="#"
                                                                        class="post_info_date">Agosto 5,
                                                                        2021</a>
                                                                </span>
                                                            </div>
                                                            <h4 class="post_title">
                                                                <a href="#">Nota 3</a>
                                                            </h4>
                                                            <div class="post_descr">
                                                                <p>Lorem ipsum dolor sit amet, consectetuer
                                                                    adipiscing elit. Praesent vestibulum
                                                                    molestie lacus. Aenean nonummy hendrerit
                                                                    mauris. Phasellus porta.</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="sc_section_button sc_item_button">
                                            <a href="#"
                                                class="sc_button sc_button_square sc_button_style_filled sc_button_size_large">Ver Más</a>
                                        </div>
                                    </div>
                                </div> --}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="vc_row-full-width"></div>
        </div>
    </div>
</div>
@endsection
