@extends('layouts.admin')

@section('main')
<div class="container">
    <form action="{{url('/admin_lasombrilla/videosyt')}}" method="post" enctype="multipart/form-data">
        @csrf

        @include('admin.videosyt.form',['modo'=>'Crear'])

    </form>
</div>
@endsection