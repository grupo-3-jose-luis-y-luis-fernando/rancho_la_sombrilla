<div class="container">
    <h1> {{ $modo }} Video</h1>

    @if (count($errors)>0)

    <div class="alert alert-danger" role="alert">

        @foreach ( $errors->all() as $error )

        <li> {{ $error }} </li>

        @endforeach

    </div>

    @endif

    <div class="form-group">
        <label for="Nombre">Nombre</label>
        <input class="form-control" type="text" name="nombre" value="{{ isset($videosyt->nombre)?$videosyt->nombre:''}}"
            id="nombre">
        <br>
    </div>

    <div class="form-group">
        <label for="Miniatura">Miniatura</label><br>
        @if (isset($videosyt->miniatura))
        <img class="img-thumbnail img-fluid" src="{{ asset('storage').'/'.$videosyt->miniatura }}" width="150" alt="">
        @endif
        <input class="form-control" type="file" name="miniatura" value="{{ isset($videosyt->miniatura)?$videosyt->miniatura:''}}" id="miniatura">
    </div>

    <div class="form-group">
        <label for="Enlace">Enlace</label>
        <input class="form-control" type="text" name="enlace" value="{{ isset($videosyt->enlace)?$videosyt->enlace:''}}"
            id="enlace">
        <br>
    </div>

    <input class="btn btn-success" type="submit" value="{{ $modo }} Video">

    <a class="btn btn-primary" href="{{ url('/admin_lasombrilla/videosyt') }}">Regresar</a>
</div>