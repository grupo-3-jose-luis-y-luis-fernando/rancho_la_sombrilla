@extends('layouts.admin')

@section('main')
<div class="container">
    <h1 class="titulo-admin">Galería de {{ $videosyts->nombre }}</h1>
</div>
<div class="container"> 
  <a href="{{ route('admin.galeriareceptoras.create', $videosyts->id) }}" class="btn btn-primary mb-3">Añadir Nueva Imagen</a> 
    <table class="table table-dark" id="dt_galeriareceptoras" data-url="{{ route('admin.dt_galeriareceptoras', $videosyts->id) }}">
        <thead>
            <tr> 
                <th>Foto</th>
                <th>Acciones</th>
            </tr>
        </thead> 
        <tbody> 
        </tbody>
    </table> 
</div>
@endsection