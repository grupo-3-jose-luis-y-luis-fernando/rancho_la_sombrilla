@extends('layouts.admin')
@section('main')
<div class="container">
    <h1>galerias</h1>
    <form method="POST" class="form" action="{{ $form_edit ? route('admin.galeriareceptoras.update',['video_id'=>$video->id,'galeriareceptora'=>$galeriareceptoras->id]) : route('admin.galeriareceptoras.store',$video->id) }}" data-alert=true data-archivos=true enctype="multipart/form-data" id="form_galeria">
        @csrf
        
        <div class="form-group">
            @if ($form_edit)
                {{ method_field('PATCH') }}
                <div class="col-md-4">
                    <img class="img-responsive" src="{{ asset('storage/receptoras/galeria/'.$galeriareceptoras->imagen) }}" alt="">
                </div>
            @endif
            <input type="file" name="imagen" class="form-control" id="imagen">
            <span class="invalid-feedback"></span>
        </div>
        <button type="submit">{{ $form_edit ? "Actualizar" : "Guardar" }}</button>
    </form>
</div>
@endsection