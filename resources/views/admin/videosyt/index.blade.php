@extends('layouts.admin')

@section('main')
<div class="container">
    <h1 class="titulo-admin">Galeria de Videos de receptoras de la raza F1</h1>
</div>
<div class="container">

    @if(Session::has('mensaje'))
    <div class="alert alert-success alert-dismissible" role="alert">
        {{ Session::get('mensaje') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>

    </div>
    @endif

    <a href="{{ url('/admin_lasombrilla/videosyt/create') }}" class="btn btn-primary mb-3">Añadir Nuevo Video</a>
    <table class="table table-dark"  id="dt_videosyt" data-url="{{ route('admin.dt_videosyt') }}">
        <thead class="thead-light">
            <tr>
                <th>Nombre</th>
                <th>Miniatura</th>
                <th>Enlace</th>
                <th>Acciones</th>
            </tr>
        </thead>

        <tbody>
{{--             @foreach ( $videosyts as $videosyt )
            <tr>
                <td>{{ $videosyt->nombre}}</td>
                <td>
                    <img class="img-thumbnail img-fluid" src="{{ asset('storage').'/'.$videosyt->miniatura }}" width="150"
                        alt="">
                </td>
                <td>{{ $videosyt->enlace}}</td>
                <td>

                    <a href="{{ url('admin_lasombrilla/videosyt/'.$videosyt->id.'/edit' ) }}" class="btn btn-warning">
                        Editar
                    </a>
                    <form action="{{ url('admin_lasombrilla/videosyt/'.$videosyt->id ) }}" method="post" class="d-inline">
                        @csrf
                        {{ method_field('DELETE') }}
                        <input class="btn btn-danger" type="submit" onclick="return confirm('¿Estas seguro?')"
                            value="Borrar">
                    </form>
                </td>
            </tr>
            @endforeach --}}
        </tbody>
    </table>
</div>
@endsection