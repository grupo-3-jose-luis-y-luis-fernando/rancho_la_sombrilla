@extends('layouts.admin')

@section('main')
<div class="container">
    <form action="{{ url('/admin_lasombrilla/videosyt/'.$videosyt->id ) }}" method="post" enctype="multipart/form-data">
        @csrf
        {{ method_field('PATCH') }}

        @include('admin.videosyt.form',['modo'=>'Editar'])
    </form>
</div>
@endsection