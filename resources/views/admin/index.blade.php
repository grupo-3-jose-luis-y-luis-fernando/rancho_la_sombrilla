@extends('layouts.admin')
 
@section('main')
<!-- Page content -->
<div id="page-content">
    <!-- Dashboard Header -->
    <!-- For an image header add the class 'content-header-media' and an image as in the following example -->
    <div class="content-header content-header-media">
        <div class="header-section hidden-xs hidden-sm">
            <div class="row">
                <!-- Main Title (hidden on small devices for the statistics to fit) -->
                <div class="col-md-12 col-lg-12 hidden-xs hidden-sm">
                    <h1>Administrador de Contenido <strong>Rancho "La Sombrilla"</strong></h1>
                </div>
                <!-- END Main Title -->

            </div>
        </div>
        <!-- For best results use an image with a resolution of 2560x248 pixels (You can also use a blurred image with ratio 10:1 - eg: 1000x100 pixels - it will adjust and look great!) -->
        <img src="administrador/img/placeholders/headers/dashboard_header.jpg" alt="header image" class="animation-pulseSlow">
    </div>
    <!-- END Dashboard Header -->

    <!-- Mini Top Stats Row -->
    <div class="row">
        <div class="col-sm-6 col-lg-12">
            <h2><strong>Inicio</strong></h2>
        </div>
        <div class="col-sm-6 col-lg-6">
            <!-- Widget -->
            <a href="{{ route('admin.slider.index') }}" class="widget widget-hover-effect1">
                <div class="widget-simple">
                    <div class="widget-icon pull-left themed-background-spring animation-fadeIn">
                        <i class="fa fa-home"></i>
                    </div>
                    <h3 class="widget-content text-right animation-pullDown">
                        <strong>Slider</strong>                     
                   </h3>
                </div>
            </a>
            <!-- END Widget -->
        </div>
        <div class="col-sm-6 col-lg-6">
            <a href="{{ route('admin.slider.index') }}" class="widget widget-hover-effect1">
                <div class="widget-simple">
                    <div class="widget-icon pull-left themed-background-spring animation-fadeIn">
                        <i class="fa fa-home"></i>
                    </div>
                    <h3 class="widget-content text-right animation-pullDown" style="color: #b5903f !important;">
                        <strong>Titulo Principal</strong>
                    </h3>
                </div>
            </a>
            <!-- END Widget -->
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6 col-lg-12">
            <h2><strong>ventas</strong></h2>
        </div>
        <div class="col-sm-6 col-lg-6">
            <!-- Widget -->
            <a href="{{ route('admin.categoria.index') }}" class="widget widget-hover-effect1">
                <div class="widget-simple">
                    <div class="widget-icon pull-left themed-background animation-fadeIn">
                        <i class="fa fa-home"></i>
                    </div>
                    <h3 class="widget-content animation-pullDown" style="color: #b5903f !important;">
                        <strong>Categorias</strong>
                    </h3>
                </div>
            </a>
            <!-- END Widget -->
        </div>
        <div class="col-sm-6 col-lg-6">
            <!-- Widget -->
            <a href="{{ route('admin.animales.index') }}" class="widget widget-hover-effect1">
                <div class="widget-simple">
                    <div class="widget-icon pull-left themed-background animation-fadeIn">
                        <i class="fa fa-home"></i>
                    </div>
                    <h3 class="widget-content animation-pullDown" style="color: #b5903f !important;">
                        <strong>Animales</strong>
                    </h3>
                </div>
            </a>
            <!-- END Widget -->
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6 col-lg-12">
            <h2><strong>Más Secciones</strong></h2>
        </div>
        <div class="col-sm-6 col-lg-4">
            <!-- Widget -->
            <a href="{{ route('admin.caballocuartodemilla.index') }}" class="widget widget-hover-effect1">
                <div class="widget-simple">
                    <div class="widget-icon pull-left themed-background animation-fadeIn">
                        <img src="/images/cowb.png" alt="">
                    </div>
                    <h3 class="widget-content animation-pullDown" style="color: #b5903f !important;">
                        <strong>Caballos Cuarto de Milla</strong>
                    </h3>
                </div>
            </a>
            <!-- END Widget -->
        </div>
        <div class="col-sm-6 col-lg-4">
            <!-- Widget -->
            <a href="{{ route('admin.preguntasfrecuente.index') }}" class="widget widget-hover-effect1">
                <div class="widget-simple">
                    <div class="widget-icon pull-left themed-background animation-fadeIn">
                        <img src="/images/cowb.png" alt="">
                    </div>
                    <h3 class="widget-content animation-pullDown" style="color: #b5903f !important;">
                        <strong>Preguntas Frecuentes</strong>
                    </h3>
                </div>
            </a>
            <!-- END Widget -->
        </div>
        <div class="col-sm-6 col-lg-4">
            <!-- Widget -->
            <a href="{{ route('admin.contacto.index') }}" class="widget widget-hover-effect1">
                <div class="widget-simple">
                    <div class="widget-icon pull-left themed-background animation-fadeIn">
                        <img src="/images/cowb.png" alt="">
                    </div>
                    <h3 class="widget-content animation-pullDown" style="color: #b5903f !important;">
                        <strong>Inf. de Contacto</strong>
                    </h3>
                </div>
            </a>
            <!-- END Widget -->
        </div>
    </div>

    <!-- END Mini Top Stats Row -->
</div>
@endsection