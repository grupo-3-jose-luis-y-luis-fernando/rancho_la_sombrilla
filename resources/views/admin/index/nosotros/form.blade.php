<div class="container">
    <h1> {{ $modo }} Titulo </h1>

    @if (count($errors)>0)

    <div class="alert alert-danger" role="alert">

        @foreach ( $errors->all() as $error )

        <li> {{ $error }} </li>

        @endforeach

    </div>

    @endif

    <div class="form-group">
        <label for="Titulo">Titulo</label>
        <input class="form-control" type="text" name="titulo" value="{{ isset($nosotros->titulo)?$nosotros->titulo:''}}"
            id="titulo">
        <br>
    </div>

    <div class="form-group">
        <label for="Parrafo">Parrafo</label>
        <textarea name="parrafo" class="form-control" cols="30" rows="5">{{ isset($nosotros->parrafo)?$nosotros->parrafo:'' }}</textarea>
    
        <br>
    </div>

    <input class="btn btn-success" type="submit" value="{{ $modo }} Titulo">

    <a class="btn btn-primary" href="{{ url('/admin_lasombrilla/nosotros') }}">Regresar</a>
</div>