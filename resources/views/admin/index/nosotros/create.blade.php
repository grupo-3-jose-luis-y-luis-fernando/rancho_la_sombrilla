@extends('layouts.admin')

@section('main')
<div class="container">
    <form action="{{url('/admin_lasombrilla/nosotros')}}" method="post" enctype="multipart/form-data">
        @csrf

        @include('admin.index.nosotros.form',['modo'=>'Crear'])

    </form>
</div>
@endsection