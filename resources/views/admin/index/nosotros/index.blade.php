@extends('layouts.admin')

@section('main')
<div class="container">
    <h1 class="titulo-admin">Sección Nosotros</h1>
</div>
<div class="container">

    @if(Session::has('mensaje'))
    <div class="alert alert-success alert-dismissible" role="alert">
        {{ Session::get('mensaje') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>

    </div>
    @endif

    <table class="table table-dark">
        <thead class="thead-light">
            <tr>
                <th>Id</th>
                <th>Titulo</th>
                <th>Parrafo</th>
                <th>Acciones</th>
            </tr>
        </thead>

        <tbody>
            @foreach ( $nosotros as $nosotro )
            <tr>
                <td>{{ $nosotro->id}}</td>
                <td>{{ $nosotro->titulo}}</td>
                <td>{{ $nosotro->parrafo}}</td>
                <td>

                    <a href="{{ url('admin_lasombrilla/nosotros/'.$nosotro->id.'/edit' ) }}" class="btn btn-warning">
                        Editar
                    </a>

                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    {{ $nosotros->links() }}
</div>
@endsection