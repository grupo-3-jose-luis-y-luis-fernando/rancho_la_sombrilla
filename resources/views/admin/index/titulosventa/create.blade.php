@extends('layouts.admin')

@section('main')
<div class="container">
    <form action="{{url('/admin_lasombrilla/titulosventa')}}" method="post" enctype="multipart/form-data">
        @csrf

        @include('admin.index.titulosventa.form',['modo'=>'Crear'])

    </form>
</div>
@endsection