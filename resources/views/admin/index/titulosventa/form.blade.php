<div class="container">
    <h1> {{ $modo }} Titulo </h1>

    @if (count($errors)>0)

    <div class="alert alert-danger" role="alert">

        @foreach ( $errors->all() as $error )

        <li> {{ $error }} </li>

        @endforeach

    </div>

    @endif

    <div class="form-group">
        <label for="Titulo">Titulo</label>
        <input class="form-control" type="text" name="titulo" value="{{ isset($titulosventas->titulo)?$titulosventas->titulo:''}}"
            id="titulo">
        <br>
    </div>

    <div class="form-group">
        <label for="Portada">Portada (El archivo debe medir <strong>1170 x 500 px</strong> para no salir recortado)</label><br>
        @if (isset($titulosventas->portada))
        <img class="img-thumbnail img-fluid" src="{{ asset('storage').'/'.$titulosventas->portada }}" width="150" alt="">
        @endif
        <input class="form-control" type="file" name="portada" value="{{ isset($titulosventas->portada)?$titulosventas->portada:''}}" id="portada">
    </div>


    <input class="btn btn-success" type="submit" value="{{ $modo }} Titulo">

    <a class="btn btn-primary" href="{{ url('/admin_lasombrilla/titulosventa') }}">Regresar</a>
</div>