@extends('layouts.admin')

@section('main')
<div class="container">
    <form action="{{ url('/admin_lasombrilla/titulosventa/'.$titulosventas->id ) }}" method="post" enctype="multipart/form-data">
        @csrf
        {{ method_field('PATCH') }}

        @include('admin.index.titulosventa.form',['modo'=>'Editar'])
    </form>
</div>
@endsection