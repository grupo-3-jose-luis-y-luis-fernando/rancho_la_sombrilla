@extends('layouts.admin')

@section('main')
<div class="container">
    <form action="{{ url('/admin_lasombrilla/titulo/'.$titulos->id ) }}" method="post" enctype="multipart/form-data">
        @csrf
        {{ method_field('PATCH') }}

        @include('admin.index.titulo.form',['modo'=>'Editar'])
    </form>
</div>
@endsection
@section('scripts')
    @parent
    <script>
        editor('#titulo',250);
    </script>
@endsection