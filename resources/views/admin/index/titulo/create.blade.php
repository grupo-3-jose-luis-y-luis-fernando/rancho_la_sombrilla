@extends('layouts.admin')

@section('main')
<div class="container">
    <form action="{{url('/admin_lasombrilla/titulo')}}" method="post" enctype="multipart/form-data">
        @csrf

        @include('admin.index.titulo.form',['modo'=>'Crear'])

    </form>
</div>
@endsection
@section('scripts')
    @parent
    <script>
        editor('#titulo',250);
    </script>
@endsection