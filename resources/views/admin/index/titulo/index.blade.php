@extends('layouts.admin')

@section('main')
<div class="container">
    <h1 class="titulo-admin">Sección Titulo Principal</h1>
</div>
<div class="container">

    @if(Session::has('mensaje'))
    <div class="alert alert-success alert-dismissible" role="alert">
        {{ Session::get('mensaje') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>

    </div>
    @endif

    <table class="table table-dark">
        <thead class="thead-light">
            <tr>
                <th>Titulo</th>
                <th class="col-lg-3">Parrafo</th>
                <th>Acciones</th>
            </tr>
        </thead>

        <tbody>
            @foreach ( $titulos as $titulo )
            <tr>
                <td>{!! $titulo->titulo !!}</td>
                <td>{{ $titulo->subtitulo}}</td>
                <td class="col-lg-3">{{ $titulo->parrafo}}</td>
                <td>

                    <a href="{{ url('admin_lasombrilla/titulo/'.$titulo->id.'/edit' ) }}" class="btn btn-warning">
                        Editar
                    </a>

                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    {{ $titulos->links() }}
</div>
@endsection