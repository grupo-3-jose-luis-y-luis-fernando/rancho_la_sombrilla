<div class="container">
    <h1> {{ $modo }} Titulo </h1>

    @if (count($errors)>0)

    <div class="alert alert-danger" role="alert">

        @foreach ( $errors->all() as $error )

        <li> {{ $error }} </li>

        @endforeach

    </div>

    @endif

    <div class="form-group">
        <label for="Titulo">Titulo</label>
        <textarea class="form-control" type="text" name="titulo" id="titulo">{!! isset($titulos->titulo)?$titulos->titulo:'' !!}</textarea>
        <br>
    </div>
    <div class="form-group">
        <label for="Subtitulo">Subtitulo</label>
        <input class="form-control" type="text" name="subtitulo" value="{{ isset($titulos->subtitulo)?$titulos->subtitulo:''}}"
            id="subtitulo">
        <br>
    </div>

    <div class="form-group">
        <label for="Parrafo">Parrafo</label>
        <textarea name="parrafo" class="form-control" cols="30" rows="5">{{ isset($titulos->parrafo)?$titulos->parrafo:'' }}</textarea>
    
        <br>
    </div>

    <input class="btn btn-success" type="submit" value="{{ $modo }} Titulo">

    <a class="btn btn-primary" href="{{ url('/admin_lasombrilla/titulo') }}">Regresar</a>
</div>