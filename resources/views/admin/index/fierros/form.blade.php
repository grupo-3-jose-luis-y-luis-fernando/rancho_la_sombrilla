@extends('layouts.admin')
@section('main')
<div class="container">

    <h1>{{ $form_edit ? "Edición de " : "Creación de "}} Fierro</h1>

    <form method="POST" class="form" action="{{ $form_edit ? route('admin.actualizar_fierro', $fierros->id) : route('admin.guardar_fierro') }}" data-alert=true data-archivos=true enctype="multipart/form-data" id="form-fierro">
        @csrf

        <div class="form-group">
            <label for="Imagen" class="col-xs-12">Imagen{!! !$form_edit ? '<span class="text-danger">*</span>' : '' !!}</label>
            @if ($form_edit)
            <div class="col-xs-6" style="margin-bottom: 30px;">
                <img src="{{ asset('storage/fierros/'.$fierros->imagen) }}" alt="" class="img-responsive">
            </div>
            @endif
            <input type="file" name="imagen" class="form-control" id="imagen">
            <span class="invalid-feedback"></span>
        </div>

        <button class="btn btn-success" type="submit">{{ $form_edit ? "Actualizar" : "Guardar" }}</button>
        <a class="btn btn-primary" href="{{ route('admin.fierro') }}">Regresar</a> 
    </form>    
</div>
@endsection