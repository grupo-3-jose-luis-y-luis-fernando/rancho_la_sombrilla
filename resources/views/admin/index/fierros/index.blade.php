@extends('layouts.admin')

@section('main')
<div class="container">
    <h1 class="titulo-admin">Logo principal y Fierros</h1>
</div>
<div class="container">
    <a href="{{ route('admin.crear_fierro') }}" class="btn btn-success mb-3" style="margin-bottom: 20px;">Añadir Nuevo Fierro</a>
    <table class="table table-dark" id="dt_fierro" data-url="{{ route('admin.dt_fierro') }}">
        <thead>
            <tr>
                <th>Numero</th>
                <th>Imagen</th>
                <th>Acciones</th>
            </tr>
        </thead> 
        <tbody> 
        </tbody>
    </table> 
</div>
@endsection