<div class="container">
    <h1> {{ $modo }} Imagen </h1>

    @if (count($errors)>0)

    <div class="alert alert-danger" role="alert">

        @foreach ( $errors->all() as $error )

        <li> {{ $error }} </li>

        @endforeach

    </div>

    @endif

    <div class="form-group">
        <label for="Portada">Portada (El archivo debe medir <strong>800 x 400 px</strong>)</label><br>
        @if (isset($portadanosotros->portada))
        <img class="img-thumbnail img-fluid" src="{{ asset('storage').'/'.$portadanosotros->portada }}" width="150" alt="">
        @endif
        <input class="form-control" type="file" name="portada" value="" id="portada">
    </div>

    <input class="btn btn-success" type="submit" value="{{ $modo }} Imagen">

    <a class="btn btn-primary" href="{{ url('/admin_lasombrilla/portadanosotros') }}">Regresar</a>
</div>