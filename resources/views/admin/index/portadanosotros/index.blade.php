@extends('layouts.admin')

@section('main')
<div class="container">
    <h1 class="titulo-admin">Foto de portada nosotros</h1>
</div>
<div class="container">

    @if(Session::has('mensaje'))
    <div class="alert alert-success alert-dismissible" role="alert">
        {{ Session::get('mensaje') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>

    </div>
    @endif

    <table class="table table-dark">
        <thead class="thead-light">
            <tr>
                <th>Portada</th>
                <th>Acciones</th>
            </tr>
        </thead>

        <tbody>
            @foreach ( $portadanosotros as $portadanosotro )
            <tr>
                <td>
                    <img class="img-thumbnail img-fluid" src="{{ asset('storage').'/'.$portadanosotro->portada }}" width="250"
                        alt="">
                </td>
                <td>

                    <a href="{{ url('admin_lasombrilla/portadanosotros/'.$portadanosotro->id.'/edit' ) }}" class="btn btn-warning">
                        Editar
                    </a>

                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    {{ $portadanosotros->links() }}
</div>
@endsection