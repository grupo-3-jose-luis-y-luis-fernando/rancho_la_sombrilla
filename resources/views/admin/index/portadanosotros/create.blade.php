@extends('layouts.admin')

@section('main')
<div class="container">
    <form action="{{url('/admin_lasombrilla/portadanosotros')}}" method="post" enctype="multipart/form-data">
        @csrf

        @include('admin.index.portadanosotros.form',['modo'=>'Crear'])

    </form>
</div>
@endsection