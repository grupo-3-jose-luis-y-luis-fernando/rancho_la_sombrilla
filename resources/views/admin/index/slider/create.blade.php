@extends('layouts.admin')

@section('main')
<div class="container">
    <form action="{{url('/admin_lasombrilla/slider')}}" method="post" enctype="multipart/form-data">
        @csrf

        @include('admin.index.slider.form',['modo'=>'Crear'])

    </form>
</div>
@endsection
@section('scripts')
    @parent
    <script>
        editor('#titulo',250);
    </script>
@endsection