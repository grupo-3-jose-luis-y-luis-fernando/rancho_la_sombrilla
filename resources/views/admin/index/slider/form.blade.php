<div class="container">
    <h1> {{ $modo }} Slider </h1>

    @if (count($errors)>0)

    <div class="alert alert-danger" role="alert">

        @foreach ( $errors->all() as $error )

        <li> {{ $error }} </li>

        @endforeach

    </div>

    @endif

    <div class="form-group">
        <label for="Titulo">Titulo</label>
        <textarea class="form-control" type="text" name="titulo" id="titulo">{!! isset($slider->titulo)?$slider->titulo:"" !!}</textarea>
            
        <br>
    </div>

    <div class="form-group">
        <label for="Imagen">Imagen (El archivo debe medir <strong>1583 x 594 px</strong> para no salir recortado)</label><br>
        @if (isset($slider->imagen))
        <img class="img-thumbnail img-fluid" src="{{ asset('storage').'/'.$slider->imagen }}" width="150" alt="">
        @endif
        <input class="form-control" type="file" name="imagen" value="" id="imagen">

    </div>

    <input class="btn btn-success" type="submit" value="{{ $modo }} slider">

    <a class="btn btn-primary" href="{{ url('/admin_lasombrilla/slider') }}">Regresar</a>
</div>