@extends('layouts.admin')

@section('main')
<div class="container">
    <h1 class="titulo-admin">Sección Genetica</h1>
</div>
<div class="container">

    @if(Session::has('mensaje'))
    <div class="alert alert-success alert-dismissible" role="alert">
        {{ Session::get('mensaje') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>

    </div>
    @endif

    <table class="table table-dark">
        <thead class="thead-light">
            <tr>
                <th>Titulo</th>
                <th>Parrafo</th>
                <th>Imagen</th>
                <th>Slider</th>
                <th>Acciones</th>
            </tr>
        </thead>

        <tbody>
            @foreach ( $geneticas as $genetica )
            <tr>
                <td>{{ $genetica->titulo}}</td>
                <td>{{ $genetica->parrafo}}</td>
                <td>
                    <img class="img-thumbnail img-fluid" src="{{ asset('storage').'/'.$genetica->imagen }}" width="150"
                        alt="">
                </td>
                <td>
                    <img class="img-thumbnail img-fluid" src="{{ asset('storage').'/'.$genetica->slider }}" width="150"
                        alt="">
                </td>
                <td>

                    <a href="{{ url('admin_lasombrilla/genetica/'.$genetica->id.'/edit' ) }}" class="btn btn-warning">
                        Editar
                    </a>

                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    {{ $geneticas->links() }}
</div>
@endsection