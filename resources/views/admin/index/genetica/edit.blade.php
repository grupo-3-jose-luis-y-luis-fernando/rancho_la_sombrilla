@extends('layouts.admin')

@section('main')
<div class="container">
    <form action="{{ url('/admin_lasombrilla/genetica/'.$genetica->id ) }}" method="post" enctype="multipart/form-data">
        @csrf
        {{ method_field('PATCH') }}

        @include('admin.index.genetica.form',['modo'=>'Editar'])
    </form>
</div>
@endsection