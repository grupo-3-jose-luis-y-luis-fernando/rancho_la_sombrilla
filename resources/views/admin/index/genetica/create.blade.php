@extends('layouts.admin')

@section('main')
<div class="container">
    <form action="{{url('/admin_lasombrilla/genetica')}}" method="post" enctype="multipart/form-data">
        @csrf
        @include('admin.index.genetica.form',['modo'=>'Crear'])
    </form>
</div>
@endsection