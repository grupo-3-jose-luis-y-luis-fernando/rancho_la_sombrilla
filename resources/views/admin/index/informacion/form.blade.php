<div class="container">
    <h1> {{ $modo }} Titulo </h1>

    @if (count($errors)>0)

    <div class="alert alert-danger" role="alert">

        @foreach ( $errors->all() as $error )

        <li> {{ $error }} </li>

        @endforeach

    </div>

    @endif

    <div class="form-group">
        <label for="Titulo">Titulo</label>
        <input class="form-control" type="text" name="titulo" value="{{ isset($informacions->titulo)?$informacions->titulo:''}}"
            id="titulo">
        <br>
    </div>

    <div class="form-group">
        <label for="Parrafo">Parrafo</label>
        <textarea name="parrafo" class="form-control" cols="30" rows="5">{!! isset($informacions->parrafo)?$informacions->parrafo:'' !!}</textarea>
    
        <br>
    </div>

    <div class="form-group">
        <label for="Portada">Portada (El archivo de preguntas debe medir <strong>700 x 308 px</strong> y el de galeria <strong>470 x 308 px</strong> para no salir recortado)</label><br>
        @if (isset($informacions->portada))
        <img class="img-thumbnail img-fluid" src="{{ asset('storage').'/'.$informacions->portada }}" width="150" alt="">
        @endif
        <input class="form-control" type="file" name="portada" value="{{ isset($informacions->portada)?$informacions->portada:''}}" id="portada">
    </div>


    <input class="btn btn-success" type="submit" value="{{ $modo }} Información">

    <a class="btn btn-primary" href="{{ url('/admin_lasombrilla/informacion') }}">Regresar</a>
</div>