@extends('layouts.admin')

@section('main')
<div class="container">
    <form action="{{url('/admin_lasombrilla/informacion')}}" method="post" enctype="multipart/form-data">
        @csrf

        @include('admin.index.informacion.form',['modo'=>'Crear'])

    </form>
</div>
@endsection