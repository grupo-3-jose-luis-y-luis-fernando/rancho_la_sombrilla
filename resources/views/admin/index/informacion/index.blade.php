@extends('layouts.admin')

@section('main')
<div class="container">
    <h1 class="titulo-admin">Sección de Información</h1>
</div>
<div class="container">

    @if(Session::has('mensaje'))
    <div class="alert alert-success alert-dismissible" role="alert">
        {{ Session::get('mensaje') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>

    </div>
    @endif

    <table class="table table-dark">
        <thead class="thead-light">
            <tr>
                <th>Titulo</th>
                <th>Parrafo</th>
                <th>Portada</th>
                <th>Acciones</th>
            </tr>
        </thead>

        <tbody>
            @foreach ( $informacions as $informacion )
            <tr>
                <td>{{ $informacion->titulo}}</td>
                <td>{{ $informacion->parrafo}}</td>
                <td>
                    <img class="img-thumbnail img-fluid" src="{{ asset('storage').'/'.$informacion->portada }}" width="150"
                        alt="">
                </td>
                <td>

                    <a href="{{ url('admin_lasombrilla/informacion/'.$informacion->id.'/edit' ) }}" class="btn btn-warning">
                        Editar
                    </a>

                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    {{ $informacions->links() }}
</div>
@endsection