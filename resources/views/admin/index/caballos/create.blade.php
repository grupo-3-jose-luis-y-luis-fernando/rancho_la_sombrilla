@extends('layouts.admin')

@section('main')
<div class="container">
    <form action="{{url('/admin_lasombrilla/indexcaballos')}}" method="post" enctype="multipart/form-data">
        @csrf
        @include('admin.index.caballos.form',['modo'=>'Crear'])
    </form>
</div>
@endsection