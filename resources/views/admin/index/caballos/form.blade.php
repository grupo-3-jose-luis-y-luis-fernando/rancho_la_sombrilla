<div class="container">
    <h1> {{ $modo }} Sección Caballos </h1>

    @if (count($errors)>0)

    <div class="alert alert-danger" role="alert">

        @foreach ( $errors->all() as $error )

        <li> {{ $error }} </li>

        @endforeach

    </div>

    @endif

    <div class="form-group">
        <label for="Titulo">Titulo</label>
        <input class="form-control" type="text" name="titulo" value="{{ isset($indexcaballos->titulo)?$indexcaballos->titulo:''}}"
            id="titulo">
        <br>
    </div>

    <div class="form-group">
        <label for="Parrafo">Parrafo</label>
        <textarea name="parrafo" class="form-control" cols="30" rows="5">{!! isset($indexcaballos->parrafo)?$indexcaballos->parrafo:'' !!}</textarea>
    
        <br>
    </div>
    <div class="form-group">
        <label for="Imagen">Imagen (El archivo debe medir <strong>415 x 308 px</strong> para no deformar.)</label><br>
        @if (isset($indexcaballos->imagen))
        <img class="img-thumbnail img-fluid" src="{{ asset('storage').'/'.$indexcaballos->imagen }}" width="150" alt="">
        @endif
        <input class="form-control" type="file" name="imagen" value="" id="imagen">
    </div>

    <div class="form-group">
        <label for="Slider">Slider (El archivo debe medir <strong>2000 x 800 px</strong> para no deformar.)</label><br>
        @if (isset($indexcaballos->slider))
        <img class="img-thumbnail img-fluid" src="{{ asset('storage').'/'.$indexcaballos->slider }}" width="150" alt="">
        @endif
        <input class="form-control" type="file" name="slider" value="" id="slider">
    </div>

    <input class="btn btn-success" type="submit" value="{{ $modo }} Caballos">

    <a class="btn btn-primary" href="{{ url('/admin_lasombrilla/indexcaballos') }}">Regresar</a>
</div>