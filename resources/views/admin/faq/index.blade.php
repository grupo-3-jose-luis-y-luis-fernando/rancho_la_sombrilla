@extends('layouts.admin')

@section('main')
<div class="container">
    <h1 class="titulo-admin">Preguntas Frecuentes</h1>
</div>
<div class="container">

    @if(Session::has('mensaje'))
    <div class="alert alert-success alert-dismissible" role="alert">
        {{ Session::get('mensaje') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif

    <a href="{{ route('admin.preguntasfrecuente.create') }}" class="btn btn-success">Crear una pregunta</a>
    <table class="table table-dark">
        <thead class="thead-light">
            <tr>
                <th>Pregunta</th>
                <th>Informacion</th>
                <th>Acciones</th>
            </tr>
        </thead>

        <tbody>
            @foreach ( $preguntasfrecuentes as $preguntasfrecuente )
            <tr>
                <td>{{ $preguntasfrecuente->pregunta}}</td>
                <td>{{ $preguntasfrecuente->informacion}}</td>
                <td>

                    <a href="{{ url('admin_lasombrilla/preguntasfrecuente/'.$preguntasfrecuente->id.'/edit' ) }}" class="btn btn-warning">
                        Editar
                    </a>
                    <form action="{{ url('admin_lasombrilla/preguntasfrecuente/'.$preguntasfrecuente->id ) }}" method="post" class="d-inline">
                        @csrf
                        {{ method_field('DELETE') }}
                        <input class="btn btn-danger" type="submit" onclick="return confirm('¿Estas seguro?')"
                            value="Borrar">
                    </form>

                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    {{ $preguntasfrecuentes->links() }}
</div>
@endsection