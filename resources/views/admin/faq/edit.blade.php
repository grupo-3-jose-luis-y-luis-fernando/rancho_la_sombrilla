@extends('layouts.admin')

@section('main')
<div class="container">
    <form action="{{ url('/admin_lasombrilla/preguntasfrecuente/'.$preguntasfrecuentes->id ) }}" method="post" enctype="multipart/form-data">
        @csrf
        {{ method_field('PATCH') }}

        @include('admin.faq.form',['modo'=>'Editar'])
    </form>
</div>
@endsection