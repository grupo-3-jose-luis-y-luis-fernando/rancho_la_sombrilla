@extends('layouts.admin')

@section('main')
<div class="container">
    <form action="{{url('/admin_lasombrilla/preguntasfrecuente')}}" method="post" enctype="multipart/form-data">
        @csrf

        @include('admin.faq.form',['modo'=>'Crear'])

    </form>
</div>
@endsection