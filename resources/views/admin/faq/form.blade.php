<div class="container">
    <h1> {{ $modo }} Preguntas Frecuentes </h1>

    @if (count($errors)>0)

    <div class="alert alert-danger" role="alert">

        @foreach ( $errors->all() as $error )

        <li> {{ $error }} </li>

        @endforeach

    </div>

    @endif

    <div class="form-group">
        <label for="Pregunta">Pregunta</label>
        <input class="form-control" type="text" name="pregunta" value="{{ isset($preguntasfrecuentes->pregunta)?$preguntasfrecuentes->pregunta:''}}"
            id="pregunta">
        <br>
    </div>

    <div class="form-group">
        <label for="informacion">Informacion</label>
        <textarea name="informacion" class="form-control" cols="30" rows="5">{{ isset($preguntasfrecuentes->informacion)?$preguntasfrecuentes->informacion:'' }}</textarea>
    
        <br>
    </div>

    <input class="btn btn-success" type="submit" value="{{ $modo }} Preguntas Frecuentes">

    <a class="btn btn-primary" href="{{ url('/admin_lasombrilla/preguntasfrecuente') }}">Regresar</a>
</div>