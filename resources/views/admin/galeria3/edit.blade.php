@extends('layouts.admin')

@section('main')
<div class="container">
    <form action="{{ url('/admin_lasombrilla/galeria3/'.$galeria->id ) }}" method="post" enctype="multipart/form-data">
        @csrf
        {{ method_field('PATCH') }}

        @include('admin.galeria3.form',['modo'=>'Editar'])

    </form>
</div>
@endsection