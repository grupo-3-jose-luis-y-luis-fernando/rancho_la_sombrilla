@extends('layouts.admin')

@section('main')
<div class="container">
    <h1 class="titulo-admin">Galeria Caballos Cuarto de Milla</h1>
</div>
<div class="container">

    @if(Session::has('mensaje'))
    <div class="alert alert-success alert-dismissible" role="alert">
        {{ Session::get('mensaje') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>

    </div>
    @endif


    <a href="{{ url('/admin_lasombrilla/galeria3/create') }}" class="btn btn-primary mb-3">Añadir Nueva galeria</a>

    <table class="table table-dark">
        <thead class="thead-light">
            <tr>
                <th>Categoria</th>
                <th>Nombre</th>
                <th>Imagen</th>
                <th>Acciones</th>
            </tr>
        </thead>

        <tbody>
            @foreach($galeria3s as $galeria3)
            <tr>
                <td>{{ $galeria3->nombre_categoria}}</td>
                <td>{{ $galeria3->nombre}}</td>
                <td>
                    <img class="img-thumbnail img-fluid" src="{{ asset('storage').'/'.$galeria3->imagen }}" width="150"
                        alt="">
                </td>
                <td>

                    <a href="{{ url('admin_lasombrilla/galeria3/'.$galeria3->id.'/edit' ) }}" class="btn btn-warning">
                        Editar
                    </a>
                    <form action="{{ url('admin_lasombrilla/galeria3/'.$galeria3->id ) }}" method="post" class="d-inline">
                        @csrf
                        {{ method_field('DELETE') }}
                        <input class="btn btn-danger" type="submit" onclick="return confirm('¿Estas seguro?')"
                            value="Borrar">
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    {{ $galeria3s->links() }}
</div>
@endsection