@extends('layouts.admin')

@section('main')
<div class="container">
    <form action="{{url('/admin_lasombrilla/galeria3')}}" method="post" enctype="multipart/form-data">
        @csrf
        @include('admin.galeria3.form',['modo'=>'Crear'])
    </form>
</div>
@endsection