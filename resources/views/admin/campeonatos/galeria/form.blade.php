@extends('layouts.admin')

@section('main')
<div class="container">
    <h1>{{ $form_edit ? "Edición de " : "Creación de "}}Galeria de Campeonato</h1>
    <form method="POST" action="{{ $form_edit ? route('admin.actualizar_galeria_campeonato', ["campeonato_id"=>$campeonato_id,"galeria_campeonato_id"=>$galeria_campeonato->id]) :  route('admin.guardar_galeria_campeonato',$campeonato_id) }}" class="form" data-archivos=true enctype="multipart/form-data" id="form_galeria-campeonato">
        @csrf
        
        <div class="form-group">
            <label for="imagen" class="col-xs-12">Imagen</label>
            @if ($form_edit)
            <div class="col-xs-6" style="margin-bottom: 30px;">
                <img src="{{ asset('storage/campeonatos-galeria/'.$galeria_campeonato->imagen) }}" alt="" class="img-responsive">
            </div>
            @endif
            <input type="file" name="imagen" class="form-control" id="imagen">
            <span class="invalid-feedback"></span>
        </div>

        <button class="btn btn-success" type="submit">{{ $form_edit ? "Actualizar" : "Guardar" }}</button>
        <a class="btn btn-primary" href="{{ route('admin.galeria_campeonato', ["campeonato_id"=>$campeonato_id]) }}">Regresar</a>
    </form> 
</div>
@endsection