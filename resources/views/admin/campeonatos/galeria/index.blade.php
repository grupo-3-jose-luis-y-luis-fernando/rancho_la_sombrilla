@extends('layouts.admin')

@section('main')
<div class="container">
    <h1 class="titulo-admin">Galeria de Campeonato - {{ $campeonato->nombre }}</h1>
</div>
<div class="container"> 
    <a href="{{ route('admin.crear_galeria_campeonato',$campeonato_id) }}" class="btn btn-success">Crear Galería</a>
    <a class="btn btn-primary" href="{{ route('admin.campeonato') }}">Regresar a Campeonatos</a>
    <table class="table table-dark" id="dt_galeria-campeonato" data-url="{{ route('admin.dt_galeria-campeonato', $campeonato_id) }}">
        <thead>
            <tr>
                <th>Numero</th>
                <th>Imagen</th>
                <th>Campeonato</th>
                <th>Acciones</th>
            </tr>
        </thead> 
        <tbody> 
        </tbody>
    </table> 
</div>
@endsection