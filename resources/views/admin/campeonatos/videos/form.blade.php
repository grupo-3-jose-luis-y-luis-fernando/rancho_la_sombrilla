@extends('layouts.admin')

@section('main')
<div class="container">
    <h1>{{ $form_edit ? "Edición de " : "Creación de "}}Galeria de Campeonato</h1>
    <form method="POST" action="{{ $form_edit ? route('admin.actualizar_video_campeonato', ["campeonato_id"=>$campeonato_id,"video_campeonato_id"=>$video_campeonato->id]) :  route('admin.guardar_video_campeonato',$campeonato_id) }}" class="form" data-archivos=true enctype="multipart/form-data" id="form_video-campeonato">
        @csrf
        
        <div class="form-group">
            <label for="Enlace de Video">Enlace de Video</label>
            <input type="text" class="form-control" value="{{ $form_edit ? $video_campeonato->video : "" }}" name="video" id="video">
            <span class="invalid-feedback"></span>
        </div>

        <button class="btn btn-success" type="submit">{{ $form_edit ? "Actualizar" : "Guardar" }}</button>
        <a class="btn btn-primary" href="{{ route('admin.video_campeonato', ["campeonato_id"=>$campeonato_id]) }}">Regresar</a>
    </form> 
</div>
@endsection