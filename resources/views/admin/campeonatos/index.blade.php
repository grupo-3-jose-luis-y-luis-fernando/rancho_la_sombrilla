@extends('layouts.admin')

@section('main')
<div class="container">
    <h1 class="titulo-admin">Campeonatos</h1>
</div>
<div class="container"> 
    <a href="{{ route('admin.crear_campeonato') }}" class="btn btn-success">Crear un Campeonato</a>
    <table class="table table-dark" id="dt_campeonato" data-url="{{ route('admin.dt_campeonato') }}">
        <thead>
            <tr>
                <th>Numero</th>
                <th>Nombre</th>
                <th>Premio</th>
                <th>Campeonato</th>
                <th>Imagen</th>
                <th>Acciones</th>
            </tr>
        </thead> 
        <tbody> 
        </tbody>
    </table> 
</div>
@endsection