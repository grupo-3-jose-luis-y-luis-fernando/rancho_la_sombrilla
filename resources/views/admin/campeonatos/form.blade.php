@extends('layouts.admin')
@section('main')
<div class="container">

    <h1>{{ $form_edit ? "Edición de " : "Creación de "}} Campeonato</h1>

    <form method="POST" class="form" action="{{ $form_edit ? route('admin.actualizar_campeonato', $campeonato->id) : route('admin.guardar_campeonato') }}" data-alert=true data-archivos=true enctype="multipart/form-data" id="form-campeonato">
        @csrf

        <div class="form-group">
            <label for="nombre">Nombre</label>
            <input type="text" class="form-control" value="{{ $form_edit ? $campeonato->nombre : "" }}" name="nombre" id="nombre">
            <span class="invalid-feedback"></span>
        </div>
        
        <div class="form-group">
            <label for="premio">Premio</label>
            <input type="text" class="form-control" value="{{ $form_edit ? $campeonato->premio : "" }}" name="premio" id="premio">
            <span class="invalid-feedback"></span>
        </div>
        
        <div class="form-group">
            <label for="campeonato">Campeonato</label>
            <input type="text" class="form-control" value="{{ $form_edit ? $campeonato->campeonato : "" }}" name="campeonato" id="campeonato">
            <span class="invalid-feedback"></span>
        </div>

        <div class="form-group">
            <label for="imagen" class="col-xs-12">Imagen</label>
            @if ($form_edit)
            <div class="col-xs-6" style="margin-bottom: 30px;">
                <img src="{{ asset('storage/campeonatos/'.$campeonato->imagen) }}" alt="" class="img-responsive">
            </div>
            @endif
            <input type="file" name="imagen" class="form-control" id="imagen">
            <span class="invalid-feedback"></span>
        </div>
        <button class="btn btn-success" type="submit">{{ $form_edit ? "Actualizar" : "Guardar" }}</button>
        <a class="btn btn-primary" href="{{ route('admin.campeonato') }}">Regresar</a> 
    </form>    
</div>
@endsection