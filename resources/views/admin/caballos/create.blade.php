@extends('layouts.admin')

@section('main')
<div class="container">
    <form action="{{url('/admin_lasombrilla/caballocuartodemilla')}}" method="post" enctype="multipart/form-data">
        @csrf
        @include('admin.caballos.form',['modo'=>'Crear'])
    </form>
</div>
@endsection