@extends('layouts.admin')

@section('main')
<div class="container">
    <form action="{{ url('/admin_lasombrilla/caballocuartodemilla/'.$caballocuartodemilla->id ) }}" method="post" enctype="multipart/form-data">
        @csrf
        {{ method_field('PATCH') }}

        @include('admin.caballos.form',['modo'=>'Editar'])
    </form>
</div>
@endsection