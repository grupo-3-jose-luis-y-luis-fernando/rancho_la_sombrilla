@extends('layouts.admin')
@section('main')
<div class="container">
    <h1>Subcategorias</h1>
    <form method="POST" class="form" action="{{ $form_edit ? route('admin.subcategoria.update',['categoria_id'=>$categoria_id,'subcategorium'=>$subcategoria->id]) : route('admin.subcategoria.store',$categoria_id) }}" data-alert=true>
        @csrf
        @if ($form_edit)
        {{ method_field('PATCH') }}
        @endif
        <div class="form-group">
            <input type="text" name="nombre" class="form-control" id="nombre" value="{{ $form_edit ? $subcategoria->nombre : "" }}">
            <span class="invalid-feedback"></span>
        </div>
        <button type="submit">{{ $form_edit ? "Actualizar" : "Guardar" }}</button>
    </form>
</div>
@endsection