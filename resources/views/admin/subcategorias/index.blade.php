@extends('layouts.admin')

@section('main')
<div class="container">
    <h1 class="titulo-admin">Subcategoría de {{ $categoria->nombre }}</h1>
</div>
<div class="container">

    @if(Session::has('mensaje'))
    <div class="alert alert-success alert-dismissible" role="alert">
        {{ Session::get('mensaje') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>

    </div>
    @endif

  <a href="{{ route('admin.subcategoria.create',['categoria_id'=>$categoria_id]) }}" class="btn btn-primary mb-3">Añadir Nueva Subcategoría</a> 
    <table class="table table-dark">
        <thead class="thead-light">
            <tr>
                <th>Numero</th>
                <th>Nombre</th>
                <th>Acciones</th>
            </tr>
        </thead>

        <tbody>
            @foreach ( $subcategorias as $subcategoria )
            <tr>
                <td>{{ $subcategoria->id}}</td>
                <td>{{ $subcategoria->nombre}}</td>
                <td>

                    <a href="{{ route('admin.subcategoria.edit',['categoria_id'=>$categoria_id, 'subcategorium'=>$subcategoria->id]) }}" class="btn btn-warning">
                        Editar
                    </a>
                    <form action="{{ route('admin.subcategoria.destroy',['categoria_id'=>$categoria_id,'subcategorium'=>$subcategoria->id]) }}" method="post" class="d-inline form" data-alert=true>
                        @csrf
                        {{ method_field('DELETE') }}
                        <input class="btn btn-danger" type="submit" value="Borrar">
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    {{-- {{ $subcategorias->links() }} --}}
</div>
@endsection