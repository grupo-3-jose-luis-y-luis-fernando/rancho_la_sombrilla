@extends('layouts.admin')

@section('main')
<div class="container">
    <form action="{{url('/admin_lasombrilla/categoria')}}" method="post" enctype="multipart/form-data">
        @csrf

        @include('admin.categorias.form',['modo'=>'Crear'])

    </form>
</div>
@endsection