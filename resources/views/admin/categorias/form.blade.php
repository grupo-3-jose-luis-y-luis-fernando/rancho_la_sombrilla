<div class="container">
    <h1> {{ $modo }} Categoria </h1>

    @if (count($errors)>0)

    <div class="alert alert-danger" role="alert">

        @foreach ( $errors->all() as $error )

        <li> {{ $error }} </li>

        @endforeach

    </div>

    @endif

    <div class="form-group">
        <label for="Categoria">Categoria</label>
        <input class="form-control" type="text" name="nombre" value="{{ isset($categorias->nombre)?$categorias->nombre:''}}"
            id="raza">
        <br>
    </div>

    <input class="btn btn-success" type="submit" value="{{ $modo }} Categoria">

    <a class="btn btn-primary" href="{{ url('/admin_lasombrilla/categoria') }}">Regresar</a>
</div>