@extends('layouts.admin')

@section('main')
<div class="container">
    <form action="{{ url('/admin_lasombrilla/categoria/'.$categorias->id ) }}" method="post" enctype="multipart/form-data">
        @csrf
        {{ method_field('PATCH') }}

        @include('admin.categorias.form',['modo'=>'Editar'])
    </form>
</div>
@endsection