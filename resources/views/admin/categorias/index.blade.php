@extends('layouts.admin')

@section('main')
<div class="container">
    <h1 class="titulo-admin">Categoria</h1>
</div>
<div class="container">

    @if(Session::has('mensaje'))
    <div class="alert alert-success alert-dismissible" role="alert">
        {{ Session::get('mensaje') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>

    </div>
    @endif

  <a href="{{ url('/admin_lasombrilla/categoria/create') }}" class="btn btn-primary mb-3">Añadir Nueva Categoria</a> 
    <table class="table table-dark" id="dt_categorias" data-url="{{ route('admin.dt_categorias') }}">
        <thead>
            <tr>
                <th>Numero</th>
                <th>Nombre</th>
                <th>Acciones</th>
            </tr>
        </thead>

        <tbody>
            {{-- @foreach ( $categorias as $categoria )
            <tr>
                <td>{{ $categoria->id}}</td>
                <td>{{ $categoria->nombre}}</td>
                <td>

                    <a href="{{ url('admin_lasombrilla/categoria/'.$categoria->id.'/edit' ) }}" class="btn btn-warning">
                        Editar
                    </a>
                    <form action="{{ url('admin_lasombrilla/categoria/'.$categoria->id ) }}" method="post" class="d-inline">
                        @csrf
                        {{ method_field('DELETE') }}
                        <input class="btn btn-danger" type="submit" onclick="return confirm('¿Estas seguro?')"
                            value="Borrar">
                    </form>

                    <a href="{{ route('admin.subcategoria.index', $categoria->id) }}" class="btn btn-primary">Ver subcategorías</a>

                </td>
            </tr>
            @endforeach --}}
        </tbody>
    </table>
    {{ $categorias->links() }}
</div>
@endsection