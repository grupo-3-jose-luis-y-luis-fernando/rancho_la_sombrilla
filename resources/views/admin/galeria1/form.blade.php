<div class="container">
    <h1> {{ $modo }} Galeria brahman</h1>

    @if (count($errors)>0)

    <div class="alert alert-danger" role="alert">

        @foreach ( $errors->all() as $error )

        <li> {{ $error }} </li>

        @endforeach

    </div>

    @endif

    <div class="form-group">
        <label for="categoria">Categoria</label>
        <select class="form-control" name="categoria" id="categoria">
            <option value="">--Seleccionar Categoria--</option>
            @foreach($categoria as $item)
            <option value="{{ $item->id }}">{{ $item->nombre }}</option>
            @endforeach
        </select>
    </div>

    <div class="form-group">
        <label for="Nombre">Nombre</label>
        <input class="form-control" type="text" name="nombre" value="{{ isset($galeria->nombre)?$galeria->nombre:'' }}"
            id="nombre">
    </div>

    <div class="form-group">
        <label for="Imagen">Imagen</label><br>
        @if (isset($galeria->imagen))
        <img class="img-thumbnail img-fluid" src="{{ asset('storage').'/'.$galeria->imagen }}" width="150" alt="">
        @endif
        <input class="form-control" type="file" name="imagen" value="" id="imagen">

    </div>

    <input class="btn btn-success" type="submit" value="{{ $modo }} Galería">

    <a class="btn btn-primary" href="{{ url('/admin_lasombrilla/galeria1') }}">Regresar</a>
</div>