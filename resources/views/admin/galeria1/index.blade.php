@extends('layouts.admin')

@section('main')
<div class="container">
    <h1 class="titulo-admin">Galeria brahman</h1>
</div>
<div class="container">

    @if(Session::has('mensaje'))
    <div class="alert alert-success alert-dismissible" role="alert">
        {{ Session::get('mensaje') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>

    </div>
    @endif


    <a href="{{ url('/admin_lasombrilla/galeria1/create') }}" class="btn btn-primary mb-3">Añadir Nueva galeria</a>

    <table class="table table-dark">
        <thead class="thead-light">
            <tr>
                <th>Categoria</th>
                <th>Nombre</th>
                <th>Imagen</th>
                <th>Acciones</th>
            </tr>
        </thead>

        <tbody>
            @foreach($galeria1s as $galeria1)
            <tr>
                <td>{{ $galeria1->nombre_categoria}}</td>
                <td>{{ $galeria1->nombre}}</td>
                <td>
                    <img class="img-thumbnail img-fluid" src="{{ asset('storage').'/'.$galeria1->imagen }}" width="150"
                        alt="">
                </td>
                <td>

                    <a href="{{ url('admin_lasombrilla/galeria1/'.$galeria1->id.'/edit' ) }}" class="btn btn-warning">
                        Editar
                    </a>
                    <form action="{{ url('admin_lasombrilla/galeria1/'.$galeria1->id ) }}" method="post" class="d-inline">
                        @csrf
                        {{ method_field('DELETE') }}
                        <input class="btn btn-danger" type="submit" onclick="return confirm('¿Estas seguro?')"
                            value="Borrar">
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    {{ $galeria1s->links() }}
</div>
@endsection