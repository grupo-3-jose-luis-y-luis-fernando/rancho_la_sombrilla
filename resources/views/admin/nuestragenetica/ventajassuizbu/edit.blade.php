@extends('layouts.admin')

@section('main')
<div class="container">
    <form action="{{ url('/admin_lasombrilla/ventajassuizbu/'.$ventajassuizbus->id ) }}" method="post" enctype="multipart/form-data">
        @csrf
        {{ method_field('PATCH') }}

        @include('admin.nuestragenetica.ventajassuizbu.form',['modo'=>'Editar'])
    </form>
</div>
@endsection