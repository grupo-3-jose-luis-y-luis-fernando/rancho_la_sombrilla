@extends('layouts.admin')

@section('main')
<div class="container">
    <form action="{{url('/admin_lasombrilla/ventajassuizbu')}}" method="post" enctype="multipart/form-data">
        @csrf

        @include('admin.nuestragenetica.ventajassuizbu.form',['modo'=>'Crear'])

    </form>
</div>
@endsection