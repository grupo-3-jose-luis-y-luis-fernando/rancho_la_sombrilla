@extends('layouts.admin')

@section('main')
<div class="container">
    <h1 class="titulo-admin">Imagenes Suizbu</h1>
</div>
<div class="container">

    @if(Session::has('mensaje'))
    <div class="alert alert-success alert-dismissible" role="alert">
        {{ Session::get('mensaje') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>

    </div>
    @endif

    <table class="table table-dark">
        <thead class="thead-light">
            <tr>
                <th>Ventajas Economicas</th>
                <th>Acciones</th>
            </tr>
        </thead>

        <tbody>
            @foreach ( $imagenventajasuizbus as $imagenventajasuizbu )
            <tr>
                <td>
                    <img class="img-thumbnail img-fluid" src="{{ asset('storage').'/'.$imagenventajasuizbu->ventajas_economicas }}" width="150"
                        alt="">
                </td>
                <td>

                    <a href="{{ url('admin_lasombrilla/imagenventajasuizbu/'.$imagenventajasuizbu->id.'/edit' ) }}" class="btn btn-warning">
                        Editar
                    </a>

                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    {{ $imagenventajasuizbus->links() }}
</div>
@endsection