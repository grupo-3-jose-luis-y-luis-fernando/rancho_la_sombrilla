@extends('layouts.admin')

@section('main')
<div class="container">
    <form action="{{url('/admin_lasombrilla/imagenventajasuizbu')}}" method="post" enctype="multipart/form-data">
        @csrf

        @include('admin.nuestragenetica.imagenvsuizbu.form',['modo'=>'Crear'])

    </form>
</div>
@endsection