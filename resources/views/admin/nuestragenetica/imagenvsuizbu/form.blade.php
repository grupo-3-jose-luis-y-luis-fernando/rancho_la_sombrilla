<div class="container">
    <h1> {{ $modo }} Imagen </h1>

    @if (count($errors)>0)

    <div class="alert alert-danger" role="alert">

        @foreach ( $errors->all() as $error )

        <li> {{ $error }} </li>

        @endforeach

    </div>

    @endif

    <div class="form-group">
        <label for="Imagen Ventajas Economicas">Imagen Previa (El archivo debe medir <strong>362 x 430 px</strong> para no descuadrar y en formato png con fondo transparente para no afectar el diseño.)</label><br>
        @if (isset($imagenventajasuizbus->ventajas_economicas))
        <img class="img-thumbnail img-fluid" src="{{ asset('storage').'/'.$imagenventajasuizbus->ventajas_economicas }}" width="150" alt="">
        @endif
        <input class="form-control" type="file" name="ventajas_economicas" value="" id="ventajas_economicas">
    </div>

    <input class="btn btn-success" type="submit" value="{{ $modo }} Imagen">

    <a class="btn btn-primary" href="{{ url('/admin_lasombrilla/imagenventajasuizbu') }}">Regresar</a>
</div>