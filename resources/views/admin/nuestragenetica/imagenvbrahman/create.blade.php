@extends('layouts.admin')

@section('main')
<div class="container">
    <form action="{{url('/admin_lasombrilla/imagenventajasbrahman')}}" method="post" enctype="multipart/form-data">
        @csrf

        @include('admin.nuestragenetica.imagenvbrahman.form',['modo'=>'Crear'])

    </form>
</div>
@endsection