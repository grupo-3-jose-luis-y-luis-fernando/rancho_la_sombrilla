@extends('layouts.admin')

@section('main')
<div class="container">
    <h1 class="titulo-admin">Imagenes Brahman</h1>
</div>
<div class="container">

    @if(Session::has('mensaje'))
    <div class="alert alert-success alert-dismissible" role="alert">
        {{ Session::get('mensaje') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>

    </div>
    @endif

    <table class="table table-dark">
        <thead class="thead-light">
            <tr>
                <th>Ventajas Economicas</th>
                <th>Acciones</th>
            </tr>
        </thead>

        <tbody>
            @foreach ( $imagenventajasbrahmans as $imagenventajasbrahman )
            <tr>
                <td>
                    <img class="img-thumbnail img-fluid" src="{{ asset('storage').'/'.$imagenventajasbrahman->ventajas_economicas }}" width="150"
                        alt="">
                </td>
                <td>

                    <a href="{{ url('admin_lasombrilla/imagenventajasbrahman/'.$imagenventajasbrahman->id.'/edit' ) }}" class="btn btn-warning">
                        Editar
                    </a>

                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    {{ $imagenventajasbrahmans->links() }}
</div>
@endsection