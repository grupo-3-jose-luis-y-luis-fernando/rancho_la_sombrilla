@extends('layouts.admin')

@section('main')
<div class="container">
    <form action="{{ url('/admin_lasombrilla/imagenventajasbrahman/'.$imagenventajasbrahmans->id ) }}" method="post" enctype="multipart/form-data">
        @csrf
        {{ method_field('PATCH') }}

        @include('admin.nuestragenetica.imagenvbrahman.form',['modo'=>'Editar'])
    </form>
</div>
@endsection