@extends('layouts.admin')

@section('main')
<div class="container">
    <form action="{{url('/admin_lasombrilla/geneticasuizbu')}}" method="post" enctype="multipart/form-data">
        @csrf
        @include('admin.nuestragenetica.suizbu.form',['modo'=>'Crear'])
    </form>
</div>
@endsection