@extends('layouts.admin')

@section('main')
<div class="container">
    <h1 class="titulo-admin">Genética Brahman</h1>
</div>
<div class="container">

    @if(Session::has('mensaje'))
    <div class="alert alert-success alert-dismissible" role="alert">
        {{ Session::get('mensaje') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>

    </div>
    @endif

    <table class="table table-dark">
        <thead class="thead-light">
            <tr>
                <th>Subtitulo</th>
                <th>Parrafo</th>
                <th>Imagen</th>
                <th>Slider</th>
                <th>Acciones</th>
            </tr>
        </thead>

        <tbody>
            @foreach ( $geneticasuizbus as $geneticasuizbu )
            <tr>
                <td>{{ $geneticasuizbu->subtitulo}}</td>
                <td>{{ $geneticasuizbu->parrafo}}</td>
                <td>
                    <img class="img-thumbnail img-fluid" src="{{ asset('storage').'/'.$geneticasuizbu->imagen }}" width="150"
                        alt="">
                </td>
                <td>
                    <img class="img-thumbnail img-fluid" src="{{ asset('storage').'/'.$geneticasuizbu->slider }}" width="150"
                        alt="">
                </td>
                <td>

                    <a href="{{ url('admin_lasombrilla/geneticasuizbu/'.$geneticasuizbu->id.'/edit' ) }}" class="btn btn-warning">
                        Editar
                    </a>

                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    {{ $geneticasuizbus->links() }}
</div>
@endsection