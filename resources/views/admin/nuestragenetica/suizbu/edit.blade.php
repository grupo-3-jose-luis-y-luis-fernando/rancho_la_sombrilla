@extends('layouts.admin')

@section('main')
<div class="container">
    <form action="{{ url('/admin_lasombrilla/geneticasuizbu/'.$geneticasuizbu->id ) }}" method="post" enctype="multipart/form-data">
        @csrf
        {{ method_field('PATCH') }}

        @include('admin.nuestragenetica.suizbu.form',['modo'=>'Editar'])
    </form>
</div>
@endsection