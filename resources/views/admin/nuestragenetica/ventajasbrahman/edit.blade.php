@extends('layouts.admin')

@section('main')
<div class="container">
    <form action="{{ url('/admin_lasombrilla/ventajasbrahman/'.$ventajasbrahmans->id ) }}" method="post" enctype="multipart/form-data">
        @csrf
        {{ method_field('PATCH') }}

        @include('admin.nuestragenetica.ventajasbrahman.form',['modo'=>'Editar'])
    </form>
</div>
@endsection