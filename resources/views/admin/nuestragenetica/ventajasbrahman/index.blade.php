@extends('layouts.admin')

@section('main')
<div class="container">
    <h1 class="titulo-admin">Ventajas Brahman Gris</h1>
</div>
<div class="container">

    @if(Session::has('mensaje'))
    <div class="alert alert-success alert-dismissible" role="alert">
        {{ Session::get('mensaje') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>

    </div>
    @endif

    <table class="table table-dark">
        <thead class="thead-light">
            <tr>
                <th>Titulo</th>
                <th>Parrafo</th>
                <th>Acciones</th>
            </tr>
        </thead>

        <tbody>
            @foreach ( $ventajasbrahmans as $ventajasbrahman )
            <tr>
                <td>{{ $ventajasbrahman->titulo}}</td>
                <td>{{ $ventajasbrahman->parrafo}}</td>
                <td>

                    <a href="{{ url('admin_lasombrilla/ventajasbrahman/'.$ventajasbrahman->id.'/edit' ) }}" class="btn btn-warning">
                        Editar
                    </a>

                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    {{ $ventajasbrahmans->links() }}
</div>
@endsection