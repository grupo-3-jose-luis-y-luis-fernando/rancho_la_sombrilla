@extends('layouts.admin')

@section('main')
<div class="container">
    <form action="{{url('/admin_lasombrilla/ventajasbrahman')}}" method="post" enctype="multipart/form-data">
        @csrf

        @include('admin.nuestragenetica.ventajasbrahman.form',['modo'=>'Crear'])

    </form>
</div>
@endsection