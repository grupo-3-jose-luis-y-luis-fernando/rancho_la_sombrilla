<div class="container">
    <h1> {{ $modo }} Genetica Brahman </h1>

    @if (count($errors)>0)

    <div class="alert alert-danger" role="alert">

        @foreach ( $errors->all() as $error )

        <li> {{ $error }} </li>

        @endforeach

    </div>

    @endif

    <div class="form-group">
        <label for="Subtitulo">Subtitulo</label>
        <input class="form-control" type="text" name="subtitulo" value="{{ isset($geneticabrahman->subtitulo)?$geneticabrahman->subtitulo:''}}"
            id="subtitulo">
        <br>
    </div>

    <div class="form-group">
        <label for="Parrafo">Parrafo</label>
        <textarea name="parrafo" class="form-control" cols="30" rows="5">{!! isset($geneticabrahman->parrafo)?$geneticabrahman->parrafo:'' !!}</textarea>
    
        <br>
    </div>
    <div class="form-group">
        <label for="Imagen">Imagen (El archivo debe medir <strong>540 x 540 px</strong> para no deformar.)</label><br>
        @if (isset($geneticabrahman->imagen))
        <img class="img-thumbnail img-fluid" src="{{ asset('storage').'/'.$geneticabrahman->imagen }}" width="150" alt="">
        @endif
        <input class="form-control" type="file" name="imagen" value="" id="imagen">
    </div>

    <div class="form-group">
        <label for="Slider">Slider (El archivo debe medir <strong>2000 x 800 px</strong> para no deformar.)</label><br>
        @if (isset($geneticabrahman->slider))
        <img class="img-thumbnail img-fluid" src="{{ asset('storage').'/'.$geneticabrahman->slider }}" width="150" alt="">
        @endif
        <input class="form-control" type="file" name="slider" value="" id="slider">
    </div>

    <input class="btn btn-success" type="submit" value="{{ $modo }} Genetica Brahman">

    <a class="btn btn-primary" href="{{ url('/admin_lasombrilla/geneticabrahman') }}">Regresar</a>
</div>