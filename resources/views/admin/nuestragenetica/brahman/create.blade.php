@extends('layouts.admin')

@section('main')
<div class="container">
    <form action="{{url('/admin_lasombrilla/geneticabrahman')}}" method="post" enctype="multipart/form-data">
        @csrf
        @include('admin.nuestragenetica.brahman.form',['modo'=>'Crear'])
    </form>
</div>
@endsection