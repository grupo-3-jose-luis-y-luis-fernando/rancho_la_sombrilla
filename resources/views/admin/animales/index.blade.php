@extends('layouts.admin')

@section('main')
<div class="container">
    <h1 class="titulo-admin">Animales</h1>
</div>
<div class="container"> 
  <a href="{{ route('admin.animales.create') }}" class="btn btn-primary mb-3">Añadir Nuevo Animal</a> 
    <table class="table table-dark" id="dt_animales" data-url="{{ route('admin.dt_animales') }}">
        <thead>
            <tr>
                <th>Numero</th>
                <th>Categoría</th>
                <th>Subcategoría</th>
                <th>Foto</th>
                <th>Nombre</th>
                <th>Acciones</th>
            </tr>
        </thead> 
        <tbody> 
        </tbody>
    </table> 
</div>
@endsection