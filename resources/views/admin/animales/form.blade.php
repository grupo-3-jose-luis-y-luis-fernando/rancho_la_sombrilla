@extends('layouts.admin')

@section('main')
<div class="container">
    <h1>{{ $form_edit ? "Edición de " : "Creación de "}}Animales en Venta</h1>
    <form method="POST" action="{{ $form_edit ? route('admin.animales.update', $animal->id) :  route('admin.animales.store',$animal) }}" class="form" data-archivos=true enctype="multipart/form-data" id="form_animal">
        @csrf
        @if ($form_edit)
        {{ method_field('PATCH') }}
        @endif
        <div class="form-group">
            <label for="categoria">Categoría</label>
            <select name="categoria" id="categoria" class="form-control">
                <option value="">Seleccione una categoría</option>
                @foreach ($categorias as $item)
                    <option value="{{ $item->id }}" {{ $form_edit && $item->id == $animal->categoria ? "selected"  : "" }}>{{ $item->nombre }}</option>
                @endforeach
            </select>
            <span class="invalid-feedback"></span>
        </div>

        <div class="form-group">
            <label for="subcategoria">Subcategoría</label>
            <select name="subcategoria" id="subcategoria" class="form-control">
                <option value="">Seleccione una subcategoría</option>
                @foreach ($subcategorias as $item)
                    <option value="{{ $item->id }}" {{ $form_edit && $item->id == $animal->subcategoria ? "selected"  : "" }}>{{ $item->nombre }}</option>
                @endforeach
            </select>
            <span class="invalid-feedback"></span>
        </div>

        <div class="form-group">
            <label for="nombre">Nombre</label>
            <input type="text" class="form-control" value="{{ $form_edit ? $animal->nombre : "" }}" name="nombre" id="nombre">
            <span class="invalid-feedback"></span>
        </div>

        <div class="form-group">
            <label for="imagen" class="col-xs-12">Imagen</label>
            @if ($form_edit)
            @method('PATCH')
            <div class="col-xs-6">
                <img src="{{ asset('storage/animales/'.$animal->imagen) }}" alt="" class="img-responsive">
            </div>
            @endif
            <input type="file" name="imagen" class="form-control" id="imagen">
            <span class="invalid-feedback"></span>
        </div>
        <div class="form-group">
            <button type="submit">{{ $form_edit ? "Actualizar" : "Guardar" }}</button>
        </div>
    </form>
</div>
@endsection

@section('scripts')
    @parent
    <script>
        $(document).ready(function(){
            $("#categoria").change(function(){
                $.ajax({
                    url:"{{ route('admin.animales_subcategorias') }}",
                    type:"POST",
                    data:{_token:"{{ csrf_token() }}",categoria:$(this).val()}
                }).done(function(response){
                    $("#subcategoria").html(response);
                }).fail(function(response){
                    console.log(response);
                });
            });
        });
    </script>
@endsection