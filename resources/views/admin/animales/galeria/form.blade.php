@extends('layouts.admin')
@section('main')
<div class="container">
    <h1>galerias</h1>
    <form method="POST" class="form" action="{{ $form_edit ? route('admin.galeria.update',['animal_id'=>$animal->id,'galerium'=>$galeria->id]) : route('admin.galeria.store',$animal->id) }}" data-alert=true data-archivos=true enctype="multipart/form-data" id="form_galeria">
        @csrf
        @if ($form_edit)
        {{ method_field('PATCH') }}
        @endif
        <div class="form-group">
            <label for="tipo">Tipo</label>
            <select name="tipo" id="tipo" class="form-control">
                <option value="">Seleccione el tipo de foto</option>
                <option value="1">Venta</option>
                <option value="2">General</option>
            </select>
            <span class="invalid-feedback"></span>
        </div>
        <div class="form-group">
            <input type="file" name="imagen" class="form-control" id="imagen" value="{{ $form_edit ? $galeria->imagen : "" }}">
            <span class="invalid-feedback"></span>
        </div>
        <button type="submit">{{ $form_edit ? "Actualizar" : "Guardar" }}</button>
    </form>
</div>
@endsection