@extends('layouts.admin')

@section('main')
<div class="container">
    <h1 class="titulo-admin">Galería de {{ $animal->nombre }}</h1>
</div>
<div class="container"> 
  <a href="{{ route('admin.galeria.create', $animal->id) }}" class="btn btn-primary mb-3">Añadir Nueva Imagen</a> 
    <table class="table table-dark" id="dt_galeria" data-url="{{ route('admin.dt_galeria', $animal->id) }}">
        <thead>
            <tr> 
                <th>Foto</th>
                <th>Tipo</th>
                <th>Acciones</th>
            </tr>
        </thead> 
        <tbody> 
        </tbody>
    </table> 
</div>
@endsection