@extends('layouts.admin')

@section('main')
<div class="container">
    <form action="{{url('/admin_lasombrilla/galeria4')}}" method="post" enctype="multipart/form-data">
        @csrf
        @include('admin.galeria1.form',['modo'=>'Crear'])
    </form>
</div>
@endsection