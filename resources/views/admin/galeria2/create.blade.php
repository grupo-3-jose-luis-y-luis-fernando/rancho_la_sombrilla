@extends('layouts.admin')

@section('main')
<div class="container">
    <form action="{{url('/admin_lasombrilla/galeria2')}}" method="post" enctype="multipart/form-data">
        @csrf
        @include('admin.galeria2.form',['modo'=>'Crear'])
    </form>
</div>
@endsection