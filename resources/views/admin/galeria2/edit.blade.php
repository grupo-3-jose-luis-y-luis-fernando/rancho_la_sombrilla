@extends('layouts.admin')

@section('main')
<div class="container">
    <form action="{{ url('/admin_lasombrilla/galeria2/'.$galeria->id ) }}" method="post" enctype="multipart/form-data">
        @csrf
        {{ method_field('PATCH') }}

        @include('admin.galeria2.form',['modo'=>'Editar'])

    </form>
</div>
@endsection