@extends('layouts.admin')

@section('main')
<div class="container">
    <form action="{{url('/admin_lasombrilla/contacto')}}" method="post" enctype="multipart/form-data">
        @csrf

        @include('admin.contacto.form',['modo'=>'Crear'])

    </form>
</div>
@endsection