@extends('layouts.admin')

@section('main')
<div class="container">
    <form action="{{url('/admin_lasombrilla/fotocontacto')}}" method="post" enctype="multipart/form-data">
        @csrf

        @include('admin.contacto.fotocontacto.form',['modo'=>'Crear'])

    </form>
</div>
@endsection