@extends('layouts.admin')

@section('main')
<div class="container">
    <form action="{{ url('/admin_lasombrilla/fotocontacto/'.$portadacontacto->id ) }}" method="post" enctype="multipart/form-data">
        @csrf
        {{ method_field('PATCH') }}

        @include('admin.contacto.fotocontacto.form',['modo'=>'Editar'])
    </form>
</div>
@endsection