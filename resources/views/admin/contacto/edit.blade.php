@extends('layouts.admin')

@section('main')
<div class="container">
    <form action="{{ url('/admin_lasombrilla/contacto/'.$contactos->id) }}" method="post" enctype="multipart/form-data">
        @csrf
        {{ method_field('PATCH') }}

        @include('admin.contacto.form',['modo'=>'Editar'])

    </form>
</div>
@endsection