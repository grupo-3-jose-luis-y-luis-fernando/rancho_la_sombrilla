<div class="container">
    <h1> {{ $modo }} Contacto </h1>

    @if (count($errors)>0)

    <div class="alert alert-danger" role="alert">

        @foreach ( $errors->all() as $error )

        <li> {{ $error }} </li>

        @endforeach

    </div>

    @endif

    <div class="form-group">
        <label for="direccion">Dirección</label>
        <input class="form-control" type="text" name="direccion" value="{{ isset($contactos->direccion)?$contactos->direccion:''}}"
            id="direccion">
        <br>
    </div>    
    
    <div class="form-group">
        <label for="telefono">Teléfono</label>
        <input class="form-control" type="text" name="telefono" value="{{ isset($contactos->telefono)?$contactos->telefono:''}}"
            id="telefono">
        <br>
    </div>
    <div class="form-group">
        <label for="telefono2">Teléfono2</label>
        <input class="form-control" type="text" name="telefono2" value="{{ isset($contactos->telefono2)?$contactos->telefono2:''}}"
        id="telefono2">
        <br>
    </div>
    <div class="form-group">
        <label for="telefono3">Teléfono3</label>
        <input class="form-control" type="text" name="telefono3" value="{{ isset($contactos->telefono3)?$contactos->telefono3:''}}"
            id="telefono3">
        <br>
    </div>
    
    <div class="form-group">
        <label for="correo">Correo Electrónico</label>
        <input class="form-control" type="text" name="correo" value="{{ isset($contactos->correo)?$contactos->correo:''}}"
            id="correo">
        <br>
    </div>
    
    <div class="form-group">
        <label for="facebook">Facebook</label>
        <input class="form-control" type="text" name="facebook" value="{{ isset($contactos->facebook)?$contactos->facebook:''}}"
            id="facebook">
        <br>
    </div>
    
    <div class="form-group">
        <label for="youtube">Youtube</label>
        <input class="form-control" type="text" name="youtube" value="{{ isset($contactos->youtube)?$contactos->youtube:''}}"
            id="youtube">
        <br>
    </div>
    
    <div class="form-group">
        <label for="instagram">Instagram</label>
        <input class="form-control" type="text" name="instagram" value="{{ isset($contactos->instagram)?$contactos->instagram:''}}"
            id="instagram">
        <br>
    </div>

    <input class="btn btn-success" type="submit" value="{{ $modo }} Contacto">

    <a class="btn btn-primary" href="{{ url('/admin_lasombrilla/contacto') }}">Regresar</a>
</div>