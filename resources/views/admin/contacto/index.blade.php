@extends('layouts.admin')

@section('main')
<div class="container">
    <h1 class="titulo-admin">Datos de Contacto</h1>
</div>
<div class="container">

    {{-- @if(Session::has('mensaje'))
    <div class="alert alert-success alert-dismissible" role="alert">
        {{ Session::get('mensaje') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>

    </div>
    @endif --}}
    <table class="table table-dark" id="dt_contacto" data-url="{{ route('admin.dt_contacto') }}">
        <thead class="thead-light">
            <tr>
                <th>Dirección</th>
                <th>Teléfono</th>
                <th>Teléfono2</th>
                <th>Teléfono3</th>
                <th>Correo Electrónico</th>
                <th>Facebook</th>
                <th>Instagram</th>
                <th>Youtube</th>
                <th>Acciones</th>
            </tr>
        </thead>
        <tbody>
            {{-- <tr>
                <td>{{ $contacto->direccion }}</td>
                <td>{{ $contacto->telefono }}</td>
                <td>{{ $contacto->correo }}</td>
                <td>{{ $contacto->facebook }}</td>
                <td>{{ $contacto->instagram }}</td>
                <td>{{ $contacto->youtube }}</td>
                <td>
                    <a href="{{ url('admin_lasombrilla/contacto/'.$contacto->id.'/edit' ) }}" class="btn btn-warning">
                        Editar
                    </a>
                </td>
            </tr> --}}
        </tbody>
    </table>
</div>
@endsection
