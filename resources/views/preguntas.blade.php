@extends('layouts.web')
@section('title', 'Preguntas Frecuentes | Rancho La Sombrilla')
@section('metas')
<meta name="language" content="spanish">
<meta name="copyright" content="Rancho La Sombrilla">
<meta name="author" content="CYBAC">
<meta name="audience" content="all">
<meta name="description" content="Resuelve tus dudas con nosotros.">
<meta name="keywords" content="rancho, sombrilla, preguntas frecuentes, preguntas, dudas,">
<meta name="robots" content="index, all, follow">
<meta name="category" content="ranch">
@show
@section('main')
<div class="top_panel_title top_panel_style_1 title_present breadcrumbs_present scheme_original">
    <div class="bg_cust_11 top_panel_title_inner top_panel_inner_style_1 title_present_inner breadcrumbs_present_inner" style="background-color: #ffffff !important; @mobile margin-top: 5rem !important; @endmobile">
        <div class="content_wrap">
            <h1 class="page_title" style="color: #2b4276;">Preguntas Frecuentes</h1>
        </div>
    </div>
</div>
<div class="page_content_wrap page_paddings_no">
    <div class="content_wrap">
        <div class="content">
            <article class="post_item post_item_single post_featured_default post_format_standard page type-page hentry">
                <section class="post_content">
                    <div class="vc_row wpb_row vc_row-fluid">
                        <div class="wpb_column vc_column_container vc_col-sm-12">
                            <div class="vc_column-inner ">
                                <div class="wpb_wrapper">
                                    <div class="sc_section margin_bottom_large" data-animation="animated fadeInUp normal">
                                        <div class="sc_section_inner">
                                            <h2 class="sc_section_title sc_item_title sc_item_title0 sc_item_title_without_descr">Resuelve Tus Dudas<span></span></h2>
                                            <div class="sc_section_content_wrap">
                                                <div class='faq'>
                                                    @foreach ($preguntas as $item)
                                                    <input id='faq-{{ $item->id }}' type='checkbox'>
                                                    <label for='faq-{{ $item->id }}'>
                                                      <p class="faq-heading">{{ $item->pregunta }}</p>
                                                      <div class='faq-arrow'></div>
                                                        <div class="faq-text">{!! $item->informacion !!}</div>
                                                    </label>
                                                    @endforeach 
                                                  </div>
                                                <div class="vc_empty_space h_1p">
                                                    <span class="vc_empty_space_inner"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </article>
            <section class="related_wrap related_wrap_empty"></section>
        </div>
    </div>
</div>
@endsection