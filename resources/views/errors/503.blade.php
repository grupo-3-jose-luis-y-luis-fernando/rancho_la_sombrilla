@extends('errors::minimal')

@section('title', __('Rancho La Sombrilla'))
<!doctype html>
<html lang="es">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <link href="images/favicon.ico" rel="icon" type="image/x-icon">

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />

    <link href="{{ asset('css/bootstrap.css')}}" rel="stylesheet" />
    <link href="{{ asset('css/coming-sssoon.css')}}" rel="stylesheet" />

    <!--     Fonts     -->
    <link href="https://netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Grand+Hotel' rel='stylesheet' type='text/css'>

</head>

<body>
    <nav class="navbar navbar-transparent navbar-fixed-top" role="navigation">
        <div class="container">
            </ul>
            <ul class="nav navbar-nav navbar-right" style="float: left !important;">
                <li>
                    <a href="#" class="texto">
                        <i class="fa fa-facebook-square icono"></i>
                        &nbsp;&nbsp;Rancho "La Sombrilla"
                    </a>
                </li>
                <li>
                    <a href="#" class="texto">
                        <i class="fa fa-phone icono"></i>
                        &nbsp;&nbsp;4442881241
                    </a>
                </li>
                <li>
                    <a href="contacto@altamirainmobiliaria.com.mx" class="texto">
                        <i class="fa fa-envelope icono"></i>
                        &nbsp;&nbsp;admin@rancholasombrilla.mx
                    </a>
                </li>
            </ul>

        </div>
        </div><!-- /.container -->
    </nav>
    <div class="main" style="background-image: url('{{ asset('images/7.jpg')}}'); z-index: 1;">

        <!--    Change the image source 'images/rick.jpg' with your favourite image.     -->

        <div class="" data-color="black"></div>

        <!--   You can change the black color for the filter with those colors: blue, green, red, orange       -->

        <div class="container">

            <!--  H1 can have 2 designs: "logo" and "logo cursive"           -->

            <div class="content" style="z-index: 3">
                <div style="padding-top: 200px;">
                    {{-- <div style="box-sizing: border-box; text-align: center;">
                        <img src="web/images/txt_logo_color.png" alt="" width="60%" height="">
                    </div> --}}
                    <h2 class="motto" style="min-height: 0px;"><strong>SITIO EN<br>CONSTRUCCIÓN</strong></h2>
                    <h4 style="text-align: center; letter-spacing: 15px; color: #ffff;">EL SITIO ESTARÁ LISTO PRONTO
                    </h4>
                </div>
            </div>
        </div>
        <div class="footer">
            <div class="container">
                Derechos Reservados "Rancho La Sombrilla 2021 | Diseñado por <a target="_blank"
                    href="https://www.grupocybac.com/">CYBAC</a>
            </div>
        </div>
    </div>
</body>
<script src="{{ asset('js/jquery-1.10.2.js')}}" type="text/javascript"></script>
<script src="{{ asset('js/bootstrap.min.js')}}" type="text/javascript"></script>

</html>
