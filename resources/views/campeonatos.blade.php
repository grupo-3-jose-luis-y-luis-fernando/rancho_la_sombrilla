@extends('layouts.campeonatos')
@section('title', 'Campeonatos | Rancho La Sombrilla')
@section('metas')
<meta name="language" content="spanish">
<meta name="copyright" content="Rancho La Sombrilla">
<meta name="author" content="CYBAC">
<meta name="audience" content="all">
<meta name="description" content="Aquí podras ver todos nuestros campeonatos con ejemplares de la mejor calidad de las Razas Suizbu y Brahman Gris.">
<meta name="keywords" content="rancho, sombrilla, campeonatos, suizbu, brahman, brahman gris">
<meta name="robots" content="index, all, follow">
<meta name="category" content="ranch">
@show
@section('main')
<div class="top_panel_title top_panel_style_1 title_present breadcrumbs_present scheme_original">
    <div  class="bg_cust_11 top_panel_title_inner top_panel_inner_style_1  title_present_inner breadcrumbs_present_inner"  style="background-color: #ffffff !important; @mobile margin-top: 5rem !important; @endmobile">
        <div class="content_wrap">
            <h1 class="page_title" style="color: #2b4276;">Campeonatos</h1>
            <div class="breadcrumbs">
                <a class="breadcrumbs_item home" href="{{route('index')}}" style="color: #2b4276;">Inicio</a>
            </div>
        </div>
    </div>
</div>
<div class="page_content_wrap fondo page_paddings_yes">
    <div class="content_wrap">
        <div class="content">
            <article class="post_item post_item_single post_featured_default post_format_standard page type-page hentry">
                <section class="post_content">
                    <div class="vc_row wpb_row vc_row-fluid">
                        <div class="wpb_column vc_column_container vc_col-sm-12">
                            <div class="vc_column-inner ">
                                <div class="wpb_wrapper">
                                    @if(count($campeonatos) > 0)
                                    <div id="sc_blogger_822" class="sc_blogger layout_portfolio_3 template_portfolio sc_blogger_horizontal no_description" style="display: flex; flex-wrap: wrap;"> 
                                            @foreach ($campeonatos as $campeonato) 
                                                <div class="vc_col-md-4">
                                                    <div class="post_content isotope_item_content ih-item colored square effect_shift left_to_right">
                                                        <div class="post_featured img ">
                                                            <a href="#">
                                                                <img alt="{{ $campeonato->nombre }}" width="100%" src="{{ asset('storage/campeonatos/'.$campeonato->imagen) }}" class='img-responsive'>
                                                            </a>
                                                        </div>
                                                        <div class="post_info_wrap info">
                                                            <div class="info-back">
                                                                <h4 class="post_title">
                                                                    {{ $campeonato->premio }}
                                                                </h4>
                                                                <div class="post_descr">
                                                                    <p class="post_info">
                                                                        <span class="post_info_item post_info_posted">
                                                                            <a data-fancybox="campeonato-{{ $campeonato->id }}" href="{{ asset('storage/campeonatos/'.$campeonato->imagen)}}" class="post_info_date">Ver Fotos</a> 
                                                                            @foreach ($galeria_campeonatos->where('categoria',$campeonato->id) as $galeria_campeonato)
                                                                            <a data-fancybox="campeonato-{{ $campeonato->id }}" href="{{ asset('storage/campeonatos-galeria/'.$galeria_campeonato->imagen)}}" class="post_info_date hidden"></a>  
                                                                            @endforeach
                                                                        </span>
                                                                    </p>
                                                                </div>
                                                                <div class="post_descr">
                                                                    <p class="post_info">
                                                                        <span class="post_info_item post_info_posted">
                                                                            @foreach ($video_campeonatos->where('categoria',$campeonato->id) as $video_campeonato)
                                                                            <a data-fancybox="campeonato-{{ $campeonato->id }}" href="{{ $video_campeonato->video }}" class="post_info_date">Ver Videos</a>  
                                                                            @endforeach
                                                                        </span>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <h4 class="sc_title sc_title_regular cmrg_2" style="text-align: center;">
                                                            <a href="#">{{ $campeonato->nombre }}</a>
                                                        </h4>
                                                    </div>
                                                     
                                                </div>
                                                
                                            @endforeach  
                                    </div>
                                    @else
                                        <h4 class="sc_section_title sc_item_title_without_descr" style="text-align: center; margin-bottom: 200px;">Próximamente más contenido</h4>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </article>
            <section class="related_wrap related_wrap_empty"></section>
        </div>
    </div>
</div>
@endsection